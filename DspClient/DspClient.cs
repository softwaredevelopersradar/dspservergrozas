﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Hardware;
using DspDataModel.LinkedStation;
using DspDataModel.Server;
using DspDataModel.Tasks;
using llcss;
using NetLib;
using Nito.AsyncEx;
using Protocols;
using FrequencyRange = Protocols.FrequencyRange;

namespace DspClient
{
    public class DspClient
    {
        public NetClientSlim Client { get; }

        public ClientStatus Status => Client.Status;

        private const byte SenderAddress = 1;
        private const byte ReceiverAddress = 0;

        private readonly AsyncLock _asyncLock;

        public event EventHandler<RadioJamStateUpdateEvent> FrsRadioJamStateUpdateEvent;

        public event EventHandler<SpecialFrequenciesMessage> SpecialFrequenciesUpdateEvent;

        public event EventHandler<DspServerMode> ModeUpdateEvent;

        public event EventHandler<RangeSector[]> SectorsAndRangesUpdateEvent;

        public event EventHandler<FiltersMessage> FiltersUpdateEvent;

        public event EventHandler<string> TextReceivedEvent;

        public event EventHandler<float> DirectionCorrectionChangedEvent;

        public event EventHandler<AttenuatorSetting[]> AttenuatorsUpdateEvent;

        public event EventHandler<MasterSlaveStateChangedEvent> LinkedStationStateChangedEvent;

        public event EventHandler<StationLocationMessage> CoordinatesUpdateEvent;

        public event EventHandler<FRSJammingSetting[]> FrsJammingTargetsUpdateEvent;

        public event EventHandler<FhssJammingSetting[]> FhssJammingTargetsUpdateEvent;

        public event EventHandler<bool> ShaperConnectionUpdateEvent;

        public event EventHandler<StorageActionMessage> PerformStoragActionEvent;

        public event EventHandler<FhssRadioJamUpdateEvent> FhssRadioJamStateUpdateEvent;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> _completionSources;

        private bool _isThreadedReceiveIsWorking = false;

        public DspClient()
        {
            Client = new NetClientSlim {GenerateExceptions = false, AutoReconnectEnabled = true};
            Client.ConnectEvent += OnClientConnected;
            _asyncLock = new AsyncLock();
            _completionSources = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }

        private void OnClientConnected(object sender, EventArgs e)
        {
            var taskQueues = _completionSources.Values.ToArray();
            foreach (var queue in taskQueues)
            {
                foreach (var task in queue)
                {
                    task.TrySetResult(null);
                }
            }
            _completionSources.Clear();
            ThreadedReceive();
        }

        private static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(SenderAddress, ReceiverAddress, (byte) code, 0, length);
        }

        public async Task<bool> Connect(string host, int port)
        {
            if (Status == ClientStatus.IsWorking)
            {
                return true;
            }
            var result = await Client.Connect(host, port).ConfigureAwait(false);
            return result;
        }

        private async Task ThreadedReceive()
        {
            if (_isThreadedReceiveIsWorking)
            {
                return;
            }
            _isThreadedReceiveIsWorking = true;
            try
            {
                while (Client.IsWorking)
                {
                    var headerBuffer = new byte[MessageHeader.BinarySize];
                    var count = await Client.ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    if (count != MessageHeader.BinarySize)
                    {
                        Stop();
                        break;
                    }
                    if (!MessageHeader.TryParse(headerBuffer, out var header))
                    {
                        Stop();
                        break;
                    }
                    switch (header.Code)
                    {
                        case 1:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 2:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 3:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 4:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 6:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 7:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 8:
                            await HandleResponse<GetAttenuatorsResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 9:
                            await HandleResponse<GetAmplifiersResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 11:
                            await HandleResponse<GetRadioSourcesResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 12:
                            await HandleResponse<GetSpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 15:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 16:
                            await HandleResponse<ExecutiveDFResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 17:
                            await HandleResponse<QuasiSimultaneouslyDFResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 18:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 19:
                            await HandleResponse<TechAppSpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 20:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 21:
                            await HandleResponse<HeterodyneRadioSourcesResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 22:
                            await HandleResponse<GetCalibrationProgressResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 23:
                            await HandleResponse<GetBandAmplitudeLevelsResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 24:
                            await HandleResponse<GetAmplitudeTimeSpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 25:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 26:
                            await HandleResponse<StopRecordingResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 27:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 28:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 29:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 30:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 31:
                            await HandleResponse<GetAdaptiveThresholdResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 32:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 33:
                            var radioJamStateUpdate = await ReadResponse<RadioJamStateUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(FrsRadioJamStateUpdateEvent, radioJamStateUpdate);
                            break;
                        case 35:
                            await HandleResponse<SpecialFrequenciesMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 36:
                            await HandleResponse<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 37:
                            await HandleResponse<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 38:
                            await HandleResponse<SectorsAndRangesMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 39:
                            await HandleResponse<FrsJammingMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 40:
                            await HandleResponse<FhssJammingMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 41:
                            await HandleResponse<GetScanSpeedResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 42:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 43:
                            await HandleResponse<StationLocationMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 44:
                            await HandleResponse<GetRadioControlSpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 45:
                            var shaperStateUpdate = await ReadResponse<ShaperStateUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(ShaperConnectionUpdateEvent, shaperStateUpdate.IsShaperConnected);
                            break;
                        case 46:
                            var jamState = await ReadResponse<FhssRadioJamUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(FhssRadioJamStateUpdateEvent, jamState);
                            break;
                        case 47:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 48:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 49:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 50:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 51:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 52:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 53:
                            await HandleResponse<GetBearingPanoramaSignalsResponse>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 54:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 55:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 56:
                            var linkedStationState = await ReadResponse<MasterSlaveStateChangedEvent>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(LinkedStationStateChangedEvent, linkedStationState);
                            break;
                        case 57:
                            await HandleResponse<DirectionCorrectionMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 99:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 100:
                            await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                            break;
                        case 101:
                            var frequenciesMessage = await ReadResponse<SpecialFrequenciesMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(SpecialFrequenciesUpdateEvent, frequenciesMessage);
                            break;
                        case 102:
                            var modeMessage = await ReadResponse<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(ModeUpdateEvent, modeMessage.Mode);
                            break;
                        case 103:
                            var sectorsAndRangesMessage = await ReadResponse<SectorsAndRangesMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(SectorsAndRangesUpdateEvent, sectorsAndRangesMessage.RangeSectors);
                            break;
                        case 104:
                            var attenuatorsMessage = await ReadResponse<AttenuatorsMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(AttenuatorsUpdateEvent, attenuatorsMessage.Settings);
                            break;
                        case 106:
                            var frsMessage = await ReadResponse<FrsJammingMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(FrsJammingTargetsUpdateEvent, frsMessage.Settings);
                            break;
                        case 107:
                            var fhssMessage = await ReadResponse<FhssJammingMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(FhssJammingTargetsUpdateEvent, fhssMessage.Settings);
                            break;
                        case 127:
                            var directionMessage = await ReadResponse<DirectionCorrectionMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(DirectionCorrectionChangedEvent, directionMessage.DirectionCorrection * 0.1f);
                            break;
                        case 118:
                            var filtersMessage = await ReadResponse<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(FiltersUpdateEvent, filtersMessage);
                            break;
                        case 142:
                            var locationMessage = await ReadResponse<StationLocationMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(CoordinatesUpdateEvent, locationMessage);
                            break;
                        case 150:
                            var textMessage = await ReadResponse<SendTextMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(TextReceivedEvent, textMessage.Text);
                            break;
                        case 151:
                            var timeMessage = await ReadResponse<SetTimeRequest>(header, headerBuffer).ConfigureAwait(false);
                            // TODO : add event, write test
                            break;
                        case 152:
                            var storageActionMessage = await ReadResponse<StorageActionMessage>(header, headerBuffer).ConfigureAwait(false);
                            FireEvent(PerformStoragActionEvent, storageActionMessage);
                            break;
                        default:
                            MessageLogger.Error("Received unknown code");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                _isThreadedReceiveIsWorking = false;
            }
        }

        private void FireEvent<T>(EventHandler<T> eventHandler, T eventData)
        {
            Task.Run(() =>
            {
                try
                {
                    eventHandler?.Invoke(this, eventData);
                }
                catch (Exception e)
                {
                    //ignored
                }
            });
        }

        public void Stop()
        {
            Client?.Stop();
        }

        private async Task HandleResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var response = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);
            if (!_completionSources[header.Code].TryDequeue(out var taskCompletionSource))
            {
                throw new Exception("Response came, but no one is waiting for it");
            }
            taskCompletionSource.SetResult(response ?? null);
        }

        private async Task<TaskCompletionSource<IBinarySerializable>> SendRequest(MessageHeader header, byte[] message)
        {
            using (await _asyncLock.LockAsync().ConfigureAwait(false))
            {
                var receiveTask = new TaskCompletionSource<IBinarySerializable>();
                if (!_completionSources.ContainsKey(header.Code))
                {
                    _completionSources[header.Code] = new ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>();
                }
                _completionSources[header.Code].Enqueue(receiveTask);

                if (!await Client.WriteAsync(message).ConfigureAwait(false))
                {
                    if (!receiveTask.Task.IsCompleted)
                    {
                        receiveTask.SetResult(null);
                    }
                }
                return receiveTask;
            }
        }

        public async Task<RequestResult> SetSpecialFrequencies(FrequencyType frequencyType, FrequencyRange[] frequencies)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 1, length: FrequencyRange.BinarySize * frequencies.Length + 2);
            var message = SpecialFrequenciesMessage.ToBinary(header, frequencyType, TargetStation.Current, frequencies);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult) answer.Header.ErrorCode;
        }

        public async Task<RequestResult> SetFrsRadioJamTargets(FRSJammingSetting[] targets)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }
            
            var header = GetMessageHeader(code: 6, length: FRSJammingSetting.BinarySize * targets.Length + 1);
            var message = FrsJammingMessage.ToBinary(header, TargetStation.Current, targets);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }
        
        public async Task<RequestResult> SetFhssRadioJamTargets(int duration, FftResolution fftResolutionCode, IEnumerable<FhssJammingSetting> targets)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }
            var targetsArray = targets.ToArray();
            var header = GetMessageHeader(code: 7, length: 7 + targetsArray.Sum(t => t.StructureBinarySize));
            var message = FhssJammingMessage.ToBinary(header, duration, fftResolutionCode, TargetStation.Current, (byte) targetsArray.Length, targetsArray);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> ConnectToLinkedStation(string linkedStationAddress)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }
            var header = GetMessageHeader(code: 48, length: SerializationExtensions.BinarySize(linkedStationAddress));
            var message = InitMasterSlaveRequest.ToBinary(header, linkedStationAddress);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> DisconnectFromLinkedStation()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }
            var header = GetMessageHeader(code: 49, length: DisconnectFromMasterSlaveStationRequest.BinarySize - MessageHeader.BinarySize);
            var message = DisconnectFromMasterSlaveStationRequest.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> SendSyncTimeToLinkedStation(DateTime time)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }
            var header = GetMessageHeader(code: 51, length: SetTimeRequest.BinarySize - MessageHeader.BinarySize);
            var message = SetTimeRequest.ToBinary(header, (byte) time.Hour, (byte) time.Minute, (byte) time.Second, (short) time.Millisecond);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public Task<RequestResult> ClearStorage(StorageType storageType)
        {
            return PerformStorageAction(storageType, SignalAction.Hide, new int[0]);
        }

        public async Task<RequestResult> PerformStorageAction(StorageType storageType, SignalAction action, int[] signalIds)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }
            var header = GetMessageHeader(code: 52, length: signalIds.Length * 4 + 2);
            var message = StorageActionMessage.ToBinary(header, storageType, action, signalIds);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> SetStationRole(StationRole role, int stationAddress, int linkedStationAddress)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }
            var header = GetMessageHeader(code: 47, length: SetStationRoleRequest.BinarySize - MessageHeader.BinarySize);
            var message = SetStationRoleRequest.ToBinary(header, role, (byte)stationAddress, (byte)linkedStationAddress);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<GetBearingPanoramaSignalsResponse> GetPanoramaSignals(int startFrequencyKhz, int endFrequencyKhz)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 53, length: GetBearingPanoramaSignalsRequest.BinarySize - MessageHeader.BinarySize);
            var message = GetBearingPanoramaSignalsRequest.ToBinary(header, startFrequencyKhz * 10, endFrequencyKhz * 10);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetBearingPanoramaSignalsResponse;
            return answer;
        }

        public async Task<FrequencyRange[]> GetSpecialFrequencies(FrequencyType frequencyType)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }

            var header = GetMessageHeader(code: 35, length: GetSpecialFrequenciesRequest.BinarySize);
            var message = GetSpecialFrequenciesRequest.ToBinary(header, frequencyType, TargetStation.Current);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as SpecialFrequenciesMessage;
            return answer.Frequencies;
        }

        public async Task<byte[]> GetRadioControlSpectrum(int bandNumber)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }

            var header = GetMessageHeader(code: 44, length: GetRadioControlSpectrumRequest.BinarySize - MessageHeader.BinarySize);
            var message = GetRadioControlSpectrumRequest.ToBinary(header, (byte) bandNumber);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetRadioControlSpectrumResponse;
            return answer?.Spectrum;
        }

        public async Task<RequestResult> SetMode(DspServerMode mode)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 2, length: ModeMessage.BinarySize - MessageHeader.BinarySize);
            var message = ModeMessage.ToBinary(header, mode);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> SendText(string text)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 50, length: SerializationExtensions.BinarySize(text));
            var message = SendTextMessage.ToBinary(header, text);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> SetDirectionCorrection(float directionCorrection, bool useCorrection)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 27, length: DirectionCorrectionMessage.BinarySize - MessageHeader.BinarySize);
            var message = DirectionCorrectionMessage.ToBinary(header, (short) (directionCorrection * 10), useCorrection);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<float> GetDirectionCorrection()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return -1;
            }

            var header = GetMessageHeader(code: 57, length: 0);
            var message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DirectionCorrectionMessage;
            var result = answer?.DirectionCorrection ?? -1;
            if (result == -1)
            {
                return result;
            }

            return result * 0.1f;
        }

        public async Task<RequestResult> SetSynchronizationShift(int shift)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 28, length: SetSynchronizationShiftRequest.BinarySize - MessageHeader.BinarySize);
            var message = SetSynchronizationShiftRequest.ToBinary(header, (short)shift);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<AttenuatorSetting?> GetAttenuatorsValues(int bandNumber)
        {
            var result = await GetAttenuatorsValues(new[] {bandNumber}).ConfigureAwait(false);
            if (result == null || result.Length == 0)
            {
                return null;
            }
            return result[0];
        }

        public async Task SetPreciseCalibrationPhases(float frequencyKhz, float direction, float[] phases)
        {
            var header = GetMessageHeader(code: 99, length: SetPrecisePhasesRequest.BinarySize - MessageHeader.BinarySize);

            var message = SetPrecisePhasesRequest.ToBinary(header,
                (int) (frequencyKhz * 10),
                (short) (direction * 10),
                phases.Select(p => (short) (p * 10)).ToArray()
            );

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
        }

        public async Task SetCalibrationType(int calibrationType)
        {
            var header = GetMessageHeader(code: 100, length: SetCorrelationTypeRequest.BinarySize - MessageHeader.BinarySize);
            
            var message = SetCorrelationTypeRequest.ToBinary(header, (byte)calibrationType);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
        }

        public async Task<AttenuatorSetting[]> GetAttenuatorsValues(int[] bandNumbers)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }

            var header = GetMessageHeader(code: 8, length: bandNumbers.Length);
            var message = GetAttenuatorsRequest.ToBinary(header, bandNumbers.Select(n => (byte)n).ToArray());

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAttenuatorsResponse;
            return answer?.Settings;
        }

        public async Task SetCalibrationBands(int[] bandNumbers)
        {
            var header = GetMessageHeader(code: 15, length: bandNumbers.Length);
            var message = GetAttenuatorsRequest.ToBinary(header, bandNumbers.Select(n => (byte)n).ToArray());

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
        }

        public void SaveTheoreticalTable()
        {
            var header = GetMessageHeader(code: 64, length: 0);
            var message = DefaultMessage.ToBinary(header);
            SendRequest(header, message);
        }

        public async Task<float> GetScanSpeed()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return 0;
            }

            var header = GetMessageHeader(code: 41, length: 0);
            var message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetScanSpeedResponse;
            return answer?.FpgaScanSpeed ?? 0;
        }

        public async Task<RequestResult> SetAttenuatorsValue(int bandNumber, float attenuatorValue, bool isConstAttenuatorEnabled)
        {
            var setting = new AttenuatorSetting((byte) bandNumber, (byte) (attenuatorValue * 2), (byte) (isConstAttenuatorEnabled ? 1 : 0));
            return await SetAttenuatorsValues(new[] {setting}).ConfigureAwait(false);
        }

        public async Task<RequestResult> SetAttenuatorsValues(AttenuatorSetting[] settings)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 4, length: settings.Length * AttenuatorSetting.BinarySize);
            var message = AttenuatorsMessage.ToBinary(header, settings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> CalculateCalibrationCorrection(CalibrationCorrectionSignal[] signals)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 30, length: signals.Length * CalibrationCorrectionSignal.BinarySize);
            var message = CalculateCalibrationCorrectionRequest.ToBinary(header, signals);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<byte?> GetAmplifierValues(int bandNumber)
        {
            var result = await GetAmplifiersValues(new[] { bandNumber }).ConfigureAwait(false);
            if (result == null || result.Length == 0)
            {
                return null;
            }
            return result[0];
        }

        public async Task<byte[]> GetAmplifiersValues(int[] bandNumbers)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }

            var header = GetMessageHeader(code: 9, length: bandNumbers.Length);
            var message = GetAttenuatorsRequest.ToBinary(header, bandNumbers.Select(n => (byte)n).ToArray());

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAmplifiersResponse;
            return answer?.Settings;
        }

        public async Task<BandAmplitudeLevel[]> GetBandLevels(int[] bandNumbers)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }

            var header = GetMessageHeader(code: 23, length: bandNumbers.Length);
            var message = GetBandAmplitudeLevelsRequest.ToBinary(header, bandNumbers.Select(n => (byte)n).ToArray());

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetBandAmplitudeLevelsResponse;
            return answer?.BandLevels;
        }

        public async Task<RequestResult> SetStationLocation(double latitude, double longitude, int height, bool useLocation, TargetStation station)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 42, length: StationLocationMessage.BinarySize - MessageHeader.BinarySize);
            var message = StationLocationMessage.ToBinary(header, latitude, longitude, (short) height, useLocation, station);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<StationLocationMessage> GetStationLocation()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }

            var header = GetMessageHeader(code: 43, length: StationLocationMessage.BinarySize - MessageHeader.BinarySize);
            var message = StationLocationMessage.ToBinary(header,0,0,0, true, TargetStation.Current);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as StationLocationMessage;
            return answer;
        }

        public async Task<float> GetAdaptiveThreshold(int bandNumber)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return -1;
            }

            var header = GetMessageHeader(code: 31, length: GetAdaptiveThresholdRequest.BinarySize - MessageHeader.BinarySize);
            var message = GetAdaptiveThresholdRequest.ToBinary(header, (byte) bandNumber);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAdaptiveThresholdResponse;

            return -answer?.ThresholdLevel ?? -1;
        }

        public async Task<ExecutiveDFResponse> ExecutiveDfRequest(int startFrequenceKhz, int endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 16, length: ExecutiveDFRequest.BinarySize - MessageHeader.BinarySize);
            var message = ExecutiveDFRequest.ToBinary(header, startFrequenceKhz * 10, endFrequencyKhz * 10, 
                (byte)phaseAveragingCount, (byte)directionAveragingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as ExecutiveDFResponse;
            return answer;
        }

        public async Task<RadioSource?> QuasiSimultaneousDfRequest(int startFrequenceKhz, int endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 17, length: QuasiSimultaneouslyDFRequest.BinarySize - MessageHeader.BinarySize);
            var message = QuasiSimultaneouslyDFRequest.ToBinary(header, startFrequenceKhz * 10, endFrequencyKhz * 10,
                (byte)phaseAveragingCount, (byte)directionAveragingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as QuasiSimultaneouslyDFResponse;

            if (answer != null && answer.Header.ErrorCode != (byte) RequestResult.Ok)
            {
                return null;
            }
            return answer?.Source;
        }

        public async Task<HeterodyneRadioSouce[]> GetHeterodyneRadioSources(int startFrequenceKhz, int endFrequencyKhz, int stepMhz, int phaseAveragingCount, int directionAveragingCount)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }

            var header = GetMessageHeader(code: 21, length: HeterodyneRadioSourcesRequest.BinarySize - MessageHeader.BinarySize);
            var message = HeterodyneRadioSourcesRequest.ToBinary(header, startFrequenceKhz * 10, endFrequencyKhz * 10, 
                (byte)(stepMhz * 10), (byte)phaseAveragingCount, (byte)directionAveragingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as HeterodyneRadioSourcesResponse;
            return answer?.RadioSources;
        }

        public async Task<RequestResult> SetReceiversChannel(ReceiverChannel channel)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 20, length: SetReceiversChannelRequest.BinarySize - MessageHeader.BinarySize);
            var message = SetReceiversChannelRequest.ToBinary(header, channel);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> SetFrsRadioJamSettings(int emitionDurationMs, int longWorkingSignalDurationMs, int channelsInLiter)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 32, length: SetFrsRadioJamSettingsRequest.BinarySize - MessageHeader.BinarySize);
            var message = SetFrsRadioJamSettingsRequest.ToBinary(header, emitionDurationMs, longWorkingSignalDurationMs, (byte) channelsInLiter);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> SetFrsAutoRadioJamSettings(int emitionDurationMs, int longWorkingSignalDurationMs, int channelsInLiter, int signalBandwitdhThresholdKhz, float threshold)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 54, length: SetFrsAutoRadioJamSettingsRequest.BinarySize - MessageHeader.BinarySize);
            var byteThreshold = (byte) -threshold;
            var message = SetFrsAutoRadioJamSettingsRequest
                .ToBinary(header, emitionDurationMs, longWorkingSignalDurationMs, (byte) channelsInLiter, signalBandwitdhThresholdKhz * 10, byteThreshold);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public async Task<RequestResult> SetAfrsRadioJamSettings(int emitionDurationMs, int longWorkingSignalDurationMs, int channelsInLiter, int searchBandwidthKhz, int searchSector, float threshold)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 55, length: SetAfrsRadioJamSettingsRequest.BinarySize - MessageHeader.BinarySize);
            var byteThreshold = (byte)-threshold;
            var message = SetAfrsRadioJamSettingsRequest
                .ToBinary(header, emitionDurationMs, longWorkingSignalDurationMs, (byte)channelsInLiter, searchBandwidthKhz * 10, (short) (searchSector * 10), byteThreshold);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult)answer.Header.ErrorCode;
        }

        public Task<RequestResult> SetSectorsAndRanges(RangeSector[] rangeSectors)
        {
            return SetSectorsAndRanges(RangeType.Intelligence, TargetStation.Current, rangeSectors);
        }

        public async Task<RequestResult> SetSectorsAndRanges(RangeType rangesType, TargetStation target, RangeSector[] rangeSectors)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }

            var header = GetMessageHeader(code: 3, length: RangeSector.BinarySize * rangeSectors.Length + 2);
            var message = SectorsAndRangesMessage.ToBinary(header, rangesType, target, rangeSectors);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer == null ? RequestResult.ResponseParseError : (RequestResult) answer.Header.ErrorCode;
        }

        public async Task<RequestResult> SetFilters(int threshold, float standardDeviation, int bandwidth, TimeSpan duration)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }
            const int code = 18;
            var header = GetMessageHeader(code, length: FiltersMessage.BinarySize - MessageHeader.BinarySize);
            var message = FiltersMessage.ToBinary(header, (byte)-threshold, (short)(standardDeviation * 10), bandwidth, (int)duration.TotalMilliseconds);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return GetRequestResult(answer, code);
        }

        public async Task<FiltersMessage> GetFilters()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 36, length: 0);
            var message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            return await taskCompletionSource.Task.ConfigureAwait(false) as FiltersMessage;
        }

        public async Task<DspServerMode> GetMode()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return DspServerMode.Stop;
            }
            var header = GetMessageHeader(code: 37, length: 0);
            var message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as ModeMessage;
            return answer.Mode;
        }

        public async Task<RangeSector[]> GetSectorsAndRanges(RangeType rangesType, TargetStation station = TargetStation.Current)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 38, length: GetSectorsAndRangesRequest.BinarySize - MessageHeader.BinarySize);
            var message = GetSectorsAndRangesRequest.ToBinary(header, rangesType, station);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as SectorsAndRangesMessage;
            return answer?.RangeSectors;
        }

        public async Task<FRSJammingSetting[]> GetFrsJammingTargets()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 39, length: 1);
            var message = GetFrsJammingRequest.ToBinary(header, TargetStation.Current);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as FrsJammingMessage;
            return answer?.Settings;
        }

        public async Task<FhssJammingSetting[]> GetFhssJammingTargets()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 40, length: 1);
            var message = GetFhssJammingRequest.ToBinary(header,TargetStation.Current);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as FhssJammingMessage;
            return answer?.Settings;
        }

        public async Task<RequestResult> StartRadioRecording()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return RequestResult.NoConnection;
            }
            const int code = 25;
            var header = GetMessageHeader(code, length: 0);
            var message = StartRecordingRequest.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return GetRequestResult(answer, code);
        }

        /// <summary>
        /// returns Id of recorded record, or -1 if some errors occured during the request
        /// </summary>
        /// <returns></returns>
        public async Task<int> StopRadioRecording()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return -1;
            }
            const int code = 26;
            var header = GetMessageHeader(code, length: 0);
            var message = StopRecordingRequest.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as StopRecordingResponse;
            return answer?.RecordId ?? -1;
        }

        private static RequestResult GetRequestResult(DefaultMessage response, int expectedCode)
        {
            return response == null || response.Header.Code != expectedCode ? RequestResult.ResponseParseError : (RequestResult)response.Header.ErrorCode;
        }

        public async Task<RadioSource[]> GetRadioSources()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }

            var header = GetMessageHeader(code: 11, length: 0);
            var message = GetRadioSourcesRequest.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetRadioSourcesResponse;
            return answer?.RadioSources;
        }

        public async Task<GetCalibrationProgressResponse> GetCalibrationProgress()
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }

            var header = GetMessageHeader(code: 22, length: 0);
            var message = GetCalibrationProgressRequest.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetCalibrationProgressResponse;
            return answer;
        }

        public async Task<byte[]> GetSpectrum(int startFrequency, int endFrequency, int pointCount)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 12, length: GetSpectrumRequest.BinarySize - MessageHeader.BinarySize);
            var message = GetSpectrumRequest.ToBinary(header, startFrequency * 10, endFrequency * 10, pointCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetSpectrumResponse;
            return answer?.Spectrum;
        }

        public async Task<Scan[]> GetTechAppSpectrum(int bandNumber, int averagingCount)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 19, length: TechAppSpectrumRequest.BinarySize - MessageHeader.BinarySize);
            var message = TechAppSpectrumRequest.ToBinary(header, (byte)bandNumber, (byte)averagingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as TechAppSpectrumResponse;
            return answer?.Scans;
        }

        public async Task<byte[]> GetAmplitudeTimeSpectrum(int startFrequency, int endFrequency, int pointCount, int timeSpanSeconds)
        {
            if (Status != ClientStatus.IsWorking)
            {
                return null;
            }
            var header = GetMessageHeader(code: 24, length: GetAmplitudeTimeSpectrumRequest.BinarySize - MessageHeader.BinarySize);
            var message = GetAmplitudeTimeSpectrumRequest
                .ToBinary(header, startFrequency * 10, endFrequency * 10, pointCount, (byte) timeSpanSeconds);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAmplitudeTimeSpectrumResponse;
            return answer?.Spectrum;
        }



        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (await Client.ReadExactBytesCountAsync(buffer, MessageHeader.BinarySize, header.InformationLength).ConfigureAwait(false) != header.InformationLength)
            {
                return null;
            }
            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
