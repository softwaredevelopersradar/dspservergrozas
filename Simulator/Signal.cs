﻿using DspDataModel;
using Settings;
using YamlDotNet.Serialization;

namespace Simulator
{
    public class Signal
    {
        public int FrequencyKhz
        {
            get => _frequencyKhz;
            set
            {
                _frequencyKhz = value;
                Simulator?.UpdateSignal(this);
            }
        }

        public int BandwidthKhz
        {
            get => _bandwidthKhz;
            set
            {
                _bandwidthKhz = value;
                Simulator?.UpdateSignal(this);
            }
        }

        public int Direction
        {
            get => _direction;
            set
            {
                _direction = value;
                Simulator?.UpdateSignal(this);
            }
        }

        public float Amplitude
        {
            get => _amplitude;
            set
            {
                _amplitude = value;
                Simulator?.UpdateSignal(this);
            }
        }

        [YamlIgnore]
        public SignalSimulator Simulator { get; set; }

        public int StartOffset;
        public int EndOffset;

        public int BandNumber { get; private set; }

        public float[,] Phases;
        private int _frequencyKhz;
        private int _bandwidthKhz;
        private int _direction;
        private float _amplitude;

        public Signal()
        {
            _bandwidthKhz = 1;
            Amplitude = -50;
            _frequencyKhz = Constants.FirstBandMinKhz + 10000;
        }

        public Signal(int frequencyKhz, int bandwidthKhz, int direction, float amplitude)
        {
            _frequencyKhz = frequencyKhz;
            _bandwidthKhz = bandwidthKhz;
            _direction = direction;
            Amplitude = amplitude;
            CalculateOffsets();
        }

        public void CalculateOffsets()
        {
            BandNumber = Utilities.GetBandNumber(FrequencyKhz);
            var startFrequency = FrequencyKhz - BandwidthKhz / 2;
            var startBandNumber = Utilities.GetBandNumber(startFrequency);

            StartOffset = startFrequency < Constants.FirstBandMinKhz || startBandNumber != BandNumber
                ? 0
                : Utilities.GetSampleNumber(startFrequency);

            var endFrequency = FrequencyKhz + BandwidthKhz / 2;
            var endBandNumber = Utilities.GetBandNumber(endFrequency);

            EndOffset = endFrequency > Config.Instance.BandSettings.BandCount || endBandNumber != BandNumber
                ? Constants.BandSampleCount - 1
                : Utilities.GetSampleNumber(endFrequency);

            EndOffset = Utilities.GetSampleNumber(endFrequency);
        }
    }
}
