﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel.Hardware;
using llcss;
using NetLib;
using SimulatorProtocols;

namespace Simulator
{
    public class SimulatorServer
    {
        public readonly NetServer Server;

        public bool IsWorking => Server.IsWorking;

        public event EventHandler<DataRequest> DataRequestReadEvent;

        public event EventHandler<SetGeneratorSignalRequest> SignalGeneratorRequestEvent;

        public event EventHandler<ReceiverChannel> ReceiversChannelRequestEvent;

        public event EventHandler<UsrpDataRequest> UsrpDataRequestReadEvent;

        private NetClientSlim _lastClient;

        public SimulatorServer()
        {
            Server = new NetServer();
            Server.DataReadEvent += OnDataRead;
        }

        private async void OnDataRead(object sender, NetServerDataReadEventArgs e)
        {
            if (e.Data.Length == 0)
            {
                e.ClientHandler.Stop();
                return;
            }
            _lastClient = e.ClientHandler;
            await HandleData(e.Data);
        }

        private async Task HandleData(byte[] data)
        {
            var requestSize = 0;
            while (data.Length != requestSize)
            {
                if (DataRequest.IsValid(data))
                {
                    var request = DataRequest.Parse(data);
                    requestSize = request.StructureBinarySize;
                    DataRequestReadEvent?.Invoke(this, request);
                }
                else if (SetGeneratorSignalRequest.IsValid(data))
                {
                    var request = SetGeneratorSignalRequest.Parse(data);
                    requestSize = request.StructureBinarySize;
                    SignalGeneratorRequestEvent?.Invoke(this, request);
                }
                else if (SetReceiversChannel.IsValid(data))
                {
                    var request = SetReceiversChannel.Parse(data);
                    requestSize = request.StructureBinarySize;
                    ReceiversChannelRequestEvent?.Invoke(this, request.Channel);
                }
                else if (UsrpDataRequest.IsValid(data))
                {
                    var request = UsrpDataRequest.Parse(data);
                    requestSize = request.StructureBinarySize;
                    UsrpDataRequestReadEvent?.Invoke(this, request);
                }
                else
                {
                    await Stop();
                    return;
                }

                // check if current byte array is containing more than one request
                if (data.Length != requestSize)
                {
                    data = data.Skip(requestSize).ToArray();
                    requestSize = 0;
                }
            }
        }

        public void Send(byte[] data)
        {
            try
            {
                _lastClient.Write(data);
            }
            catch (Exception e)
            {
                _lastClient.Stop();
            }
        }

        public bool Start(string host, int port)
        {
            return Server.TryStart(host, port);
        }

        public async Task Stop()
        {
            await Server.Stop();
        }
    }
}
