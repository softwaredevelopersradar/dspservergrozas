﻿using System;

namespace Settings
{
    public static class Contract
    {
        public static void Assert(bool condition)
        {
            #if DEBUG
            if (!condition)
            {
                throw new Exception("Contract violation!");
            }
            #endif
        }

        public static void Assert(bool condition, string errorMessage)
        {
        #if DEBUG
            if (!condition)
            {
                throw new Exception(errorMessage);
            }
        #endif
        }
    }
}
