﻿using RadioTables;

namespace Correlator
{
    public interface ITheoreticalTable
    {
        PhaseArray[] GetPhases(float frequencyKhz);
    }
}