﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using RadioTables;
using Settings;

namespace Correlator.CalibrationCorrection
{
    public class CorrectionTable : ITheoreticalTable
    {
        private readonly CorrectionTableDto _table;

        private List<CorrectionFrequencyDifferences>[] _correctionDifferencesByBands;

        public int MinFrequencyKhz { get; private set; }
        public int MaxFrequencyKhz { get; private set; }

        public CorrectionTable(CorrectionTableDto table, int bandCount)
        {
            _table = table;
            Initialize(bandCount);
        }

        public CorrectionTable(string radioPathTableFilename, int bandCount)
        {
            _table = new CorrectionTableDto();
            _table.LoadFromFile(radioPathTableFilename);
            Initialize(bandCount);
        }

        private void Initialize(int bandCount)
        {
            _correctionDifferencesByBands = new List<CorrectionFrequencyDifferences>[bandCount];
            for (var i = 0; i < _correctionDifferencesByBands.Length; i++)
            {
                _correctionDifferencesByBands[i] = new List<CorrectionFrequencyDifferences>();
            }

            foreach (var correctionDifferences in _table.CorrectionDifferences)
            {
                var bandNumber = Utilities.GetBandNumber(correctionDifferences.FrequencyKhz);
                _correctionDifferencesByBands[bandNumber].Add(correctionDifferences);
            }

            MinFrequencyKhz = _table.CorrectionDifferences[0].FrequencyKhz;
            MaxFrequencyKhz = _table.CorrectionDifferences.Max(c => c.FrequencyKhz);
        }

        public VisualTable ToVisualTable()
        {
            return new VisualTable(
                phasesFunc: GetPhases,
                nearestFreqFunc: GetNearestPhases,
                amplitudeFunc: fr => new float[Constants.DfReceiversCount],
                minFrequencyKhz: MinFrequencyKhz,
                maxFrequencyKhz: MaxFrequencyKhz,
                valuesCount: _table.CorrectionDifferences.Count
            );
        }

        private float GetNearestPhases(float frequencyKhz)
        {
            var bandNumber = Utilities.GetBandNumber(frequencyKhz);
            var bestDelta = float.MaxValue;
            CorrectionFrequencyDifferences bestDifferences = null;
            var corrections = _correctionDifferencesByBands[bandNumber];

            for (var i = 0; i < corrections.Count; i++)
            {
                var differences = corrections[i];
                var delta = Math.Abs(differences.FrequencyKhz - frequencyKhz);
                if (delta < bestDelta)
                {
                    bestDelta = delta;
                    bestDifferences = differences;
                }
            }
            return bestDifferences?.FrequencyKhz ?? frequencyKhz;
        }

        public void Save(string filename)
        {
            _table.SaveToFile(filename);
        }

        public PhaseArray[] GetPhases(float frequencyKhz)
        {
            var bandNumber = Utilities.GetBandNumber(frequencyKhz);
            var bestDelta = float.MaxValue;
            CorrectionFrequencyDifferences bestDifferences = null;

            foreach (var differences in _correctionDifferencesByBands[bandNumber])
            {
                var delta = Math.Abs(differences.FrequencyKhz - frequencyKhz);
                if (delta < bestDelta)
                {
                    bestDelta = delta;
                    bestDifferences = differences;
                }
            }
            return bestDifferences?.Phases
                   ?? Enumerable
                       .Repeat(new PhaseArray(new float[Constants.PhasesDifferencesCount]), Constants.DirectionsCount)
                       .ToArray();
        }
    }
}
