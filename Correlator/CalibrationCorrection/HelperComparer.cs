﻿using System;
using System.Collections.Generic;

namespace Correlator.CalibrationCorrection
{
    internal class HelperComparer<T> : IComparer<T>
    {
        private readonly Func<T, T, int> _compare;

        public HelperComparer(Func<T, T, int> compare)
        {
            _compare = compare;
        }

        public int Compare(T x, T y)
        {
            return _compare(x, y);
        }
    }
}