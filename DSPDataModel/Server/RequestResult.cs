﻿namespace DspDataModel.Server
{
    public enum RequestResult
    {
        Ok = 0,
        NoConnection = 1,
        ResponseParseError = 2,
        InvalidServerMode = 3,
        InvalidRequest = 4,
        LinkedStationConnectionError = 5,
    }
}
