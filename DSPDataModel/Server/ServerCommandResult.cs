namespace DspDataModel.Server
{
    public struct ServerCommandResult : IServerCommandResult
    {
        public RequestResult Result { get; }
        public int? ClientId { get; }
        public bool SendBroadcastEvent { get; }

        public ServerCommandResult(RequestResult result, bool sendBroadcastEvent = false, int? clientId = null) : this()
        {
            Result = result;
            ClientId = clientId;
            SendBroadcastEvent = sendBroadcastEvent;
        }
    }
}