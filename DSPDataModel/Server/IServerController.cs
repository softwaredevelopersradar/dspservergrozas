﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel.DataTransfer;
using DspDataModel.LinkedStation;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;

namespace DspDataModel.Server
{
    public interface IServerController
    {
        ITaskManager TaskManager { get; }
        IRadioJamManager RadioJamManager { get; }
        DspServerMode CurrentMode { get; }
        DspServerMode PreDurationMeasurementMode { get; }
        IDataTransferController DataTransferController { get; }

        event EventHandler<IServerCommandResult> ModeChangedEvent;
        event EventHandler<SectorAndRangesChangedEventArgs> SectorsAndRangesChangedEvent;
        event EventHandler<RadioJamTargetsChangedEventArgs> RadioJamFrsTargetsChangedEvent;
        event EventHandler<DspServerMode> SetLinkedStationSlaveModeEvent;
        event EventHandler<SpecialRangesChangedEventArgs> SpecialRangesChangedEvent;
        event EventHandler<RadioJamFhssTargetsChangedEventArgs> RadioJamFhssTargetsChangedEvent;
        event EventHandler RequestFrsTargetsEvent;
        event EventHandler<IReadOnlyCollection<IRadioSource>> ResponseFrsTargetsEvent;

        bool Initialize();

        void UpdateFrsRadioJamTargets(IReadOnlyList<IRadioJamTarget> targets, int clientId = -1);
        Task<IReadOnlyList<ISignal>> GetSignals(IReadOnlyList<FrequencyRange> frequencyRanges, int phaseAveragingCount, int directionAveragingCount, bool calculateHighQualityPhases = false);
        Task<ISignal> GetSignal(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount, bool calculateHighQualityPhases = false);

        Task<RequestResult> SetMode(DspServerMode serverMode, int clientId = -1);

        bool PerformStorageAction(StorageType storage, SignalAction action, int[] signalIds);

        Task<RequestResult> SetFrsJammingTargets(IReadOnlyCollection<IRadioJamTarget> targets, TargetStation station, int clientId = -1);

        Task<RequestResult> SetFhssJammingTargets(IReadOnlyCollection<IRadioJamFhssTarget> targets, TargetStation station, int clientId = -1);
    }
}
