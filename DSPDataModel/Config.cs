﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using DspDataModel.AmplitudeCalulators;
using DspDataModel.Hardware;
using YamlDotNet.Serialization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO.Ports;
using DspDataModel.Data;
using YamlDotNet.Core;
using Constants = Settings.Constants;
using DspDataModel.LinkedStation;

namespace DspDataModel
{
    public class Config
    {
        private static Config _instance;

        public const string ConfigPath = "config.yaml";
        public const string CalibrationConfigPath = "calibrationConfig.yaml";

        public AppConfig AppSettings { get; private set; }
        public DatabaseConfig DatabaseSettings { get; private set; }
        public DataTransferConfig DataTransferSettings { get; private set; }
        public BandConfig BandSettings { get; private set; }
        public RecorderConfig RecorderSettings { get; private set; }
        public ServerConfig ServerSettings { get; private set; }
        public SimulatorConfig SimulatorSettings { get; private set; }
        public DirectionFindingConfig DirectionFindingSettings { get; private set; }
        public FhssSearchConfig FhssSearchSettings { get; private set; }
        public PhaseCorrelatorConfig CorrelatorSettings { get; private set; }
        public HardwareConfig HardwareSettings { get; set; }
        public TheoreticalTableConfig TheoreticalTableSettings { get; set; }
        public CalibrationConfig CalibrationSettings { get; private set; }
        public StoragesConfig StoragesSettings { get; private set; }

        [YamlIgnore]
        public StationsConfig StationsSettings { get; set; }

        private static readonly object LockObject;

        static Config()
        {
            LockObject = new object();
        }

        public Config()
        {
            StationsSettings = new StationsConfig();
        }

        public static void Save(string filename = ConfigPath)
        {
            try
            {
                lock (LockObject)
                {
                    var serializer = new SerializerBuilder()
                        .EmitDefaults()
                        .Build();
                    using (var fs = File.Open(filename, FileMode.Create))
                    using (var sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        serializer.Serialize(sw, _instance);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex, "Can't save config");
            }
        }

        /// <returns> returns true if load is successful </returns>
        public static bool TryLoad(string filename, out Config config)
        {
            try
            {
                LoadInner(filename);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex, $"Can't load config: {ex.Message}");
                config = null;
                return false;
            }
            config = _instance;
            return true;
        }

        private static void LoadInner(string filename)
        {
            CalibrationCorrectionConfig calibrationCorrection = null;
            lock (LockObject)
            {
                var deserializer = new DeserializerBuilder().Build();
                _instance = deserializer.Deserialize<Config>(File.ReadAllText(filename));
                try
                {
                    calibrationCorrection = deserializer
                        .Deserialize<CalibrationCorrectionConfig>(File.ReadAllText(CalibrationConfigPath));
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            var errors = _instance.Validate();
            if (errors.Count > 0)
            {
                var message = errors.Aggregate("Config is invalid!", (s, e) => s + Environment.NewLine + e.ErrorMessage);
                throw new Exception(message);
            }

            _instance.ApplyCalibrationCorrection(calibrationCorrection);
        }

        private void ApplyCalibrationCorrection(CalibrationCorrectionConfig correction)
        {
            if (correction == null)
            {
                return;
            }
            var antennaTableSettings = TheoreticalTableSettings.AntennaTableSettings;
            var correctionsCount = Math.Min(
                correction.AntennaDirectionCorrections.Count,
                antennaTableSettings.Length
            );

            for (var i = 0; i < correctionsCount; ++i)
            {
                antennaTableSettings[i].DirectionCorrection = correction.AntennaDirectionCorrections[i];
            }
        }

        public static Config Instance
        {
            get
            {
                if (_instance == null)
                {
                    try
                    {
                        LoadInner(ConfigPath);
                    }
                    catch (YamlException e)
                    {
                        Exception messageException = e;
                        if (e.InnerException != null)
                        {
                            messageException = e.InnerException;
                        }

                        MessageLogger.Error(messageException);
                        throw;
                    }
                    catch (Exception e)
                    {
                        MessageLogger.Error(e);
                        throw;
                    }
                }
                return _instance;
            }
        }

        public List<ValidationResult> Validate()
        {
            var results = new List<ValidationResult>();
            var context = new ValidationContext(_instance);
            
            Validator.TryValidateObject(_instance, context, results, true);
            ValidateObjectProperties(_instance);

            return results;

            void ValidateObjectProperties(object currentObject)
            {
                var objectProperties = currentObject.GetType().GetProperties();
                foreach (var property in objectProperties)
                {
                    var member = property.GetValue(currentObject);
                    if (member == null || member is Array || member is Config || member is string || member is List<StationPositionConfig>)
                    {
                        continue;
                    }
                    var memberContext = new ValidationContext(member);
                    Validator.TryValidateObject(member, memberContext, results, true);

                    ValidateObjectProperties(member);
                }
            }
        }
    }

    public class FhssSearchConfig
    {
        [Range(1, 10_000)]
        public int FrequenciesFilterThreshold { get; set; }

        [Range(100, 30_000)]
        public float RangeSectorKhz { get; set; }

        [Range(0.01, 1.0)]
        public float RangeRelativeThreshold {get; set; }

        [Range(1, 90)]
        public int DirectionSector { get; set; }

        [Range(0.01, 1.0)]
        public float DirectionGlobalRelativeThreshold { get; set; }

        [Range(0.01, 1.0)]
        public float DirectionLocalRelativeThreshold { get; set; }

        [Range(typeof(TimeSpan), "00:00:00", "00:10:00")]
        public TimeSpan FixedRadioSourceTimeThreshold { get; set; }

        [Range(typeof(TimeSpan), "00:00:01", "01:00:00")]
        public TimeSpan FixedRadioSourceLastUpdateTimeThreshold { get; set; }

        [Range(1, 100)]
        public float StepFrequencyThreshold { get; set; }

        [Range(0, 1024)]
        public int NetworkMinSignalCount { get; set; }

        [Range(typeof(TimeSpan), "00:00:01", "00:05:00")]
        public TimeSpan FhssAcumulationDuration { get; set; }

        [Range(typeof(TimeSpan), "00:00:05", "01:00:00")]
        public TimeSpan OldFhssSignalCollectTimeSpan { get; set; }

        [Range(typeof(TimeSpan), "00:00:05", "00:10:00")]
        public TimeSpan OldFhssSignalCollectInterval { get; set; }

        [Range(100,15_000)]
        public int FhssDurationMeasureTimeMs { get; set; }

        /// <summary>
        /// 255 - not set. 1 - true, 0 - false
        /// </summary>
        [YamlIgnore]
        public byte SearchFhss { get; set; } = 255;
    }

    public class SimulatorConfig
    {
        [IpString]
        public string SimulatorHost { get; set; }

        [Port]
        public int SimulatorPort { get; set; }
    }

    public class AppConfig
    {
        public bool StartupHideWindow { get; set; }
    }

    public class BandConfig
    {
        [BandCount]
        public int BandCount { get; set; }

        [YamlIgnore]
        public int LastBandMaxKhz => Constants.FirstBandMinKhz + BandCount * Constants.BandwidthKhz;
    }

    public class StationPositionConfig : IPosition
    {
        public int StationId { get; set; }

        // WGS-84 coordinate system
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }

        public StationPositionConfig()
        {
            StationId = -1;
            Latitude = -1;
            Longitude = -1;
            Altitude = -1;
        }

        public bool AreCoordinatesDefined()
        {
            // -1, -1 is undefined
            return Math.Abs(Latitude - (-1)) > 1e-5 && Math.Abs(Longitude - (-1)) > 1e-5;
        }
    }

    public class DirectionFindingConfig
    {
        /// <summary>
        /// Scan count needed for making accurate direction finding
        /// </summary>
        [Range(1, 100)]
        public int DefaultDirectionScanCount { get; set; }
        
        [Range(1, 100)]
        public int DefaultBearingScanCount { get; set; }
        
        [Range(0.0, 1.0)]
        public float RadioSourceRelativeSubScanCount { get; set; }
        
        [Range(0.001, 10_000)]
        public float SignalsMergeGapKhz { get; private set; }
        
        [Range(0, 360)]
        public float SignalsMergeDirectionDeviation { get; private set; }
        public float DirectionCorrectionFactor { get; private set; }

        [YamlIgnore]
        public float DirectionCorrection { get; set; } = -1;

        public AmplitudeCalculatorPolicy AmplitudeCalculatorPolicy { get; private set; }

        /// <summary>
        /// Scan count needed for spectrum averaging count
        /// </summary>
        [Range(1, 100)]
        public int SpectrumScanCount { get; private set; }

        /// <summary>
        /// Minimal size of gap between signals
        /// </summary>
        [Range(1, 1000)]
        public int SignalsGapPointCount { get; private set; }

        /// <summary>
        /// Threshold for filtering signals with only one sample (point) with low amplitude.
        /// Optimization for reducing signal count when threshold approximately equals noise ampltitudes
        /// </summary>
        [Range(0, 20)]
        public float LowStraightSignalThreshold { get; private set; }

        [Range(0, 500)]
        public int StraightSignalWidthKhz { get; private set; }

        /// <summary>
        /// For all signals wider than this phase calculation uses only part of samples (points)
        /// </summary>
        [Range(50, 10_000)]
        public int BroadbandSignalWidthKhz { get; private set; }

        /// <summary>
        /// Additional level that's added to noise level for getting adaptive threshold for band
        /// </summary>
        [Range(0, 40)] 
        public float AdditionalAdaptiveLevel { get; private set; }

        [Range(typeof(TimeSpan), "00:00:01", "00:01:00")]
        public TimeSpan BearingPanoramaTimeThreshold { get; private set; }

        [YamlIgnore]
        public int SignalsGapKhz => (int)(SignalsGapPointCount * Constants.SamplesGapKhz);
    }

    public class RecorderConfig
    {
        /// <summary>
        /// Recorder's quality from 0.1 (worst quality) to 1 (best quality)
        /// </summary>
        [Range(0.1, 1)]
        public float RecorderQuality { get; private set; }
    }

    public class ServerConfig
    {
        [IpString]
        public string ServerHost { get; private set; }

        [Port]
        public int ServerPort { get; private set; }
    }

    public class DatabaseConfig
    {
        [IpString]
        public string DatabaseHost { get; private set; }

        [Port]
        public int DatabasePort { get; private set; }

        [Range(0, 5)]
        public int ReconnectionAttemptsCount { get; private set; }
    }

    public class StationsConfig
    {
        public StationRole Role { get; set; }

        public StationPositionConfig OwnPosition { get; set; } = new StationPositionConfig();

        public List<StationPositionConfig> LinkedPositions { get; private set; } = new List<StationPositionConfig>();
    }

    public class DataTransferConfig
    {
        [IpString]
        public string DataTransferHost { get; private set; }

        [Port]
        public int DataTransferPort { get; private set; }

        [Range(0, 5)]
        public int ReconnectionAttemptsCount { get; private set; }
    }

    public class CalibrationConfig
    {
        public string CalibrationCorrectionFilename { get; set; }
        public string RadioPathTableFilename { get; set; }
        
        [Range(100, Constants.BandwidthKhz)]
        public int CalibrationStepKhz { get; private set; }

        [Range(1, 200)]
        public int CalibrationMinScanCount { get; set; }
        
        [Range(1, 1000)]
        public int CalibrationMaxScanCount { get; set; }

        [Range(1, 100)]
        public int SignalCalibrationScanCount { get; set; }

        /// <summary>
        /// Threshold relative to noise level (i.e. 30 means that real threshold would be noise level + 30 dB)
        /// </summary>
        [Range(0, 50)]
        public int SignalCalibrationRelativeThreshold { get; set; }
        public bool UseNoiseGeneratorCalibration { get; set; }

        public SignalGeneratorConfig SignalGeneratorSettings { get; set; }

        /// <summary>
        /// If noise level is lower than this threshold, we wouldn't try to recalibrate this band with activated attenuators
        /// </summary>
        public int CalibrationAttenuatorThreshold { get; set; }
    }

    public class StoragesConfig
    {
        [Range(typeof(TimeSpan), "00:00:01", "00:10:00")]
        public TimeSpan ActiveSignalTimeSpan { get; private set; }

        [Range(typeof(TimeSpan), "00:00:01", "00:10:00")]
        public TimeSpan NewSignalTimeSpan { get; private set; }

        [Range(typeof(TimeSpan), "00:00:01", "00:01:00")]
        public TimeSpan SpectrumHistoryStorageUpdateRate { get; private set; }

        [Range(typeof(TimeSpan), "00:00:01", "00:30:00")]
        public TimeSpan SpectrumHistoryStorageHistoryLength { get; private set; }

        /// <summary>
        /// Old radio source - is one that inactive more than current timespan.
        /// Such radio sources are deleted from radio source storage
        /// </summary>
        [Range(typeof(TimeSpan), "00:00:01", "01:00:00")]
        public TimeSpan OldRadioSourceTimeSpan { get; private set; }

        [Range(typeof(TimeSpan), "00:00:01", "00:10:00")]
        public TimeSpan OldRadioSourcesCollectTimeSpan { get; private set; }

        [Range(1, 200)]
        public int MaxRadioSourceDataCount { get; private set; }

        [Range(typeof(TimeSpan), "00:00:01", "00:10:00")]
        public TimeSpan MaxRadioSourceDataTimeSpan { get; private set; }

        [Range(10, 10_000)]
        public int UpdateFrsTablePeriodMs { get; private set; }
    }

    public class PhaseCorrelatorConfig
    {
        /// <summary>
        /// Relative threshold from the real threshold to max amplitude
        /// </summary>
        [Range(0.0, 1.0)]
        public float AmplitudeRelativeThreshold { get; set; }

        [Range(0.0, 1.0)]
        public float CorrelationThreshold { get; set; }

        [Range(0.0, 1.0)]
        public float StandardDeviationWeigth { get; set; }
    }

    public class HardwareConfig
    {
        [Range(1, 3)]
        public int FpgaDevicesCount { get; set; }

        [IpOrComString]
        public string ReceiverComPort { get; set; }

        [Range(1, 1_000_000)]
        public int ReceiverBaudRate { get; set; }

        public string AttenuatorsFilename { get; set; }

        public bool SimulateFpgaDevices { get; set; }

        [Range(5, 200)]
        public int CycleSynchronizationShift { get; set; }

        [Range(1, 1000)]
        public int TactingCycleLengthMs { get; set; }
        public bool UseRs232TransmitionChannel { get; set; }
        public bool EnableFpgaUsbLog { get; set; }
        public bool CloseFpgaDevicesOnAppClose { get; set; }
        public int CentralPeakLowerage { get; set; }
        public RadioJamConfig JammingConfig { get; set; }
        public FpgaDeviceConfig[] DeviceSettings { get; set; }
        public UsrpConfig UsrpSettings { get; set; }
        public GpsConfig GpsSettings { get; set; }

        //todo : GET OUR NAMES PLZ
        public CompassConfig FirstCompassSettings { get; set; }
        public CompassConfig SecondCompassSettings { get; set; }

    }

    public class GpsConfig 
    {
        [IpOrComString]
        public string ComPort { get; set; }

        [Range(1, 1_000_000)]
        public int BaudRate { get; set; }
        
        [Range(1, 120)]
        public int ForceUpdateIntervalMin { get; set; }

        public bool IsSimulation { get; set; }

        /// <summary>
        /// starting point of simulation
        /// </summary>
        [Range(-90, 90)]
        public float SimulatedLatitude { get; set; }

        /// <summary>
        /// starting point of simulation
        /// </summary>
        [Range(-180, 180)]
        public float SimulatedLongitude { get; set; }

        /// <summary>
        /// position delta. newposition = position +- delta
        /// </summary>
        [Range(0,1)]
        public float SimulatedDelta { get; set; }
    }

    public class UsrpConfig
    {
        public bool SimulateUsrp { get; set; }

        [IpString]
        public string Host { get; set; }

        [Port]
        public int Port { get; set; }

        [Port]
        public int OutputPort { get; set; }

        [Range(1, 70)]
        public byte Amplification { get; set; }

        [Range(0,100)]
        public int AddedLevel { get; set; }

        [Range(-50, 50)]
        public int AdaptiveThresholdAddedLevel { get; set; }

        [Range(1,1000)]
        public int MaxResponseWaitTime { get; set; }
    }

    public class RadioJamConfig
    {
        public bool SimulateJamShaper { get; set; }

        [Range(1, 1000)]
        public float ControlFrequencyDeviationKhz { get; set; }

        [IpString]
        public string ShaperHost { get; set; }

        [Port]
        public int ShaperPort { get; set; }

        [Range(1, 3)]
        public int ShaperErrorByteCount { get; set; }

        [IpString]
        public string ClientHost { get; set; }

        [Port]
        public int ClientPort { get; set; }

        [Range(100, 10_000)]
        public int FhssJamUpdateEventPeriodMs { get; set; }

        [Range(0, 2000)]
        public int FhssJamMaxPeakCount { get; set; }

        [Range(1, 10)]
        public int SecureCommandResponseCount { get; set; }

        [Range(1, 10)]
        public int LittersCount { get; set; }

        [Range(100, 10_000)]
        public int CommandResponseTimeoutMs { get; set; }

        public byte VoiceJamDeviceId { get; set; }

        [Range(100, 60_000)]
        public int ShaperStateUpdateTimeMs { get; set; }

        [Range(100, 1000)]
        public int SendConditionCommandDelayMs { get; set; }
    }

    public class FpgaDeviceConfig
    {
        public int Id { get; set; }

        [Range(1, 100)]
        public int ChannelsCount { get; set; }
        public ChannelConfig[] ChannelSettings { get; set; }
    }

    public class ChannelConfig
    {
        public bool IsDfChannel { get; set; }
        public int AmplitudesOffset { get; set; }
        public int PhasesOffset { get; set; }
    }

    public class ComPortConfig
    {
        public string ComPortName { get; set; }

        [Range(1, 1_000_000)]
        public int BaudRate { get; set; }
    }

    public class CompassConfig
    {
        public bool IsSimulation { get; set; }
        public ComPortConfig ComPortSettings { get; set; }
    }

    public class TheoreticalTableConfig
    {
        [Range(100, 10_000)]
        public int StepKhz { get; set; }
        public AntennaTableConfig[] AntennaTableSettings { get; set; }

        public int GetAntennaIndex(float frequencyKhz)
        {
            var index = 0;
            for (; index < AntennaTableSettings.Length; ++index)
            {
                if (frequencyKhz < AntennaTableSettings[index].MaxFrequencyKhz)
                {
                    return index;
                }
            }
            throw new Exception($"Can't find antenna index, frequency {frequencyKhz} KHz is too high");
        }
    }

    public class AntennaTableConfig
    {
        [Range(0.0001, 1000)]
        public float Radius { get; set; }

        [Range(1, int.MaxValue)]
        public int MaxFrequencyKhz { get; set; }

        [YamlIgnore]
        public float DirectionCorrection { get; internal set; }
    }

    internal class CalibrationCorrectionConfig
    {
        public IReadOnlyList<float> AntennaDirectionCorrections { get; private set; }
    }

    public class SignalGeneratorConfig
    {
        [IpOrComString]
        public string Host { get; set; }
        public SignalGeneratorType Type { get; set; }
        public float DefaultCalibrationLevel { get; set; }
        public CalibrationRangesConfig[] CalibrationRangesSettings { get; set; }
    }

    public class CalibrationRangesConfig
    {
        [Range(25_000, 3025_000)]
        public int StartFrequencyKhz { get; set; }
        [Range(25_000, 3025_000)]
        public int EndFrequencyKhz { get; set; }
        [Range(-31, 0)]
        public float CalibrationLevel { get; set; }

        public bool ContainsFrequency(float frequencyKhz) => 
            frequencyKhz >= StartFrequencyKhz && frequencyKhz <= EndFrequencyKhz;

        public bool IsValidRange()
        {
            return StartFrequencyKhz < EndFrequencyKhz;
        }
    }
}
