﻿namespace DspDataModel.Data
{
    public interface ICoordinates
    {
        double Latitude { get; }
        double Longitude { get; }
    }

    public interface IPosition : ICoordinates
    {
        double Altitude { get; }
    }

    public struct Coordinates : ICoordinates
    {
        public double Latitude { get; }
        public double Longitude { get; }

        public Coordinates(double latitude, double longitude) : this()
        {
            Latitude = latitude;
            Longitude = longitude;
        }
    }

    public struct Position : IPosition
    {
        public double Latitude { get; }
        public double Longitude { get; }
        public double Altitude { get; }

        public Position(double latitude, double longitude, double altitude) : this()
        {
            Latitude = latitude;
            Longitude = longitude;
            Altitude = altitude;
        }
    }
}
