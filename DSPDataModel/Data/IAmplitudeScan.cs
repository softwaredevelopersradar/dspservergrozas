﻿namespace DspDataModel.Data
{
    public interface IAmplitudeScan : IAmplitudeBand, IBandNumber, ICreationTime
    {
        int ScanIndex { get; }
    }
}