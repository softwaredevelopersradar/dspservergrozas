﻿using System;

namespace DspDataModel.Data
{
    public interface ICreationTime
    {
        DateTime CreationTime { get; }
    }
}
