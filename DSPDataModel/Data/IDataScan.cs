﻿using System.Collections.Generic;

namespace DspDataModel.Data
{
    public interface IDataScan : IAmplitudeScan
    {
        IReadOnlyList<IReceiverScan>[] ReceiverScans { get; }

        float[] GetPhaseDifferences(int scanIndex, int index);
        float[] GetPhaseDifferences(float threshold, int index);
        float[] GetPhaseDifferences(float threshold, int index, out float deviation);

        IDataScan SubScan(int startScanIndex, int scansCount);

        float GetAmplitude(int scanIndex, int index);
        float GetAmplitude(int index);
        IReadOnlyList<float[]> GetAmplitudeBands(int startSectorIndex, int endSectorIndex);
    }
}
