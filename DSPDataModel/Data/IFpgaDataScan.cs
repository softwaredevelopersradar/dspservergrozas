﻿using System.Collections.Generic;

namespace DspDataModel.Data
{
    public interface IFpgaDataScan : IBandNumber
    {
        IReadOnlyList<IReceiverScan> Scans { get; }
        IReceiverScan RcScan { get; }
        void SetScan(int receiverIndex, IReceiverScan scan);
        int ScanIndex { get; }
    }
}
