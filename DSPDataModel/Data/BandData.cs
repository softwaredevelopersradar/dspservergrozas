﻿using System.Diagnostics;
using Settings;

namespace DspDataModel.Data
{
    public class BandData : IBandData
    {
        public float[] Amplitudes { get; }

        public float[] Phases { get; }

        public int Count => Amplitudes.Length;

        public BandData(float[] amplitudes, float[] phases)
        {
            Contract.Assert(amplitudes.Length == phases.Length);

            Amplitudes = amplitudes;
            Phases = phases;
        }        
    }
}
