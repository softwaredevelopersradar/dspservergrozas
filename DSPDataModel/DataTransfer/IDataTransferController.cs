﻿using System;
using System.Collections.Generic;
using DspDataModel.Storages;
using DspDataModel.Tasks;

namespace DspDataModel.DataTransfer
{
    public interface IDataTransferController
    {
        bool IsConnected { get; }
        void Connect();
        void Disconnect();

        bool SetMode(DspServerMode mode);
        Dictionary<int, float?> PerformExecutiveDf(IReadOnlyList<int> stations, float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount);
        void SendExecutiveDfResponse(int stationId, float? frequencyKhz, float direction);
        
        event EventHandler<DspServerMode> SetModeRequest;
        event EventHandler<(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)> PerformExecutiveDfRequest;
        event EventHandler<(int stationId, IReadOnlyList<IRadioSource> signals)> SignalsReceivedEvent;
        event EventHandler<(int stationId, IReadOnlyList<IRadioSource> signals)> SignalsResultsReceivedEvent;
        event EventHandler<int> SetBandNumberRequest;
        event EventHandler<DateTime> SetLocalTimeRequest;

        void SetBandNumber(int bandNumber);
        void SendSignals(int stationId, IReadOnlyList<ISignal> signals, int bandNumber);
        void SendRdfResults(int stationId, IReadOnlyList<IRadioSource> results);
        void SendLocalTime(DateTime localTime);
    }
}
