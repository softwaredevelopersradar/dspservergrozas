﻿using DspDataModel.Database;
using System.Collections.Generic;

namespace DspDataModel.Storages
{
    public interface IRadioSourceStorage
    {
        IDatabaseController DatabaseController { get; }

        void Put(IEnumerable<ISignal> signals);
        void PutLinkedStationSignals(IEnumerable<IRadioSource> signals);
        IEnumerable<IRadioSource> GetRadioSources();
        bool HasSameRadioSource(ISignal signal);
        void Clear();
        int? GetSignalId(ISignal signal);
        IRadioSource FindSameRadioSource(ISignal signal);
        IRadioSource FindSameLinkedStationRadioSource(ISignal signal);

        IReadOnlyCollection<IRadioSource> HidedRadioSources { get; }
        void PerformAction(SignalAction action, int[] signalsId);
    }
}
