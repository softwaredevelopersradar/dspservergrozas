﻿using System;
using DspDataModel.Storages;
using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel.Tasks;

namespace DspDataModel.Database
{
    public interface IDatabaseController
    {
        bool IsConnected { get; }

        event EventHandler<IEnumerable<Filter>> IntelligenceFiltersReceivedEvent;
        event EventHandler<IReadOnlyList<FrequencyRange>> KnownRangesReceivedEvent;
        event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenRangesReceivedEvent;

        void RadioSourceTableAddRange(IEnumerable<IRadioSource> radioSources);
        void ClearRadioSourceTable();
        void DeleteRadioSourceRecord(int id);

        Task Connect();
        void Disconnect();

        void UpdateStationMode(DspServerMode mode);
    }
}
