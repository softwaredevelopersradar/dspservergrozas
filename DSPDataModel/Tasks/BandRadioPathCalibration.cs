﻿using System.Collections.Generic;

namespace DspDataModel.Tasks
{
    public class BandRadioPathCalibration
    {
        public IReadOnlyList<float[]> RadioPathPhaseDifferencies { get; }
        public IReadOnlyList<float[]> Amplitudes { get; }
        public float BandPhaseDeviation { get; }
        public int BandNumber { get; }

        public BandRadioPathCalibration(int bandNumber, IReadOnlyList<float[]> radioPathPhaseDifferencies, IReadOnlyList<float[]> amplitudes, float bandPhaseDeviation)
        {
            BandNumber = bandNumber;
            RadioPathPhaseDifferencies = radioPathPhaseDifferencies;
            Amplitudes = amplitudes;
            BandPhaseDeviation = bandPhaseDeviation;
        }
    }
}