﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel.Data;

namespace DspDataModel.Tasks
{
    public interface IReceiverTask
    {
        bool IsEndless { get; }
        bool IsReady { get; }
        object Result { get; }
        int AveragingCount { get; }
        int Priority { get; }
        bool AutoUpdateObjectives { get; }
        IReadOnlyList<Band> Objectives { get; }
        IReadOnlyList<IterationTask> IterationTasks { get; }
        Task HandleScan(IFpgaDataScan scan);
        void Cancel();
        void UpdateObjectives(IReadOnlyList<FrequencyRange> frequencyRanges);

        /// <summary>
        /// Do not use this method when task is endless
        /// </summary>
        /// <returns>Result of the task</returns>
        Task<object> WaitForResult();
        event EventHandler TaskEndEvent;
    }
}