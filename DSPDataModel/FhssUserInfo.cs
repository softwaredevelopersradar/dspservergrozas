﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DspDataModel.Data;

namespace DspDataModel
{
    /// <summary>
    /// Contains additional info about fhss network
    /// </summary>
    public class FhssUserInfo
    {
        public float Direction { get; private set; }
        public float Amplitude { get; private set; }
        public IPosition Position { get; private set; }
        public float DistanceKm { get; private set; }
        public float StandardDeviation { get; private set; }
    }
}
