﻿using System;
using System.Linq;
using NLog;

namespace DspDataModel
{
    public class NLogLogger : IMessageLogger
    {
        public ConsoleColor ErrorColor { get; set; } = ConsoleColor.Red;
        public ConsoleColor WarningColor { get; set; } = ConsoleColor.Yellow;
        public ConsoleColor MessageColor { get; set; } = ConsoleColor.Gray;

        private Logger ReceiversLogger { get; }

        private Logger FpgaLogger { get; }

        private Logger ShaperLogger { get; }

        private Logger ServerLogger { get; }

        private Logger TraceLogger { get; }

        public bool IsReceiversLogEnabled { get; set; }
        public bool IsFpgaLogEnabled { get; set; }
        public bool IsShaperLogEnabled { get; set; }
        public bool IsServerLogEnabled { get; set; }
        public bool IsTraceLogEnabled { get; set; }

        private Logger ErrorLogger { get; }

        public NLogLogger()
        {
            try
            {
                ReceiversLogger = LogManager.GetLogger("Receivers");
                FpgaLogger = LogManager.GetLogger("Fpga");
                ErrorLogger = LogManager.GetLogger("Error");
                ShaperLogger = LogManager.GetLogger("Shaper");
                ServerLogger = LogManager.GetLogger("Server");
                TraceLogger = LogManager.GetLogger("Trace");

                var logConfig = LogManager.Configuration;
                if (logConfig == null)
                {
                    return;
                }
                if (logConfig.Variables.ContainsKey(nameof(IsReceiversLogEnabled)))
                {
                    IsReceiversLogEnabled = logConfig.Variables[nameof(IsReceiversLogEnabled)].Text == "true";
                }
                if (logConfig.Variables.ContainsKey(nameof(IsFpgaLogEnabled)))
                {
                    IsFpgaLogEnabled = logConfig.Variables[nameof(IsFpgaLogEnabled)].Text == "true";
                }
                if (logConfig.Variables.ContainsKey(nameof(IsShaperLogEnabled)))
                {
                    IsShaperLogEnabled = logConfig.Variables[nameof(IsShaperLogEnabled)].Text == "true";
                }
                if (logConfig.Variables.ContainsKey(nameof(IsServerLogEnabled)))
                {
                    IsServerLogEnabled = logConfig.Variables[nameof(IsServerLogEnabled)].Text == "true";
                }
                if (logConfig.Variables.ContainsKey(nameof(IsTraceLogEnabled)))
                {
                    IsTraceLogEnabled = logConfig.Variables[nameof(IsTraceLogEnabled)].Text == "true";
                }
            }
            // something get wrong - nlog config file doesn't exist or nlog assembly is not loaded
            catch (Exception e)
            {
                // ignored
                // in this case all logging is switched off
            }
        }

        public void Log(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
        }

        public void FpgaLog(string message)
        {
            if (IsFpgaLogEnabled)
            {
                Log(FpgaLogger, message);
            }
        }

        public void ReceveirsLog(string seed, byte[] data)
        {
            if (IsReceiversLogEnabled)
            {
                Log(ReceiversLogger, seed, data);
            }
        }

        public void ShaperLog(string message)
        {
            if (IsShaperLogEnabled)
            {
                Log(ShaperLogger, message);
            }
        }

        public void ServerLog(string message)
        {
            if (IsServerLogEnabled)
            {
                Log(ServerLogger, message);
            }
        }

        /// <summary>
        /// Traces inner app methods
        /// </summary>
        public void Trace(string message, ConsoleColor color = ConsoleColor.Gray)
        {
            if (IsTraceLogEnabled)
            {
                Log(TraceLogger, message, color);
            }
        }

        private void Log(ILogger logger, string seed, byte[] data)
        {
            var message = data.Aggregate(seed, (s, b) => s + "\t" + b.ToString("X"));
            Log(logger, message);
        }

        private void Log(ILogger logger, string message, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            logger.Trace(message);
        }

        public void Log(string message)
        {
            Log(message, MessageColor);
        }

        public void Warning(string message)
        {
            Log(message, WarningColor);
        }

        public void Error(string message)
        {
            Log(message, ErrorColor);
            ErrorLogger.Error(message);
        }

        public void Error(Exception e)
        {
            Error(e, e.Message + "\n" + e.StackTrace);
        }

        public void Error(Exception e, string message)
        {
            Log($"{message}: {e.StackTrace}", ErrorColor);
            ErrorLogger.Error(e);
        }
    }
}