﻿using System.Collections.Generic;

namespace DspDataModel.Hardware
{
    public enum ReceiverChannel
    {
        Air = 0,
        NoiseGenerator = 1,
        ExternalCalibration = 2
    }

    public interface IReceiverManager
    {
        ITransmissionChannel DataChannel { get; }
        IReadOnlyList<IReceiver> Receivers { get; }
        ReceiverChannel Channel { get; }
        FpgaDeviceBandsSettingsSource BandsSettingsSource { get; }

        bool IsNoiseGeneratorEnabled { get; }
        bool IsExternalCalibrationEnabled { get; }

        bool Initialize();
        bool SetBandNumbers(IReadOnlyList<int> bandNumbers);
        bool SetBandNumbers(int bandNumber);
        /// <summary>
        /// returns if attenuator value has changed
        /// </summary
        bool SetAttenuatorValue(int bandNumber, bool isConstAttenuatorEnabled, float value);
        bool SetAttenuatorValue(int bandNumber, float value);
        bool SetConstAttenuatorValue(int bandNumber, bool isConstAttenuatorEnabled);

        void SetSynchronizationShift(int shift);
        void SetCycleLength(int shiftMs);
        short GetAmplitudeLevel(int receiverNumber);
        short GetTemperature(int receiverNumber);
        void SetReceiversChannel(ReceiverChannel channel);

        void ResetReceiversToDefault();

        void CollectFpgaDeviceBandsInformation();
    }
}
