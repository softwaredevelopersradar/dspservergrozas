﻿namespace DspDataModel.Hardware
{
    public interface ICompassManager
    {
        ICompass FirstCompass { get; }
        ICompass SecondCompass { get; }
    }
}
