﻿using System;
using System.Collections.Generic;
using DspDataModel.Data;
using DspDataModel.DataProcessor;

namespace DspDataModel.Hardware
{
    public interface IHardwareController : IScanReadTimer
    {
        IReceiverManager ReceiverManager { get; }
        IFpgaDeviceManager DeviceManager { get; }
        IUsrpReceiverManager UsrpReceiverManager { get; }
        ISignalGenerator SignalGenerator { get; }
        IGpsReceiver GpsReceiver { get; }
        ICompassManager CompassManager { get; }
        IReadOnlyList<FpgaDeviceBandSettings> FpgaDeviceBandsSettings { get; }
        IFpgaDataScan GetScan(IReadOnlyList<int> bandNumbers);
        bool Initialize();
        event EventHandler<IFpgaDataScan> ScanReadEvent;
    }
}
