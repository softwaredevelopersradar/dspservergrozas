﻿namespace DspDataModel.Hardware
{
    public interface ICompass
    {
        bool IsWorking { get; }

        double GetPitch();
        double GetRoll();
        double GetHeading();
    }
}
