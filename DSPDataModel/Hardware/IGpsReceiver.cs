﻿using System;
using DspDataModel.Data;

namespace DspDataModel.Hardware
{
    public interface IGpsReceiver
    {
        bool IsConnected { get; }
        DateTime UtcTime { get; }
        DateTime LocalTime { get; }
        Position Location { get; }
        int NumberOfSatellites { get; }
        double AntennaHeight { get; }

        event EventHandler GpsConnectionLostEvent;
        event EventHandler<DateTime> UpdateLocalTimeEvent;

        bool Initialize();
    }

    public enum GpsSystemType
    {
        GN = 0,//i don't know what this suppose to mean
        Gps = 1,
        Glonass = 2
    }

    public delegate void UpdateLocationDelegate(IGpsReceiver receiver);
}
