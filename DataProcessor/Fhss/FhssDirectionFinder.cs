﻿using System;
using System.Collections.Generic;
using System.Linq;
using Phases;

namespace DataProcessor.Fhss
{
    public static class FhssDirectionFinder
    {
        public static List<float> CalculateDirections(
            IReadOnlyList<float> directions, 
            int sector = 15, 
            float globalRelativeThreshold = 0.5f,
            float localRelativeThreshold = 0.75f)
        {
            const int additionalThreshold = 7;

            var chart = CreateChart(directions, sector);

            var threshold = Utils.GetThreshold(chart, globalRelativeThreshold) + additionalThreshold;

            // all chart values are lower than threshold
            if (chart.All(c => c < threshold))
            {
                return new List<float>();
            }

            if (chart.All(c => c >= threshold))
            {
                var direction = PhaseMath.AveragePhase(directions);
                return new List<float> {direction};
            }

            var startSplitIndex = GetStartRangeSplitIndex();
            var endSplitIndex = (startSplitIndex + 359) % 360;

            var ranges = SplitInRanges(chart, threshold, startSplitIndex, endSplitIndex).ToList();
            var result = new List<float>();

            foreach (var range in ranges)
            {
                var subChart = GetSubChart(range);
                var maxChartValue = subChart.Max();
                var localThreshold = (uint) (maxChartValue * localRelativeThreshold);
                var subRanges = SplitInRanges(chart, localThreshold, range.From, range.To);
                
                foreach (var subRange in subRanges)
                {
                    var direction = PhaseMath.AveragePhase(directions, subRange.From, subRange.To - 1);
                    result.Add(direction);
                }
            }

            return result;

            int GetStartRangeSplitIndex()
            {
                for (var i = 359; i > 0; --i)
                {
                    if (chart[i] < threshold)
                    {
                        return i;
                    }
                }

                return 0;
            }

            IReadOnlyList<uint> GetSubChart(Range range)
            {
                var end = (range.To + sector) % 360;
                if (range.From < end)
                {
                    return new ArraySegment<uint>(chart, range.From, range.To - range.From);
                }

                if (range.To < range.From && end >= range.From)
                {
                    return chart;
                }

                var size = 360 - range.From + end;
                var subChart = new uint[size];

                var index = 0;
                for (var i = range.From; i != end; UpdateIndex(ref i), ++index)
                {
                    subChart[index] = chart[i];
                }

                return subChart;
            }
        }

        private static uint[] CreateChart(IReadOnlyList<float> directions, int sector)
        {
            var chart = PhaseMath.CreatePhaseChart(directions);
            var result = new uint[360];
            result[0] = PhaseCount(chart, sector);

            var secondIndex = (sector + 1) % 360;
            
            for (var i = 1; i < 360; ++i)
            {
                result[i] = result[i - 1] + chart[secondIndex] - chart[i - 1];
                UpdateIndex(ref secondIndex);
            }

            return result;
        }

        private static void UpdateIndex(ref int index)
        {
            if (index == 359)
            {
                index = 0;
            }
            ++index;
        }

        private static IEnumerable<Range> SplitInRanges(uint[] chart, uint threshold, int startIndex = 0, int endIndex = 359)
        {
            var count = 0;
            for (var i = startIndex; i != endIndex; UpdateIndex(ref i))
            {
                count++;
                //some data can make this loop infinite!!
                if (count > 1000)
                    break;
                if (chart[i] < threshold)
                {
                    continue;
                }
                var rangeEndIndex = GetRangeEndIndex(i);
                yield return new Range(i, rangeEndIndex);

                i = rangeEndIndex;
                if (i == endIndex)
                {
                    break;
                }
            }

            int GetRangeEndIndex(int rangeStartIndex)
            {
                var rangeEndIndex = (rangeStartIndex + 359) % 360;
                for (var i = rangeStartIndex; i != rangeEndIndex; UpdateIndex(ref i))
                {
                    if (chart[i] < threshold)
                    {
                        return i;
                    }
                }

                return (startIndex + 359) % 360;
            }
        }

        /// <summary>
        /// returns phase count in the chart from direction 0 to direction "sector"
        /// </summary>
        private static uint PhaseCount(uint[] chart, int sector)
        {
            if (sector > 360)
                sector = 360;
            
            uint result = 0;
            for (var i = 0; i != sector + 1; ++i)
            {
                result += chart[i];
            }
            return result;
        }
    }
}