﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DspDataModel;
using Phases;
using Settings;

namespace DataProcessor.Fhss
{
    public class FhssNetwork : IFhssNetwork
    {
        public float MinFrequencyKhz { get; private set; }
        public float MaxFrequencyKhz { get; private set; }
        public float BandwidthKhz { get; private set; }
        public float StepKhz { get; private set; }
        public float Amplitude { get; private set; }
        public int FrequenciesCount { get; private set; }
        public float ImpulseDurationMs { get; private set; }
        public bool IsDurationMeasured { get; private set; }
        public int Id { get; }
        public bool IsActive { get; private set; }
        public DateTime LastUpdateTime { get; private set; }

        public IReadOnlyList<float> Directions { get; private set; }
        public IReadOnlyDictionary<int, IReadOnlyList<float>> LinkedInfo => _linkedInfo;
        public IReadOnlyList<FixedRadioSource> FixedRadioSources { get; private set; }

        private readonly Dictionary<int, IReadOnlyList<float>> _linkedInfo;
        private static int _idCounter = 0;

        public FhssNetwork(float minFrequencyKhz, float maxFrequencyKhz, float bandwidthKhz, float stepKhz, float amplitude, 
            float impulseDurationMs, bool isDurationMeasured, int frequenciesCount, IEnumerable<float> directions, IEnumerable<FixedRadioSource> fixedRadioSources)
        {
            _linkedInfo = new Dictionary<int, IReadOnlyList<float>>(10);//todo : to constants as max station coutn
            MinFrequencyKhz = minFrequencyKhz;
            MaxFrequencyKhz = maxFrequencyKhz;
            BandwidthKhz = bandwidthKhz;
            StepKhz = stepKhz;
            Amplitude = amplitude;
            ImpulseDurationMs = impulseDurationMs;
            FrequenciesCount = frequenciesCount;
            Directions = directions?.ToArray() ?? new float[0];
            FixedRadioSources = fixedRadioSources?.ToArray() ?? new FixedRadioSource[0];
            IsActive = true;
            IsDurationMeasured = isDurationMeasured;
            LastUpdateTime = DateTime.Now;
            Id = Interlocked.Increment(ref _idCounter);
        }

        public void Update(IFhssNetwork network)
        {
            if (network == null)
            {
                IsActive = false;
                return;
            }

            var comparisonResult = CompareNetworks(this, network);
            var newDirections = Directions.ToList();
            switch (comparisonResult)
            {
                case FhssComparisonResult.DifferentNetworks: return;//why would we even try to do that?
                case FhssComparisonResult.SameNetworks:
                    foreach (var direction in network.Directions) 
                    {
                        for (int i = 0; i < Directions.Count; i++)
                        {
                            if (PhaseMath.Angle(Directions[i], direction) <= Config.Instance.FhssSearchSettings.DirectionSector)
                            {
                                newDirections[i] = direction; //update value
                                break;
                            }
                        }
                    }
                    Directions = newDirections;
                    break;
                case FhssComparisonResult.SameNetworksDifferentUsers:
                    foreach (var direction in network.Directions)
                    {
                        var counter = 1;
                        for (int i = 0; i < Directions.Count; i++)
                        {
                            if (PhaseMath.Angle(Directions[i], direction) <= Config.Instance.FhssSearchSettings.DirectionSector)
                            {
                                newDirections[i] = direction; //update value
                                break;
                            }
                            counter++;
                        }
                        if (counter == Directions.Count)
                            newDirections.Add(direction);
                    }
                    Directions = newDirections;
                    break;
            }

            IsActive = true;
            MinFrequencyKhz = network.MinFrequencyKhz;
            MaxFrequencyKhz = network.MaxFrequencyKhz;
            StepKhz = network.StepKhz;
            Amplitude = network.Amplitude;
            LastUpdateTime = network.LastUpdateTime;
            FixedRadioSources = network.FixedRadioSources;
            FrequenciesCount = network.FrequenciesCount;
            if (IsDurationMeasured == false) 
            {
                //no remeasuring when network changed, because network changes frequently and we don't want to remeasure all the time
                ImpulseDurationMs = network.ImpulseDurationMs;
                IsDurationMeasured = network.IsDurationMeasured;
            }
        }

        public void RemeasureDuration()
        {
            IsDurationMeasured = false;
        }

        /// <summary>
        /// This operation is not COMMUTATIVE! (means, a * b != b * a)! 
        /// Network2 is considered as a condidate to be a network1 - which is valid by default
        /// </summary>
        public static FhssComparisonResult CompareNetworks(IFhssNetwork network1, IFhssNetwork network2)
        {
            var range1 = new FrequencyRange(network1.MinFrequencyKhz, network1.MaxFrequencyKhz);
            var range2 = new FrequencyRange(network2.MinFrequencyKhz, network2.MaxFrequencyKhz);
            var areFrequenciesTheSame = FrequencyRange.IntersectionFactor(range1, range2) >= Constants.FhssIntersectionThreshold;
            if (!areFrequenciesTheSame)
                return FhssComparisonResult.DifferentNetworks;

            var areDirectionsTheSame = network2.Directions
                .Select(d2 => network1.Directions
                .Any(d1 => PhaseMath.Angle(d1, d2) <= Config.Instance.FhssSearchSettings.DirectionSector))
                .All(r => r);
            if (areDirectionsTheSame)
                return FhssComparisonResult.SameNetworks;
            else
                return FhssComparisonResult.SameNetworksDifferentUsers;
        }
    }
}
