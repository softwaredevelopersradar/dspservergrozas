﻿using System.Collections.Generic;
using System.Linq;
using DspDataModel;

namespace DataProcessor.Fhss
{
    internal class DirectionNetwork
    {
        public List<ImpulseSignal> Signals { get; }
        public float Direction { get; }
        public FrequencyRange Range { get; }

        public DirectionNetwork(List<ImpulseSignal> signals, float direction)
        {
            Signals = signals;
            Direction = direction;

            var startFrequency = Signals.Min(s => s.Signal.FrequencyKhz);
            var endFrequency = Signals.Max(s => s.Signal.FrequencyKhz);
            Range = new FrequencyRange(startFrequency, endFrequency);
        }
    }
}