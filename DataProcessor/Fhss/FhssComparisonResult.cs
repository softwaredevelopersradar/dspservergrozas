﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProcessor.Fhss
{
    public enum FhssComparisonResult : byte
    {
        DifferentNetworks = 0,
        /// <summary>
        /// When two networks are the same, but their directions differ.
        /// Which means there are 2+ users of the network
        /// </summary>
        SameNetworksDifferentUsers = 1,
        SameNetworks = 2
    }
}
