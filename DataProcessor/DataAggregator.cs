﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataProcessor.AmplitudeCalculators;
using DspDataModel;
using DspDataModel.AmplitudeCalulators;
using Settings;
using DspDataModel.Data;
using Phases;

namespace DataProcessor
{
    public class DataAggregator : IDataAggregator
    {
        private readonly IReceiverScanAggregator[] _receiverScanAggregators;

        public int DataCount { get; private set; }

        private readonly int[] _receiversIndexes;

        private readonly bool _flushOnBandNumberChanged;

        private readonly IAmplitudeCalculator _amplitudeCalculator;

        public DataAggregator(int[] receiversIndexes, bool flushOnBandNumberChanged)
        {
            _receiversIndexes = receiversIndexes;
            _receiverScanAggregators = new IReceiverScanAggregator[receiversIndexes.Length];
            _flushOnBandNumberChanged = flushOnBandNumberChanged;
            _amplitudeCalculator = AmplitudeCalculatorFactory.Instance.CreateCalculator();

            for (var i = 0; i < _receiverScanAggregators.Length; ++i)
            {
                _receiverScanAggregators[i] = new ReceiverScanAggregator(flushOnBandNumberChanged);
            }
        }

        public void AddScan(IFpgaDataScan fpgaDataScan)
        {
            for (int i = 0; i < _receiversIndexes.Length; ++i)
            {
                var scan = fpgaDataScan.Scans[_receiversIndexes[i]];

                if (_flushOnBandNumberChanged)
                {
                    if (_receiverScanAggregators[i].Scans.Count > 0 && scan.BandNumber != _receiverScanAggregators[i].Scans[0].BandNumber)
                    {
                        Clear();
                        i = -1;
                        continue;
                    }
                }

                _receiverScanAggregators[i].AddScan(scan);
            }
            ++DataCount;
        }

        public void Clear()
        {
            foreach (var receiverBandData in _receiverScanAggregators)
            {
                receiverBandData.Clear();
            }
            DataCount = 0;
        }

        public IDataScan GetDataScan()
        {
            var scans = _receiverScanAggregators.Select(rc => rc.Scans).ToArray();
            return new DataScan(scans, _amplitudeCalculator);
        }

        public IDataScan GetDataScan(int scanIndex)
        {
            var scans = _receiverScanAggregators.Select(rc => new[] { rc.Scans[scanIndex] }).ToArray();
            return new DataScan(scans, _amplitudeCalculator);
        }

        private (List<float>[] Phases, float[] Amplitudes) CalculateCalibrationData(int startPoint, int endPoint)
        {
            var pointCount = endPoint - startPoint;
            var data = new List<float>[Constants.PhasesDifferencesCount];
            var scanCount = _receiverScanAggregators[0].Scans.Count;
            for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
            {
                data[i] = new List<float>(scanCount * pointCount);
            }
            var thresholds = GetThresholdLevels(startPoint, endPoint, out var noiseLevels);

            for (var scanIndex = 0; scanIndex < scanCount; ++scanIndex)
            {
                var k = 0;
                for (var i = 0; i < Constants.DfReceiversCount; ++i)
                {
                    for (var j = i + 1; j < Constants.DfReceiversCount; ++j)
                    {
                        var scanAmplitudesI = _receiverScanAggregators[i].Scans[scanIndex].Amplitudes;
                        var scanAmplitudesJ = _receiverScanAggregators[j].Scans[scanIndex].Amplitudes;

                        var scanPhasesI = _receiverScanAggregators[i].Scans[scanIndex].Phases;
                        var scanPhasesJ = _receiverScanAggregators[j].Scans[scanIndex].Phases;


                        for (var pointIndex = startPoint; pointIndex < endPoint; ++pointIndex)
                        {
                            if (scanAmplitudesI[pointIndex] > thresholds[i] && scanAmplitudesJ[pointIndex] > thresholds[j])
                            {
                                data[k].Add(PhaseMath.NormalizePhase(scanPhasesI[pointIndex] - scanPhasesJ[pointIndex]));
                            }
                        }
                        ++k;
                    }
                }
            }
            return (data, noiseLevels);
        }

        private (List<float>[] Phases, float[] Amplitudes) CalculateCalibrationBySignalData(int index, int scanOffset, int scanCount)
        {
            var data = new List<float>[Constants.PhasesDifferencesCount];
            var amplitudes = new float[Constants.DfReceiversCount];

            for (var i = 0; i < data.Length; i++)
            {
                data[i] = new List<float>();
            }

            for (var scanIndex = scanOffset; scanIndex < scanOffset + scanCount; ++scanIndex)
            {
                var k = 0;
                for (var i = 0; i < Constants.DfReceiversCount; ++i)
                {
                    amplitudes[i] += _receiverScanAggregators[i].Scans[scanIndex].Amplitudes[index];

                    for (var j = i + 1; j < Constants.DfReceiversCount; ++j)
                    {
                        var scanPhasesI = _receiverScanAggregators[i].Scans[scanIndex].Phases;
                        var scanPhasesJ = _receiverScanAggregators[j].Scans[scanIndex].Phases;

                        data[k].Add(PhaseMath.NormalizePhase(scanPhasesI[index] - scanPhasesJ[index]));
                        ++k;
                    }
                }
            }
            for (var i = 0; i < Constants.DfReceiversCount; ++i)
            {
                amplitudes[i] /= scanCount;
            }
            return (data, amplitudes);
        }

        private float[] GetThresholdLevels(int startPoint, int endPoint, out float[] noiseLevels)
        {
            var result = new float[Constants.DfReceiversCount];
            noiseLevels = new float[Constants.DfReceiversCount];

            for (var i = 0; i < result.Length; ++i)
            {
                result[i] = GetThresholdLevel(i, startPoint, endPoint, out var noiseLevel);
                noiseLevels[i] = noiseLevel;
            }
            return result;
        }

        private float GetThresholdLevel(int receiverIndex, int startPoint, int endPoint, out float noiseLevel)
        {
            var values = new int[256];
            var scanCount = _receiverScanAggregators[0].Scans.Count;

            for (var i = 0; i < scanCount; ++i)
            {
                var amplitudes = _receiverScanAggregators[receiverIndex].Scans[i].Amplitudes;
                for (var pointIndex = startPoint; pointIndex < endPoint; ++pointIndex)
                {
                    var amplitude = amplitudes[pointIndex];
                    values[(byte)-amplitude]++;
                }
            }

            var sum = 0;
            var index = 0;
            var threshold = (endPoint - startPoint) * scanCount * 3 / 4; // 4-quantile
            noiseLevel = float.MinValue;

            for (var i = values.Length - 1; i >= 0; --i)
            {
                sum += values[i];
                if (noiseLevel == float.MinValue && sum > threshold)
                {
                    noiseLevel = -i;
                }
                if (sum > threshold)
                {
                    index = i;
                    break;
                }
            }
            return -index;
        }

        private static float[] AverageCalibrationData(List<float>[] data, int offset, float[] phaseDeviations, int sector = 30, bool calculateDeviation = true)
        {
            const float maxDeviation = 180;
            const float maxDiscardedPhasesPart = 0.5f;

            var result = new float[Constants.PhasesDifferencesCount];

            for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
            {
                var filteredData = PhaseMath.FilterPhases(data[i], sector, out var discardedPhasesPart).ToArray();
                result[i] = PhaseMath.AveragePhase(filteredData);

                float deviation = 0;
                //we skip deviation calculation when frequency is less than 30 MHz or more than 3000 MHz
                //because receivers don't work there properly
                if (calculateDeviation) 
                    deviation = discardedPhasesPart < maxDiscardedPhasesPart
                        ? PhaseMath.StandardDeviation(filteredData)
                        : maxDeviation;

                if (deviation > phaseDeviations[offset])
                {
                    phaseDeviations[offset] = deviation;
                }
            }
            return result;
        }

        public BandCalibrationResult GetCalibrationData(float stepKhz)
        {
            var partsCount = (int)(Constants.BandwidthKhz / stepKhz);
            var phaseDeviations = new float[partsCount];
            var result = new float[partsCount][];
            var amplitudes = new float[partsCount][];

            Parallel.For(0, partsCount, i =>
            {
                var startPoint = Utilities.GetSampleNumber(Constants.FirstBandMinKhz + stepKhz * i, 0);
                var endPoint = Utilities.GetSampleNumber(Constants.FirstBandMinKhz + stepKhz * (i + 1), 0);
                var (data, noiseLevels) = CalculateCalibrationData(startPoint, endPoint);
                amplitudes[i] = noiseLevels;
                result[i] = AverageCalibrationData(data, i, phaseDeviations);
            });

            var calibration = new BandCalibrationResult(result, amplitudes, phaseDeviations);
            return PostProcessCalibration(calibration);
        }

        public BandCalibrationResult GetCalibrationDataBySignal(float startFrequencyKhz, float stepKhz, int scanCount)
        {
            var partsCount = (int)(Constants.BandwidthKhz / stepKhz);
            var result = new float[partsCount][];
            var amplitudes = new float[partsCount][];
            var phaseDeviations = new float[partsCount];

            for (var partIndex = 0; partIndex < partsCount; ++partIndex)
            {
                var index = GetSampleNumber(partIndex);
                var (phases, currentAmplitudes) = CalculateCalibrationBySignalData(index, partIndex * scanCount, scanCount);
                amplitudes[partIndex] = currentAmplitudes;
                result[partIndex] = AverageCalibrationData(phases, partIndex, phaseDeviations, sector: 10);

                var frequency = startFrequencyKhz + partIndex * stepKhz;

                if(frequency >= Constants.ReceiverMinWorkFrequencyKhz && frequency <= Constants.ReceiverMaxWorkFrequencyKhz)
                    result[partIndex] = AverageCalibrationData(phases, partIndex, phaseDeviations, sector: 10);
                else
                    result[partIndex] = AverageCalibrationData(phases, partIndex, phaseDeviations, sector: 10, calculateDeviation: false);
            }

            return new BandCalibrationResult(result, amplitudes, phaseDeviations);
            
            // get sample number of point with signal (point with max amplitude)
            int GetSampleNumber(int partIndex)
            {
                var frequency = startFrequencyKhz + stepKhz * partIndex;
                var pointNumber = Utilities.GetSampleNumber(frequency);

                var start = Math.Max(0, pointNumber - 5);
                var end = Math.Min(Constants.BandSampleCount, pointNumber + 5);
                var currentAmplitudes = new float[end - start];

                for (var i = start; i < end; ++i)
                {
                    for (var j = 0; j < Constants.DfReceiversCount; ++j)
                    {
                        for (var scanIndex = partIndex * scanCount; scanIndex < (1 + partIndex) * scanCount; ++scanIndex)
                        {
                            currentAmplitudes[i - start] += _receiverScanAggregators[j].Scans[scanIndex].Amplitudes[i];
                        }
                    }
                }
                var maxIndex = 0;
                for (var i = 1; i < end - start; ++i)
                {
                    if (currentAmplitudes[i] > currentAmplitudes[maxIndex])
                    {
                        maxIndex = i;
                    }
                }

                return maxIndex + start;
            }
        }

        //todo : remove this - like 99.9 %
        private BandCalibrationResult PostProcessCalibration(BandCalibrationResult calibration)
        {
            const float phaseDeviationThreshold = 25;
            const float singlePhaseEjectionThreshold = 15;
            const float phaseEjectionThreshold = 35;

            var phases = calibration.Phases.ToArray();
            var amplitudes = calibration.Amplitudes.ToArray();
            var deviations = calibration.PhaseDeviations.ToArray();

            FilterPhaseDeviationsPeaks();
            FilterPhaseEjections();
            FilterSinglePhaseEjections();

            return new BandCalibrationResult(phases, amplitudes, deviations);

            // removes single phase deviation problems according to phase deviation threhsold
            void FilterPhaseDeviationsPeaks()
            {
                for (var i = 0; i < deviations.Length; ++i)
                {
                    if (deviations[i] > phaseDeviationThreshold)
                    {
                        if (i == 0)
                        {
                            if (deviations[i + 1] < phaseDeviationThreshold)
                            {
                                deviations[0] = deviations[1];
                                phases[0] = phases[1];
                                amplitudes[0] = amplitudes[1];
                            }
                        }
                        else if (i == deviations.Length - 1)
                        {
                            if (deviations[i - 1] < phaseDeviationThreshold)
                            {
                                deviations[i] = deviations[i - 1];
                                phases[i] = phases[i - 1];
                                amplitudes[i] = amplitudes[i - 1];
                            }
                        }
                        else
                        {
                            if (deviations[i - 1] < phaseDeviationThreshold && deviations[i + 1] < phaseDeviationThreshold)
                            {
                                deviations[i] = (deviations[i - 1] + deviations[i + 1]) / 2;
                                amplitudes[i] = amplitudes[i - 1];
                                for (var j = 0; j < phases[i].Length; ++j)
                                {
                                    phases[i][j] = PhaseMath.AveragePhase(new[] { phases[i - 1][j], phases[i + 1][j] });
                                }
                            }
                        }
                    }
                }
            }

            // removes single phase deviation problems according to distance to it's average neighbours phase
            void FilterSinglePhaseEjections()
            {
                var maxIndex = phases[0].Length;
                
                for (var i = 1; i < phases.Length - 1; ++i) //TODO : check if phases[phases.Length - 1] is fixed or not.. probably not, but then find out is it the one screwed up
                {
                    for (var j = 0; j < maxIndex; ++j)
                    {
                        var arePhasesOk = PhaseMath.Angle(phases[i - 1][j], phases[i + 1][j]) < singlePhaseEjectionThreshold;//TODO : check what happens when phases ARE NOT OKAy
                        if (arePhasesOk)
                        {
                            var averagePhase = PhaseMath.AveragePhase(new[] {phases[i - 1][j], phases[i + 1][j]});
                            if (PhaseMath.Angle(phases[i][j], averagePhase) > singlePhaseEjectionThreshold)
                            {
                                phases[i][j] = averagePhase;
                            }
                        }
                    }
                }
            }

            // removes block of phases with problems according to distance to average phase in band and
            void FilterPhaseEjections()
            {
                var maxIndex = phases[0].Length;
                var currentPhases = new float[phases.Length];

                for (var i = 0; i < maxIndex; ++i)
                {
                    for (var j = 0; j < phases.Length; ++j)
                    {
                        currentPhases[j] = phases[j][i];
                    }

                    var averagePhase = PhaseMath.CalculatePhase(currentPhases);
                    for (var j = 0; j < currentPhases.Length; ++j)
                    {
                        if (PhaseMath.Angle(averagePhase, phases[j][i]) > phaseEjectionThreshold)
                        {
                            phases[j][i] = averagePhase;
                        }
                    }
                }
            }
        }

        public IAmplitudeScan GetSpectrum()
        {
            if (_receiverScanAggregators.Length == 1)
            {
                return _receiverScanAggregators[0].GetSpectrum();
            }
            var scans = _receiverScanAggregators.SelectMany(aggregator => aggregator.Scans).ToArray();
            return scans.MergeSpectrum();
        }

        public IReadOnlyList<IDataScan> GetSpectrums()
        {
            return _receiverScanAggregators.Select(aggregator => new DataScan(new[] { aggregator.Scans }, _amplitudeCalculator)).ToArray();
        }
    }
}
