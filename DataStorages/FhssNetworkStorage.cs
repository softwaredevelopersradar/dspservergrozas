﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DataProcessor.Fhss;
using DspDataModel;
using DspDataModel.Database;
using DspDataModel.Storages;
using SharpExtensions;

namespace DataStorages
{
    public class FhssNetworkStorage : IFhssNetworkStorage
    {
        public IDatabaseController DatabaseController { get; }

        public IReadOnlyCollection<IFhssNetwork> HidedFhssNetworks => _hidedFhssNetworks;

        private HashSet<IFhssNetwork> _hidedFhssNetworks;

        private readonly HashSet<int> _hidedIds;
        private readonly ConcurrentDictionary<int, IFhssNetwork> _data;
        private readonly IFilterManager _filterManager;
        private readonly Stopwatch SendToDatabaseWatch;

        public FhssNetworkStorage(IFilterManager filterManager, IDatabaseController databaseController) : this(filterManager, databaseController, Config.Instance.StoragesSettings.OldRadioSourcesCollectTimeSpan)
        { }

        public FhssNetworkStorage(IFilterManager filterManager, IDatabaseController databaseController, TimeSpan oldRadioSourcesCollectTimeSpan)
        {
            _filterManager = filterManager;
            _data = new ConcurrentDictionary<int, IFhssNetwork>();
            _hidedIds = new HashSet<int>();
            _hidedFhssNetworks = new HashSet<IFhssNetwork>();
            DatabaseController = databaseController;
            SendToDatabaseWatch = new Stopwatch();
            SendToDatabaseWatch.Start();

            PeriodicTask.Run(CollectOldRadioSources, oldRadioSourcesCollectTimeSpan);
        }

        public void PerformAction(SignalAction action, int[] signalsId)
        {
            switch (action)
            {
                case SignalAction.Hide:
                    foreach (var id in signalsId)
                    {
                        _hidedIds.Add(id);
                    }
                    break;
                case SignalAction.Restore:
                    foreach (var id in signalsId)
                    {
                        _hidedIds.Remove(id);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }

            _hidedFhssNetworks = new HashSet<IFhssNetwork>(
                GetFhssNetworksInner()
                    .Where(r => _hidedIds.Contains(r.Id))
            );

            //update database records after hiding/restoring
            var sources = GetFhssNetworks();
        }

        /// <summary>
        /// This method is running in additional thread endlessly in cycle with Config.Instance.OldRadioSourcesCollectTimeSpan delay
        /// </summary>
        private void CollectOldRadioSources()
        {
            var now = DateTime.Now;
            try
            {
                _data.Where(network => now - network.Value.LastUpdateTime > Config.Instance.StoragesSettings.OldRadioSourceTimeSpan)
                    .Do(network =>
                    {
                        _data.TryRemove(network.Key, out _);
                    });
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Collecting fhss network error.");
            }

        }

        public void Put(IEnumerable<IFhssNetwork> networks)
        {
            try
            {
                foreach (var network in networks)
                {
                    Put(network);
                }

                if (SendToDatabaseWatch.Elapsed.TotalMilliseconds >= Config.Instance.StoragesSettings.UpdateFrsTablePeriodMs)
                {
                    SendToDatabaseWatch.Restart();
                    var sources = GetFhssNetworks();
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Adding fhss network error.");
            }
        }

        public void Clear()
        {
            _data.Clear();
            _hidedIds.Clear();
            _hidedFhssNetworks = new HashSet<IFhssNetwork>();
        }

        public IEnumerable<IFhssNetwork> GetFhssNetworks()
        {
            return GetFhssNetworksInner()
                .Where(n => !_hidedIds.Contains(n.Id));
        }

        private IEnumerable<IFhssNetwork> GetFhssNetworksInner()
        {
            var bands = _filterManager.Bands;
            if (bands.IsNullOrEmpty())
            {
                return new IFhssNetwork[0];
            }
            return _data
                .Where(pair => _filterManager.IsInFilter(pair.Value))
                .Select(valuePair => valuePair.Value);
        }

        private void Put(IFhssNetwork network)
        {
            var existingNetwork = _data
                .Where(valuePair => 
                FhssNetwork.CompareNetworks(valuePair.Value, network) != FhssComparisonResult.DifferentNetworks)
                .Select(valuePair => valuePair.Value)
                .FirstOrDefault();
            if (existingNetwork == null)
            {
                _data.TryAdd(network.Id, network);
            }
            else
            {
                existingNetwork.Update(network);
            }
        }
    }
}
