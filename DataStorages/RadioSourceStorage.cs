﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using DataProcessor;
using Settings;
using DspDataModel;
using DspDataModel.Storages;
using SharpExtensions;
using DspDataModel.Database;
using System.Diagnostics;

namespace DataStorages
{
    using DataDictionary = ConcurrentDictionary<int, IRadioSource>;

    public class RadioSourceStorage : IRadioSourceStorage
    {
        private readonly List<DataDictionary> _data;
        private readonly IFilterManager _filterManager;
        private readonly HashSet<int> _hidedIds;
        private readonly Stopwatch _sendSignalsToDatabaseWatch = new Stopwatch();

        private HashSet<IRadioSource> _hidedRadioSources;

        public IDatabaseController DatabaseController { get; private set; }
        public IReadOnlyCollection<IRadioSource> HidedRadioSources => _hidedRadioSources;
        
        public RadioSourceStorage(IFilterManager filterManager, IDatabaseController databaseController) : this(filterManager, databaseController, Config.Instance.StoragesSettings.OldRadioSourcesCollectTimeSpan)
        { }

        public RadioSourceStorage(IFilterManager filterManager, IDatabaseController databaseController, TimeSpan oldRadioSourcesCollectTimeSpan)
        {
            DatabaseController = databaseController;
            _filterManager = filterManager;
            _hidedIds = new HashSet<int>();
            _hidedRadioSources = new HashSet<IRadioSource>();
            _data = new List<DataDictionary>(Config.Instance.BandSettings.BandCount);
            _data.AddRange(
                Config.Instance.BandSettings.BandCount.Repeat(() => new DataDictionary())
            );
            PeriodicTask.Run(CollectOldRadioSources, oldRadioSourcesCollectTimeSpan);
            _sendSignalsToDatabaseWatch.Start();
        }

        /// <summary>
        /// This method is running in additional thread endlessly in cycle with Config.Instance.OldRadioSourcesCollectTimeSpan delay
        /// </summary>
        private void CollectOldRadioSources()
        {
            var oldSources = new List<IRadioSource>();
            var now = DateTime.Now;
            var oldRadioSourceTimeSpan = Config.Instance.StoragesSettings.OldRadioSourceTimeSpan;
            try
            {
                foreach (var dictionary in _data)
                {
                    oldSources.Clear();
                    foreach (var valuePair in dictionary)
                    {
                        var source = valuePair.Value;
                        source.Update();
                        if (now - source.BroadcastStartTime - source.BroadcastTimeSpan > oldRadioSourceTimeSpan)
                        {
                            oldSources.Add(source);
                        }
                    }
                    RemoveSignals(dictionary, oldSources);
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Collecting old radio sources error.");
            }
            
        }

        private void SendSourcesToDatabase()
        {
            var sources = GetRadioSources();
            DatabaseController.RadioSourceTableAddRange(sources);
        }

        private static void RemoveSignals(DataDictionary dictionary, IEnumerable<IRadioSource> sources)
        {
            foreach (var source in sources)
            {
                dictionary.TryRemove(source.Id, out var _);
            }
        }

        private DataDictionary GetDataDictionary(float frequency)
        {
            return _data[Utilities.GetBandNumber(frequency)];
        }

        public void Put(IEnumerable<ISignal> signals)
        {
            try
            {
                foreach (var signal in signals)
                {
                    Put(signal);
                }

                if (_sendSignalsToDatabaseWatch.ElapsedMilliseconds > Config.Instance.StoragesSettings.UpdateFrsTablePeriodMs)
                {
                    SendSourcesToDatabase();
                    _sendSignalsToDatabaseWatch.Restart();
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Adding radio source to storage error.");
            }
        }

        public void PutLinkedStationSignals(IEnumerable<IRadioSource> signals)
        {
            try
            {
                foreach (var signal in signals)
                {
                    PutLinkedStationSignal(signal);
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Adding radio source to storage error.");
            }
        }

        public IEnumerable<IRadioSource> GetRadioSources()
        {
            return GetRadioSourcesInner()
                .Where(r => !_hidedIds.Contains(r.Id));
        }

        private IEnumerable<IRadioSource> GetRadioSourcesInner()
        {
            var bands = _filterManager.Bands;
            if (bands.IsNullOrEmpty())
            {
                return new IRadioSource[0];
            }
            var from = bands[0];
            var to = bands[bands.Count - 1];
            if (from.Number == to.Number)
            {
                return GetRadioSources(from.Number);
            }
            var sourcesLists = new List<IEnumerable<IRadioSource>>(bands.Count);

            foreach (var band in bands)
            {
                sourcesLists.Add(GetRadioSources(band.Number));
            }
            return sourcesLists.SelectMany(_ => _);
        }

        private IEnumerable<IRadioSource> GetRadioSources(int index)
        {
            var dictionary = _data[index];
            var result = new List<IRadioSource>();
            foreach (var valuePair in dictionary)
            {
                valuePair.Value.Update();
                if (_filterManager.IsInFilter(valuePair.Value))
                {
                    result.Add(valuePair.Value);
                }
            }
            return result;
        }

        public bool HasSameRadioSource(ISignal signal)
        {
            var dictionary = GetDataDictionary(signal.FrequencyKhz);
            return dictionary.Any(valuePair => valuePair.Value.IsSameSource(signal));
        }

        public void Clear()
        {
            foreach (var dictionary in _data)
            {
                dictionary.Clear();
            }
            _hidedIds.Clear();
            _hidedRadioSources = new HashSet<IRadioSource>();
            DatabaseController.ClearRadioSourceTable();
        }

        public int? GetSignalId(ISignal signal)
        {
            return FindSameRadioSource(signal)?.Id;
        }

        public IRadioSource FindSameRadioSource(ISignal signal)
        {
            var dictionary = GetDataDictionary(signal.FrequencyKhz);
            var radioSource = dictionary
                .Where(valuePair => valuePair.Value.IsSameSource(signal))
                .Select(valuePair => valuePair.Value)
                .FirstOrDefault();

            if (radioSource != null)
            {
                return radioSource;
            }

            var mergeableSources = dictionary
                .Where(valuePair => Signal.ShouldBeMerged(valuePair.Value, signal));
            if (mergeableSources.IsNullOrEmpty())
            {
                return null;
            }
            return mergeableSources
                .MinArg(valuePair => SignalExtensions.Distance(valuePair.Value, signal)).Value;
        }

        public void PerformAction(SignalAction action, int[] signalsId)
        {
            switch (action)
            {
                case SignalAction.Hide:
                    foreach (var id in signalsId)
                    {
                        _hidedIds.Add(id);
                        DatabaseController.DeleteRadioSourceRecord(id);
                    }
                    break;
                case SignalAction.Restore:
                    foreach (var id in signalsId)
                    {
                        _hidedIds.Remove(id);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }

            _hidedRadioSources = new HashSet<IRadioSource>(
                GetRadioSourcesInner()
                    .Where(r => _hidedIds.Contains(r.Id))
            );

            SendSourcesToDatabase();
        }

        public IRadioSource FindSameLinkedStationRadioSource(ISignal signal)
        {
            var dictionary = GetDataDictionary(signal.FrequencyKhz);
            return dictionary
                .Where(valuePair => valuePair.Value.IsSameLinkedStationSource(signal))
                .Select(valuePair => valuePair.Value)
                .FirstOrDefault();
        }

        public void Put(ISignal signal)
        {
            var source = FindSameRadioSource(signal);
            if (source == null)
            {
                var dictionary = GetDataDictionary(signal.FrequencyKhz);
                var radioSource = new RadioSource(signal);
                dictionary.TryAdd(radioSource.Id, radioSource);
            }
            else
            {
                source.Update(signal);
            }
        }

        private void PutLinkedStationSignal(IRadioSource signal)
        {
            var source = FindSameRadioSource(signal);
            if (source == null)
            {
                var dictionary = GetDataDictionary(signal.FrequencyKhz);
                dictionary.TryAdd(signal.Id, signal);
            }
            else
            {
                foreach (var linkedInfo in signal.LinkedInfo)
                {
                    source.UpdateLinkedStationDirection(linkedInfo.Key, linkedInfo.Value.direction, linkedInfo.Value.reliability);
                }
                source.Update(signal);
            }
        }
    }
}
