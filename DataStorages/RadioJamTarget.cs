﻿using System;
using DspDataModel.RadioJam;

namespace DataStorages
{
    public class RadioJamTarget : IRadioJamTarget
    {
        public float FrequencyKhz { get; set; }
        public int Priority { get; private set; }
        public float Threshold { get; private set; }
        public float Direction { get; private set; }
        public float Amplitude { get; set; }
        public bool UseAdaptiveThreshold { get; private set; }
        public int Id { get; private set; }
        public int Liter { get; private set; }

        public IRadioJamTargetConfig Config { get; }
        public byte ModulationCode { get; private set; }
        public byte DeviationCode { get; private set; }
        public byte ManipulationCode { get; private set; }
        public byte DurationCode { get; private set; }

        public TargetState ControlState { get; private set; }
        public TargetState JamState { get; private set; }
        public bool IsEmitted { get; private set; }

        private DateTime _controlStartTime = DateTime.MaxValue;
        private DateTime _jamStartTime = DateTime.MaxValue;

        private bool _isTargetControlled;
        private bool _isTargetJammed;

        public RadioJamTarget(float frequencyKhz, int priority, float threshold, float direction, 
            bool useAdaptiveThreshold, byte modulationCode, byte deviationCode, byte manipulationCode, 
            byte durationCode, int id, int liter, IRadioJamTargetConfig targetConfig)
        {
            Id = id;
            Liter = liter;
            FrequencyKhz = frequencyKhz;
            Priority = priority;
            Threshold = threshold;
            Direction = direction;
            UseAdaptiveThreshold = useAdaptiveThreshold;

            ModulationCode = modulationCode;
            DeviationCode = deviationCode;
            ManipulationCode = manipulationCode;
            DurationCode = durationCode;

            IsEmitted = false;
            Config = targetConfig;

            ClearStateFlags();
        }

        public RadioJamTarget(float frequencyKhz, int id)
        {
            Id = id;
            FrequencyKhz = frequencyKhz;

            ControlState = TargetState.NotActive;
            JamState = TargetState.NotActive;
            IsEmitted = false;
        }

        public bool UpdateControlState(bool isControlled)
        {
            var now = DateTime.UtcNow;
            var previousControlState = ControlState;

            if (_isTargetControlled != isControlled)
            {
                _isTargetControlled = isControlled;
                _controlStartTime = now;
                ControlState = _isTargetControlled ? TargetState.Active : TargetState.NotActive;
            }
            else
            {
                if (_controlStartTime == DateTime.MaxValue)
                {
                    _controlStartTime = now;
                    ControlState = _isTargetControlled ? TargetState.Active : TargetState.NotActive;
                }
                else
                {
                    var longWorkingSignalDuration = TimeSpan.FromMilliseconds(Config.LongWorkingSignalDurationMs);
                    if (isControlled)
                    {
                        ControlState = (now - _controlStartTime) < longWorkingSignalDuration
                            ? TargetState.Active
                            : TargetState.ActiveLongTime;
                    }
                    else
                    {
                        ControlState = (now - _controlStartTime) < longWorkingSignalDuration
                            ? TargetState.NotActive
                            : TargetState.NotActiveLongTime;
                    }
                }
            }

            return ControlState != previousControlState;
        }

        public bool UpdateJamState(bool isJammed)
        {
            var now = DateTime.UtcNow;
            var previousJamState = JamState;

            if (_isTargetJammed != isJammed)
            {
                _isTargetJammed = isJammed;
                _jamStartTime = now;
                JamState = _isTargetJammed ? TargetState.Active : TargetState.NotActive;
            }
            else
            {
                if (_jamStartTime == DateTime.MaxValue)
                {
                    _jamStartTime = now;
                    JamState = _isTargetJammed ? TargetState.Active : TargetState.NotActive;
                }
                else
                {
                    var longWorkingSignalDuration = TimeSpan.FromMilliseconds(Config.LongWorkingSignalDurationMs);
                    if (isJammed)
                    {
                        JamState = (now - _jamStartTime) < longWorkingSignalDuration
                            ? TargetState.Active
                            : TargetState.ActiveLongTime;
                    }
                    else
                    {
                        JamState = (now - _jamStartTime) < longWorkingSignalDuration
                            ? TargetState.NotActive
                            : TargetState.NotActiveLongTime;
                    }
                }
            }

            return JamState != previousJamState;
        }

        public bool UpdateEmitionState(bool newEmitionState)
        {
            var previousValue = IsEmitted;
            IsEmitted = newEmitionState;
            return previousValue != IsEmitted;
        }

        public void ClearStateFlags()
        {
            _controlStartTime = DateTime.MaxValue;
            _jamStartTime = DateTime.MaxValue;
            JamState = TargetState.NotActive;
            ControlState = TargetState.NotActive;
        }

        public void UpdateTarget(IRadioJamTarget newValue)
        {
            FrequencyKhz = newValue.FrequencyKhz;
            Priority = newValue.Priority;
            Threshold = newValue.Threshold;
            Direction = newValue.Direction;
            UseAdaptiveThreshold = newValue.UseAdaptiveThreshold;
            Id = newValue.Id;
            Liter = newValue.Liter;
            Amplitude = newValue.Amplitude;

            ModulationCode = newValue.ModulationCode;
            DeviationCode = newValue.DeviationCode;
            ManipulationCode = newValue.ManipulationCode;
            DurationCode = newValue.DurationCode;
        }

        public bool Equals(IRadioJamTarget other)
        {
            if (other == null)
            {
                return false;
            }
            return Math.Abs(FrequencyKhz - other.FrequencyKhz) < 1
                && Priority == other.Priority 
                && Math.Abs(Threshold - other.Threshold) < 0.5
                && Math.Abs(Direction - other.Direction) < 1
                && UseAdaptiveThreshold == other.UseAdaptiveThreshold 
                && Id == other.Id 
                && Liter == other.Liter 
                && ModulationCode == other.ModulationCode 
                && DeviationCode == other.DeviationCode
                && ManipulationCode == other.ManipulationCode 
                && DurationCode == other.DurationCode;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != GetType())
            {
                return false;
            }
            return Equals((RadioJamTarget) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = FrequencyKhz.GetHashCode();
                hashCode = (hashCode * 397) ^ Priority;
                hashCode = (hashCode * 397) ^ Threshold.GetHashCode();
                hashCode = (hashCode * 397) ^ Direction.GetHashCode();
                hashCode = (hashCode * 397) ^ UseAdaptiveThreshold.GetHashCode();
                hashCode = (hashCode * 397) ^ Id;
                hashCode = (hashCode * 397) ^ Liter;
                hashCode = (hashCode * 397) ^ ModulationCode.GetHashCode();
                hashCode = (hashCode * 397) ^ DeviationCode.GetHashCode();
                hashCode = (hashCode * 397) ^ ManipulationCode.GetHashCode();
                hashCode = (hashCode * 397) ^ DurationCode.GetHashCode();
                return hashCode;
            }
        }
    }
}
