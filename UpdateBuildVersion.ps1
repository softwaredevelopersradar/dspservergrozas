#Get Paths to csproj
$paths = @(
	"$PSScriptRoot\Correlator\Correlator.csproj",
	"$PSScriptRoot\DataProcessor\DataProcessor.csproj",
	"$PSScriptRoot\DataStorages\DataStorages.csproj",
	"$PSScriptRoot\DspClient\DspClient.csproj",
	"$PSScriptRoot\DSPDataModel\DSPDataModel.csproj",
	"$PSScriptRoot\DspProtocols\DspProtocols.csproj",
	"$PSScriptRoot\DspServer\DspServer.csproj",
	"$PSScriptRoot\HardwareLibrary\HardwareLibrary.csproj",
	"$PSScriptRoot\RadioRecorder\RadioRecorder.csproj",
	"$PSScriptRoot\TasksLibrary\TasksLibrary.csproj",
    "$PSScriptRoot\DspServerApp\DspServerApp.csproj",
    "$PSScriptRoot\LinkedStationLibrary\LinkedStationLibrary.csproj",
	"$PSScriptRoot\DataBaseLibrary\DataBaseLibrary.csproj"
)
$path = "$PSScriptRoot\version.xml"
$xml = [xml](Get-Content $path)

#Retieve Version Nodes
$AssemblyVersion = $xml.AssemblyVersion

$avMajor, $avMinor, $avBuild  = $AssemblyVersion.Split(".")


For ($i = 0; $i -lt $paths.Length; $i++) 
{
    #Read csproj (XML)
	$xml = [xml](Get-Content $paths[$i])
	
	#Put new version back into csproj (XML)
    $propertyGroups = $xml.SelectNodes("Project/PropertyGroup")
    foreach ($propertyGroup in $propertyGroups)
    {
        $propertyGroup.AssemblyVersion = "$avMajor.$avMinor.$avBuild"
        $propertyGroup.FileVersion     = "$avMajor.$avMinor.$avBuild"
    }
	
	#Save csproj (XML)
	$xml.Save($paths[$i])
}

$xml = [xml](Get-Content $path)

#Retieve Version Nodes
$AssemblyVersion = $xml.AssemblyVersion

$avMajor, $avMinor, $avBuild  = $AssemblyVersion.Split(".")
$avBuild = [Convert]::ToInt32($avBuild,10)+1
$xml.AssemblyVersion = "$avMajor.$avMinor.$avBuild"
$xml.Save($path)