﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataStorages;
using DataTransferClientLibrary;
using DataTransferModel.DataTransfer;
using DspDataModel;
using DspDataModel.Storages;
using DspDataModel.Tasks;

namespace DataTransferLibrary
{
    public class DataTransferController : DspDataModel.DataTransfer.IDataTransferController
    {
        private readonly DataTransferClient _client;

        public bool IsConnected { get; private set; }

        public event EventHandler<DspServerMode> SetModeRequest;
        public event EventHandler<(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)> PerformExecutiveDfRequest;
        public event EventHandler<(int stationId, IReadOnlyList<IRadioSource> signals)> SignalsReceivedEvent;
        public event EventHandler<(int stationId, IReadOnlyList<IRadioSource> signals)> SignalsResultsReceivedEvent;
        public event EventHandler<int> SetBandNumberRequest;
        public event EventHandler<DateTime> SetLocalTimeRequest;

        private readonly ManualResetEvent _executiveDfFinishedEvent = new ManualResetEvent(false);

        public DataTransferController()
        {
            _client = new DataTransferClient(Config.Instance.DataTransferSettings.DataTransferHost, Config.Instance.DataTransferSettings.DataTransferPort);
            _executiveDfResponseLock = new object();
            SubscribeToDataTransferServiceEvents();
        }

        private void SubscribeToDataTransferServiceEvents() 
        {
            _client.OnConnect += OnConnect;
            _client.OnDisconnect += OnDisconnect;
            _client.OnError += OnError;
        }

        private void SubscribeToDataTransferEvents()
        {            
            _client.OnModeReceived += OnModeReceived;
            _client.OnSetBandReceived += OnSetBandReceived;
            _client.OnLocalTimeReceived += OnLocalTimeReceived;
            
            _client.OnExecutiveDfResponseReceived += OnExecutiveDfResponseReceived;
            _client.OnExecutiveDfRequestReceived += OnExecutiveDfRequestReceived;

            _client.OnSlaveRdfResultReceived += OnSlaveRdfResultReceived;
            _client.OnMasterRdfResultReceived += OnMasterRdfResultReceived;
        }

        private void OnLocalTimeReceived(object sender, DateTime localTime)
        {
            SetLocalTimeRequest?.Invoke(this, localTime);
        }

        private void UnsubscribeToDataTransferEvents()
        {
            _client.OnModeReceived -= OnModeReceived;
            _client.OnSetBandReceived -= OnSetBandReceived;
            _client.OnLocalTimeReceived -= OnLocalTimeReceived;

            _client.OnExecutiveDfResponseReceived -= OnExecutiveDfResponseReceived;
            _client.OnExecutiveDfRequestReceived -= OnExecutiveDfRequestReceived;

            _client.OnSlaveRdfResultReceived -= OnSlaveRdfResultReceived;
            _client.OnMasterRdfResultReceived -= OnMasterRdfResultReceived;
        }
        private void OnMasterRdfResultReceived(object sender, RdfCycleResult result)
        {
            var sources = new List<IRadioSource>();
            foreach (var signal in result.Signals)
            {
                var dspSignal = new DataProcessor.Signal(
                        frequencyKhz: signal.FrequencyKhz,
                        centralFrequencyKhz: signal.FrequencyKhz,
                        direction: signal.Direction,
                        reliability: 1,
                        bandwidthKhz: signal.BandwidthKhz,
                        amplitude: signal.Amplitude,
                        standardDeviation: signal.StandardDeviation,
                        discardedDirectionsPart: signal.DiscardedDirectionsPart,
                        phaseDeviation: signal.PhaseDeviation,
                        relativeSubScanCount: signal.RelativeSubScanCount,
                        phases: new float[10],
                        broadcastTimeSpan: signal.BroadcastTimeSpan,
                        modulation: SignalModulation.Unknown);
                var source = new RadioSource(dspSignal, -1, signal.Latitude, signal.Longitude, signal.Altitude, DateTime.Now);
                sources.Add(source);
            }

            SignalsResultsReceivedEvent?.Invoke(this, (result.StationId, sources));
        }

        private void OnSlaveRdfResultReceived(object sender, RdfCycleResult result)
        {
            var sources = result.Signals.Select(
                       s => new RadioSource(
                           new DataProcessor.Signal(
                               frequencyKhz: s.FrequencyKhz,
                               centralFrequencyKhz: s.FrequencyKhz,
                               direction: s.Direction,
                               reliability: 1,
                               bandwidthKhz: s.BandwidthKhz,
                               amplitude: s.Amplitude,
                               standardDeviation: s.StandardDeviation,
                               discardedDirectionsPart: s.DiscardedDirectionsPart,
                               phaseDeviation: s.PhaseDeviation,
                               relativeSubScanCount: s.RelativeSubScanCount,
                               phases: new float[10],
                               broadcastTimeSpan: s.BroadcastTimeSpan,
                               modulation: SignalModulation.Unknown
                               ))).ToList();
            SignalsReceivedEvent?.Invoke(this, (result.StationId, sources));
        }

        private void OnExecutiveDfRequestReceived(object sender, ExecutiveDfRequestArgs args)
        {
            PerformExecutiveDfRequest?.Invoke(this, 
                (args.StartFrequencyKhz, 
                args.EndFrequencyKhz, 
                args.PhaseAveragingCount, 
                args.DirectionAveragingCount));
        }

        private void OnExecutiveDfResponseReceived(object sender, ExecutiveDfResponseArgs result)
        {
            if (_executiveDfResults.Count == 0)
                return;
            lock (_executiveDfResponseLock)
            {
                try
                {
                    _executiveDfEvents[result.StationId].Set();
                    _executiveDfResults[result.StationId] = result.Direction;

                    if (_executiveDfEvents.All(e => e.Value.WaitOne(0)))
                        _executiveDfFinishedEvent.Set();
                }
                catch (Exception e)
                {
                    MessageLogger.Log($"Received response is to late :{e.StackTrace}");
                }
            }
        }

        private void OnSetBandReceived(object sender, int bandNumber)
        {
            SetBandNumberRequest?.Invoke(this, bandNumber);
        }

        private void OnModeReceived(object sender, byte mode)
        {
            SetModeRequest?.Invoke(this, (DspServerMode)mode);
        }

        private void OnError(object sender, ErrorArgs e)
        {
            //todo
        }

        private void OnDisconnect(object sender, DisconnectedState e)
        {
            IsConnected = false;
            UnsubscribeToDataTransferEvents();
            MessageLogger.Log($"Disconnected from datatransfer, reason : {e}");
            Connect();
        }

        private void OnConnect(object sender, EventArgs e)
        {
            IsConnected = true;
            SubscribeToDataTransferEvents();
            MessageLogger.Log($"Connected to datatransfer " +
                                 $"{Config.Instance.DataTransferSettings.DataTransferHost}:" +
                                 $"{Config.Instance.DataTransferSettings.DataTransferPort}");
        }

        public void Connect()
        {
            Task.Run(async () =>
            {
                try
                {
                    while (!_client.IsConnected)
                    {
                        _client.Connect("DspServer");
                        await Task.Delay(100).ConfigureAwait(false);
                    }
                }
                catch (Exception e)
                {
                    //ignored
                }
            });
        }

        public void Disconnect()
        {
            if(IsConnected)
                _client.Disconnect();
        }

        public bool SetMode(DspServerMode mode)
        {
            if (!IsConnected)
                return false;
            _client.SendMode((byte)mode);
            return true;
        }

        //todo : add settings reference and change to constant
        private readonly Dictionary<int, float?> _executiveDfResults = new Dictionary<int, float?>(10);
        private readonly Dictionary<int, ManualResetEvent> _executiveDfEvents = new Dictionary<int, ManualResetEvent>(10);        
        private readonly object _executiveDfResponseLock;

        private void InitializeDictionaries(IReadOnlyList<int> stations)
        {
            foreach (var station in stations)
            {
                _executiveDfResults.Add(station, null);
                _executiveDfEvents.Add(station, new ManualResetEvent(false));
            }
        }

        private void ClearDictionaries()
        {
            _executiveDfResults.Clear();
            _executiveDfEvents.Clear();
            _executiveDfFinishedEvent.Reset();
        }

        public Dictionary<int, float?> PerformExecutiveDf(IReadOnlyList<int> stations, float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            if (!IsConnected)
                return new Dictionary<int, float?>();

            var stationsDirections = new Dictionary<int, float?>(stations.Count);
            InitializeDictionaries(stations);

            foreach (var station in stations)
            {
                Task.Run(() => PerformExecutiveDf(station, startFrequencyKhz, endFrequencyKhz, phaseAveragingCount, directionAveragingCount));
            }

            var executiveDfResult = _executiveDfFinishedEvent.WaitOne(5000);//todo : to config
            var results = new Dictionary<int, float?>(_executiveDfResults.Count);
            foreach (var record in _executiveDfResults)
            {
                results.Add(record.Key, record.Value);
            }
            ClearDictionaries();
            return results;
        }

        private void PerformExecutiveDf(int stationId, float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            if (!IsConnected)
                return;

            _client.SendExecutiveDfRequest(stationId, startFrequencyKhz, endFrequencyKhz, phaseAveragingCount, directionAveragingCount);
            var result = _executiveDfEvents[stationId].WaitOne(1000);//todo : to config
            if (result == false)
            {
                _executiveDfEvents[stationId].Set();
                _executiveDfResults[stationId] = null;
            }
        }
        
        public void SendExecutiveDfResponse(int stationId, float? frequencyKhz, float direction)
        {
            if (!IsConnected)
                return;

            _client.SendExecutiveDfResponse(stationId, direction);
        }

        public void SetBandNumber(int bandNumber)
        {
            if (!IsConnected)
                return;

            _client.SendBandNumber(bandNumber);
        }

        public void SendSignals(int stationId, IReadOnlyList<ISignal> signals, int bandNumber = -1)
        {
            if (!IsConnected)
                return;

            _client.SendSlaveRdfResult(new RdfCycleResult()
            {
                StationId = stationId,
                BandNumber = bandNumber,
                Signals = signals.Select(s =>
                    new Signal()
                    {
                        Amplitude = s.Amplitude,
                        Direction = s.Direction,
                        FrequencyKhz = s.FrequencyKhz,
                        StandardDeviation = s.StandardDeviation
                    }).ToArray()
            });
        }

        public void SendRdfResults(int stationId, IReadOnlyList<IRadioSource> results)
        {
            if (!IsConnected)
                return;

            _client.SendMasterRdfResult(new RdfCycleResult()
            {
                StationId = stationId, //todo :irrelevant, actually?
                BandNumber = -1,
                Signals = results.Select(s => new Signal()
                {
                    Direction = s.Direction,
                    FrequencyKhz = s.FrequencyKhz,
                    Altitude = (float)s.Altitude,
                    Longitude = (float)s.Longitude,
                    Latitude = (float)s.Latitude
                }).ToArray()
            });
        }

        public void SendLocalTime(DateTime localTime)
        {
            if (!IsConnected)
                return;

            _client.SendLocalTime(localTime);
        }
    }
}
