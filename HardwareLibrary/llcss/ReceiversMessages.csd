namespace ReceiversMessages
{
	struct SetBandRequest
	{
		7
		byte ReceiverNumber
		0x0F
		byte BandNumber
		0
		0
		0
	}

	struct SetBandResponse
	{
		7
		byte ReceiverNumber
		0x0F
		byte BandNumber
		byte AttenuatorValue
		byte AmplifierValue
		byte StateByte
	}

	struct EmptyMessage
	{
		3
		0
		0
	}

	struct SetNoiseGeneratorEnabledRequest
	{
		4
		9
		8
		byte IsEnabled
	}

	struct SetNoiseGeneratorEnabledResponse
	{
		4
		9
		8
		byte IsEnabled
	}

	struct SetConstAttenuatorEnabledRequest
	{
		4
		byte ReceiverNumber
		6
		byte IsEnabled
	}

	struct SetConstAttenuatorEnabledResponse
	{
		4
		byte ReceiverNumber
		6
		byte IsEnabled
	}

	struct SetFrequencyRequest
	{
		7
		byte ReceiverNumber
		1
		short CentralFrequencyMhz
		0
		0
	}

	struct SetFrequencyResponse
	{
		7
		byte ReceiverNumber
		1
		short CentralFrequencyMhz
		byte AttenuatorValue
		byte StateByte
	}

	struct SetAttenuatorValueRequest
	{
		4
		byte ReceiverNumber
		7
		byte Value
	}

	struct SetAttenuatorValueResponse
	{
		4
		byte ReceiverNumber
		7
		byte Value
	}

	struct SetCycleSynchronizationRequest
	{
		5
		0x0A
		0x35
		short CycleLength
	}

	struct SetCycleSynchronizationResponse
	{
		5
		0x0A
		0x35
		short CycleLength
	}

	struct SetSynchronizationShiftRequest
	{
		5
		0x0C
		0x41
		short SyncronizationShift
	}

	struct SetSynchronizationShiftResponse
	{
		5
		0x0C
		0x41
		short SyncronizationShift
	}

	struct GetAmplitudeLevelRequest
	{
		5
		byte ReceiverNumber
		0x38
		0
		0
	}

	struct GetAmplitudeLevelResponse
	{
		5
		byte ReceiverNumber
		0x38
		short AmplitudeLevel
	}

	struct GetTemperatureRequest
	{
		5
		byte ReceiverNumber
		0x2C
		0
		0
	}

	struct GetTemperatureResponse
	{
		5
		byte ReceiverNumber
		0x2C
		short Temperature
	}
}