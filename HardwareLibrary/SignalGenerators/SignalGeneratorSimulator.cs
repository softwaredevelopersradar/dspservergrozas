﻿using System;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Hardware;
using NetLib;
using Settings;
using SimulatorProtocols;

namespace HardwareLibrary.SignalGenerators
{
    public class SignalGeneratorSimulator : ISignalGenerator
    {
        public float FrequencyKhz
        {
            get => _frequencyKhz;
            set
            {
                _frequencyKhz = value;
                SendSignalToSimulator();
            }
        }

        public float Level
        {
            get => _level;
            set
            {
                _level = value;
                SendSignalToSimulator();
            }
        }

        public bool EmitionEnabled
        {
            get => _emitionEnabled;
            set
            {
                _emitionEnabled = value;
                SendSignalToSimulator();
            }
        }

        public bool IsInitialized { get; private set; }

        private readonly NetClientSlim _simulatorClient;
        private float _frequencyKhz = Constants.FirstBandMinKhz;
        private float _level = Constants.ReceiverMinAmplitude;
        private bool _emitionEnabled = false;

        public SignalGeneratorSimulator(NetClientSlim simulatorClient = null)
        {
            _simulatorClient = simulatorClient;
        }

        private void SendSignalToSimulator()
        {
            if (_simulatorClient == null)
            {
                return;
            }
            try
            {
                if (Constants.FirstBandMinKhz <= FrequencyKhz && FrequencyKhz <= Config.Instance.BandSettings.LastBandMaxKhz)
                {
                    _simulatorClient.Write(SetGeneratorSignalRequest.ToBinary((int)(FrequencyKhz * 10), (int)Level, EmitionEnabled));
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Signal generator simulator error");
            }
        }

        public async Task<bool> Initialize(string hostname)
        {
            IsInitialized = true;
            return true;
        }

        public void Close()
        { }

        public void Reset()
        {
            SendSignalToSimulator();
        }
    }
}
