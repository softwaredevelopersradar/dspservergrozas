﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.Hardware;
using HardwareLibrary.FpgaDevices;
using HardwareLibrary.Gps;
using HardwareLibrary.Receivers;
using HardwareLibrary.SignalGenerators;
using HardwareLibrary.TransmitionChannels;
using HardwareLibrary.Usrp;
using NetLib;

namespace HardwareLibrary
{
    public class HardwareController : IHardwareController
    {
        public IReceiverManager ReceiverManager { get; }
        public IFpgaDeviceManager DeviceManager { get; }
        public IUsrpReceiverManager UsrpReceiverManager { get; }
        public ISignalGenerator SignalGenerator { get; }
        public IGpsReceiver GpsReceiver { get; }
        public ICompassManager CompassManager { get; }
        public float ScanReadTimeMs { get; private set; }

        private readonly NetClientSlim _simulatorClient;

        public IReadOnlyList<FpgaDeviceBandSettings> FpgaDeviceBandsSettings => DeviceManager.BandsSettingsSource.Settings;


        public event EventHandler<IFpgaDataScan> ScanReadEvent;

        private readonly Config _config;

        private readonly Stopwatch _stopwatch;

        /// <summary>
        /// Mock, todo : null for database controller?_?
        /// </summary>
        public HardwareController() : this(new DataProcessorMock(), null)
        { }

        public HardwareController(IDataProcessor dataProcessor)
            : this(dataProcessor, Config.Instance)
        { }

        public HardwareController(IDataProcessor dataProcessor, Config config)
        {
            _config = config;
            _stopwatch = new Stopwatch();
            dataProcessor.ScanReadTimer = this;
            if (config.HardwareSettings.SimulateFpgaDevices)
            {
                //todo : fix that stuff yo! DRY
                _simulatorClient = new NetClientSlim();
                DeviceManager = new FakeFpgaDeviceManager(dataProcessor, _simulatorClient, _config);
                ReceiverManager = new ReceiverManager(DeviceManager, DeviceManager.GetFpgaTransmissionChannel());
                UsrpReceiverManager = CreateUsrpReceiverManager(config);
                SignalGenerator = new SignalGeneratorSimulator(_simulatorClient);
                GpsReceiver = CreateGpsReceiver(null);
                CompassManager = new Compasses.CompassManager();
            }
            else
            {
                DeviceManager = new FpgaDeviceManager();
                var transmitionChannel = _config.HardwareSettings.UseRs232TransmitionChannel 
                    ? new Rs232TransmitionChannel()
                    : DeviceManager.GetFpgaTransmissionChannel();
                MessageLogger.Log("Receivers connected with "
                                  + (_config.HardwareSettings.UseRs232TransmitionChannel ? "RS232" : "FPGA"));
                ReceiverManager = new ReceiverManager(DeviceManager, transmitionChannel);
                if(_simulatorClient == null)
                    _simulatorClient = new NetClientSlim();

                UsrpReceiverManager = CreateUsrpReceiverManager(config);

                var generatorType = _config.CalibrationSettings.SignalGeneratorSettings.Type;
                SignalGenerator = (generatorType == SignalGeneratorType.Simulation)
                    ? (ISignalGenerator) new SignalGeneratorSimulator()
                    : new SignalGenerator(generatorType);
                GpsReceiver = CreateGpsReceiver(null);
                CompassManager = new Compasses.CompassManager();
            }
        }

        private IUsrpReceiverManager CreateUsrpReceiverManager(Config config)
        {
            if (config.BandSettings.BandCount > 100)
                return config.HardwareSettings.UsrpSettings.SimulateUsrp
                    ? new FakeUsrpReceiverManager(_simulatorClient, _config) as IUsrpReceiverManager
                    : new UsrpReceiverManager();
            else
                return new MockUsrpReceiverManager(); //when we don't have usrp physically
        }

        private IGpsReceiver CreateGpsReceiver(UpdateLocationDelegate updateLocationFunction)
        {
            return _config.HardwareSettings.GpsSettings.IsSimulation
                    ? new FakeGpsReceiver(updateLocationFunction) as IGpsReceiver
                    : new GpsReceiver(
                    comPortName: _config.HardwareSettings.GpsSettings.ComPort,
                    baudRate: _config.HardwareSettings.GpsSettings.BaudRate,
                    updateLocalTimeIntervalMs: _config.HardwareSettings.GpsSettings.ForceUpdateIntervalMin * 60000, //min => ms
                    updateLocationFunction: updateLocationFunction);
        }

        public bool Initialize()
        {
            if (!DeviceManager.Initialize())
            {
                MessageLogger.Error("Can't initialize fpga devices");
                return false;
            }
            if (!_config.HardwareSettings.SimulateFpgaDevices)
            {
                MessageLogger.Log($"Fpga devices initialized, driver version {DeviceManager.GetDriverVersion()}");
            }
            if (!ReceiverManager.Initialize())
            {
                MessageLogger.Error("Can't initialize receivers");
                return false;
            }
            if (!UsrpReceiverManager.Initialize())
            {
                MessageLogger.Error("Can't initialize usrp receiver");
                return false;
            }
            if (!GpsReceiver.Initialize())
            {
                MessageLogger.Warning("Can't initialize gps receiver");
            }

            CheckSignalGenerator();
            return true;
        }

        private async Task CheckSignalGenerator()
        {
            await InitializeSignalGeneratorAsync();
            SignalGenerator.Close();
        }

        private async Task InitializeSignalGeneratorAsync()
        {
            MessageLogger.Trace("Hardware controller: initialize signal generator");
            var result = await SignalGenerator.Initialize(_config.CalibrationSettings.SignalGeneratorSettings.Host);
            if (!result)
            {
                MessageLogger.Warning("Can't initialize signal generator");
            }
            else if (!(SignalGenerator is SignalGeneratorSimulator))
            {
                MessageLogger.Log(_config.CalibrationSettings.SignalGeneratorSettings.Type + " signal generator initialized");
            }
        }

        private void UpdateScanReadTime()
        {
            if (ScanReadTimeMs == 0)
            {
                ScanReadTimeMs = _stopwatch.ElapsedMilliseconds;
            }
            else
            {
                const float currentScanSpeedFactor = 0.3f;
                ScanReadTimeMs = ScanReadTimeMs * (1 - currentScanSpeedFactor) +
                                 _stopwatch.ElapsedMilliseconds * currentScanSpeedFactor;
            }
        }

        public IFpgaDataScan GetScan(IReadOnlyList<int> bandNumbers)
        {
            if (!ReceiverManager.SetBandNumbers(bandNumbers))
            {
                return null;
            }
            _stopwatch.Restart();
            var data = DeviceManager.GetScan(bandNumbers);
            ScanReadEvent?.Invoke(this, data);
            _stopwatch.Stop();
            UpdateScanReadTime();
            return data;
        }
    }
}
