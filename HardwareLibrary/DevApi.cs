﻿using System;
using System.Runtime.InteropServices;
using DspDataModel.Hardware;

namespace HardwareLibrary
{
    public unsafe class DevApi
    {
        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern uint pHTONL(uint val, int e);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern uint pNTOHL(uint val, int e);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern ushort pNTOHS(ushort val, int e);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pEndianness(IntPtr hDev, out Endianness e);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pFindAddr(IntPtr hDev, byte offTblBar, Endianness e, int fpt0, int fpt1, ref uint addr);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pFindBarAndOffset(IntPtr hDev, byte offTblBar, int fpt0, int fpt1, ref byte bar, ref uint off);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pReadDwordFromBAR0(IntPtr hDevice, uint offset, out uint val);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pWriteDwordToBAR0(IntPtr hDevice, uint offset, uint val);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pReadDwordFromBAR1(IntPtr hDevice, uint offset, out uint val);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pWriteDwordToBAR1(IntPtr hDevice, uint offset, uint val);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pReadDwordFromBAR2(IntPtr hDevice, uint offset, out uint val);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pWriteDwordToBAR2(IntPtr hDevice, uint offset, uint val);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pReadDword(IntPtr hDevice, byte bar, uint offset, out uint data);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pWriteDword(IntPtr hDevice, byte bar, uint offset, uint data);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pReadPacket(IntPtr hDevice, byte bar, uint offset, uint[] data, uint dataSize);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pWritePacket(IntPtr hDevice, byte bar, uint offset, uint[] data, uint dataSize);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pGetPciConfig(IntPtr hDevice, out PciConfig data);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pGetDmaConfig(IntPtr hDevice, out DmaInfo data, uint dataSize);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        private static extern bool pInitialiseTransfer(IntPtr hDevice, void* buf, uint sizeofBuf);
        
        public static bool InitialiseTransfer(IntPtr hDevice, ref DmaConfigin dmaConf)
        {
            fixed (void* pointer = &dmaConf)
            {
                return pInitialiseTransfer(hDevice, pointer, (uint)Marshal.SizeOf(dmaConf));
            }
        }

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pStartTransfer(IntPtr hDevice);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pReadBlock(IntPtr hDevice, byte[] buf, uint bufSize);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pWriteBlock(IntPtr hDevice, byte[] buf, uint bufSize);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pStopTransfer(IntPtr hDevice);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pResetTransfer(IntPtr hDevice);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern uint pGetDeviceCount();

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern IntPtr pGetDeviceEnum();

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern IntPtr pdevOpen(uint devNum);

        [DllImport("devapi", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool pdevClose(IntPtr* hDevice);

        public static bool DevClose(IntPtr hDevice)
        {
            return pdevClose(&hDevice);
        }
    }
}
