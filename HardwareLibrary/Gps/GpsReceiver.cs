﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Timers;
using DeviceLib;
using DeviceLib.DataSource;
using DeviceLib.HeaderSplitting;
using DeviceLib.Listenning;
using DeviceLib.MessageSplitting;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using LibNMEAParser;
using Settings;

namespace HardwareLibrary.Gps
{
    public class GpsReceiver : IGpsReceiver
    {
        public DateTime UtcTime { get; private set; }

        public DateTime LocalTime { get; private set; }

        public Position Location { get; private set; }

        public int NumberOfSatellites { get; private set; }

        public double AntennaHeight { get; private set; }

        public bool IsConnected { get; private set; } = false;
        
        public event EventHandler GpsConnectionLostEvent; //todo : subscribe to it + add broadcast
        public event EventHandler<DateTime> UpdateLocalTimeEvent;

        private readonly SerialPort _gpsPort;
        private readonly Timer _updateLocalTimeTimer;
        private int _updateGpsCounter = 0;

        /// <summary>
        /// All this part is a combination of some library and Lina's library.
        /// So don't touch this fields and everything connected to them, UNLESS
        /// You want to create a new Gps library and use it.
        /// </summary>
        private readonly GPSEncoder _encoder;
        private readonly DeviceHolder<string> _deviceHolder;
        private readonly PassiveListener<string> _listener;
        private readonly GetMessage _messageParser;
        private readonly SetParameters _setParameters;
        private readonly UpdateLocationDelegate _updateGpsStateFunction;

        public GpsReceiver(string comPortName, int baudRate, 
            double updateLocalTimeIntervalMs, UpdateLocationDelegate updateLocationFunction)
        {
            _updateGpsStateFunction = updateLocationFunction;
            _gpsPort = new SerialPort(comPortName, baudRate, Parity.None, 8, StopBits.One);
            LocalTime = DateTime.Now;
            UtcTime = DateTime.UtcNow;

            _updateLocalTimeTimer = new Timer(updateLocalTimeIntervalMs);
            _updateLocalTimeTimer.Elapsed += OnUpdateLocalTime;

            _setParameters = new SetParameters();
            _messageParser = new GetMessage();
            _encoder = new GPSEncoder();
            
            _deviceHolder = new DeviceHolder<string>(
                message_splitter: new EndLineMessageSplitter(), 
                header_splitter: new EmptyGPSHeaderSplitter());

            _listener = new PassiveListener<string> 
            {
                ID = string.Empty 
            };

            _setParameters.DatetimeUTC += new SetParameters.DateTimeUTCEventHandler(UtcTimeArrived);
            _setParameters.DatetimeLocal += new SetParameters.DateTimeLocalEventHandler(LocalTimeArrived);
            _setParameters.SatelliteCount += new SetParameters.SatelliteCountEventHandler(SatellitesInfoArrived);
            _setParameters.HeightAntenna += new SetParameters.HeightAntennaEventHandler(AntennaInfoArrived);
            _setParameters.Location += new SetParameters.LocationEventHandler(LocationArrived);

            _deviceHolder.onConnectionStatusChanged += ConnectionStatusChanged;
            _listener.onDataArrived += DataArrived;
        }

        private void OnUpdateLocalTime(object sender, ElapsedEventArgs args)
        {
            UpdateLocalTimeEvent?.Invoke(sender, LocalTime);
        }

        private void ConnectionStatusChanged(object sender, bool connectionStatus) 
        {
            if (connectionStatus)
                return;

            GpsConnectionLostEvent?.Invoke(sender, EventArgs.Empty);
            _updateLocalTimeTimer.Stop();
        }

        private void DataArrived(object sender, IEnumerable<byte> bytes) 
        {
            var text = Encoding.ASCII.GetString(bytes.ToArray());
            _messageParser.parseMessage(text, _setParameters);
        }

        private void UtcTimeArrived(string code, DateTime dateTime) => UtcTime = dateTime;

        private void LocalTimeArrived(string code, DateTime dateTime) => LocalTime = dateTime;

        private void SatellitesInfoArrived(string code, int satellitesCount) =>
            NumberOfSatellites = satellitesCount;

        private void AntennaInfoArrived(string code, double antennaHeight) =>
            AntennaHeight = antennaHeight;
        
        private void LocationArrived(string code, 
            char signLatitude, double latitude, 
            char signLongitude, double longitude, 
            double altitude)
        {
            if (signLatitude == 'S')
                latitude = -latitude;

            if (signLongitude == 'E')
                longitude = -longitude;

            var newLocation = new Position(latitude, longitude, (int) altitude);

            if (!AreSameCoordinates(Location, newLocation))
            {
                //todo!
                //_updateGpsStateFunction(this);
                Location = newLocation;
                return;
            }

            _updateGpsCounter++;
            if (_updateGpsCounter == Constants.UpdateGpsValuesThreshold)
            {
                //_updateGpsStateFunction(this);
                _updateGpsCounter = 0;
            }
        }

        public bool Initialize()
        {
            try
            {
                _listener.Start();
                _deviceHolder.Open(new SerialPortDataSource(_gpsPort) { ReadTimeout = 1000 });
                _deviceHolder.Subscribe(_listener);
                _deviceHolder.SendRequest(_encoder.QueryModeCompatibility(GPSEncoder.ModeCompatibility.ALL_SYST));
                IsConnected = true;
                MessageLogger.Log($"Gps initialized at port : {_gpsPort.PortName}");
                _updateLocalTimeTimer.Start();
                return true;
            }
            catch(Exception e)
            {
                MessageLogger.Warning($"exception during gps initialization : {e.StackTrace}\r\n" +
                    $"exception : {e.ToString()}");
                return false;
            }
        }

        private bool AreSameCoordinates(Position oldPosition, Position newPosition)
        {
            return Math.Abs(oldPosition.Longitude - newPosition.Longitude) < 1e-7 &&
                   Math.Abs(oldPosition.Latitude - newPosition.Latitude) < 1e-7 &&
                   Math.Abs(oldPosition.Altitude - newPosition.Altitude) < 1e-7;
        }
    }
}
