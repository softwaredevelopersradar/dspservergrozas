﻿using System;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;

namespace HardwareLibrary.Gps
{
    public class FakeGpsReceiver : IGpsReceiver
    {
        public bool IsConnected => true;

        public DateTime UtcTime => DateTime.UtcNow;

        public DateTime LocalTime => DateTime.Now;

        public Position Location { get; private set; }

        public int NumberOfSatellites { get; private set; }

        public double AntennaHeight { get; private set; }

        public event EventHandler GpsConnectionLostEvent;
        public event EventHandler<DateTime> UpdateLocalTimeEvent;

        private readonly Random _rngGod;
        private readonly UpdateLocationDelegate _updateGpsStateFunction;

        public FakeGpsReceiver(UpdateLocationDelegate updateLocationFunction) 
        {
            _updateGpsStateFunction = updateLocationFunction;
            _rngGod = new Random();
            NumberOfSatellites = _rngGod.Next(5, 25);
            AntennaHeight = _rngGod.Next(5,10);
            Location = new Position(
                latitude: Config.Instance.HardwareSettings.GpsSettings.SimulatedLatitude, 
                longitude: Config.Instance.HardwareSettings.GpsSettings.SimulatedLongitude, 
                altitude: _rngGod.Next(10,100));
        }

        public bool Initialize() 
        {
            Task.Run(SimulateGps);
            return true;
        }

        /// <summary>
        /// this method generates gps coordinates every second, 
        /// depending on starting position
        /// </summary>
        private async Task SimulateGps() 
        {
            var delta = Config.Instance.HardwareSettings.GpsSettings.SimulatedDelta;
            while (true)
            {
                await Task.Delay(1000);

                if (_rngGod.Next(0, 2) == 0)
                {
                    continue;
                }

                Location = new Position(
                    latitude: Location.Latitude + _rngGod.Next(-1, 2) * delta,
                    longitude: Location.Longitude + _rngGod.Next(-1, 2) * delta,
                    altitude: Location.Altitude // we do not change our altitude
                    );
                  
                //todo: remove
                //_updateGpsStateFunction(this);
            }
        }
    }
}
