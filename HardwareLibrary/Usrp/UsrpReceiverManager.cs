﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using Settings;
using USRPLibrary;

namespace HardwareLibrary.Usrp
{
    public class UsrpReceiverManager : IUsrpReceiverManager
    {
        private readonly UsrpReceiver _receiver;
        private readonly ManualResetEvent _commandResponseEvent;
        private readonly UsrpConfig _config;

        private const int UsrpSampleCount = 4000;
        private const int UsrpBandGapMhz = 6;
        private const int UsrpCentralPeakLowering = 10;
        private const int UsrpBadFrequencyLowering = 35;
        private const int BadFrequencyStepsCount = 5;
        private const int UsrpSubtractionCoefficient = 172;
        
        private readonly Stopwatch _scanSpeedWatch = new Stopwatch();

        private static int ScanIndexFactory = 0;
        private float[] _lastScan;

        public bool IsConnected { get; private set; }

        public float ScanSpeed { get; private set; }

        private short _frequencyMhz = 3500;
        public short FrequencyMhz
        {
            get => _frequencyMhz;
            set
            {
                if (value >= 100 && value <= 6010)
                    _frequencyMhz = value;
                else
                    MessageLogger.Warning($"Can't set usrp receiver's frequency to : {value} MHz");
            }
        }

        public int BandNumber
        {
            get => Utilities.GetBandNumber(FrequencyMhz * 1000);
            set
            {
                //setting to frequency to band's central frequency and parsing it to MHz value
                if (value < Config.Instance.BandSettings.BandCount && value >= Constants.UsrpFirstBandNumber - 1)
                    FrequencyMhz = (short)((Utilities.GetBandMinFrequencyKhz(value) + Constants.BandwidthKhz / 2) / 1000);
                else
                    MessageLogger.Warning($"Can't set usrp receiver to band : {value}");
            }
        }
        
        private byte _amplification;
        public byte Amplification
        {
            get => _amplification;
            set
            {
                if (value >= 1 && value <= 70)
                    _amplification = value;
                if (value > 70 && value <= 120)
                    _amplification = 70;
                else
                    MessageLogger.Warning($"Can't set usrp receiver's amplification to : {value}");
            }
        }

        public UsrpReceiverManager()
        {
            _config = Config.Instance.HardwareSettings.UsrpSettings;
            _amplification = _config.Amplification;
            _receiver = new UsrpReceiver();
            _commandResponseEvent = new ManualResetEvent(false);
            SubscribeToReceiverEvents();
        }

        private void SubscribeToReceiverEvents()
        {
            _receiver.OnConnect += (sender, args) => IsConnected = true;
            _receiver.OnDisconnect += (sender, args) => IsConnected = false;
            _receiver.OnError += (sender, errorCode) => MessageLogger.Warning($"Usrp error : {errorCode}"); 
            _receiver.OnGetSpectrum += ReceiveSpectrum;
        }

        private bool Connect()
        {
            try
            {
                _receiver.Connect(Config.Instance.ServerSettings.ServerHost, _config.OutputPort, _config.Host, _config.Port);
                return true;
            }
            catch (SocketException e)
            {
                MessageLogger.Error($"Error during connecting to usrp receiver : {e}");
                return false;
            }
        }

        private void Disconnect()
        {
            _receiver.Disconnect();
        }

        public bool Initialize()
        {
            return Connect();
        }

        private void ReceiveSpectrum(object sender, SpectrumEventArgs args)
        {
            _lastScan = args.Spectrum.Select(v => (float)(v - Amplification + _config.AddedLevel - UsrpSubtractionCoefficient)).ToArray();
            _commandResponseEvent.Set();
        }
        
        /// <summary>
        /// Hack! We stretch our usrp data (4k points) to 9831 points for easier further processing,
        /// since usrp receiver is only a hack itself - this is fine.
        /// This method is NOT ideal. Don't use it for other purposes without edit
        /// </summary>
        private float[] Stretch(float[] usrpScan)
        {
            var stretchedScan = new float[9831];
            /*
             * we create 7_999 points from 4k points
             * after we add 229 points for every 1000 points and get 9831 points.
             * note : last points i
             */
            var addedThreshold = 229;
            var addedAmount = 0;
            var border = 1_000;
            var delta = 1000.0f / 229;
            float previous8kValue = 0;

            for (int _9kIndex = 0; _9kIndex < stretchedScan.Length; _9kIndex++)
            {
                float current8kValue;
                var _8kIndex = _9kIndex - addedAmount;

                if (_8kIndex % 2 == 0 || _8kIndex / 2 + 1 == usrpScan.Length)
                    current8kValue = usrpScan[_8kIndex / 2];
                else
                    current8kValue = (usrpScan[_8kIndex / 2] + usrpScan[_8kIndex / 2 + 1]) / 2;

                if (_8kIndex <= delta * (addedAmount + 1))
                {
                    if (_8kIndex >= usrpScan.Length * 2)
                    {
                        stretchedScan[_9kIndex] = previous8kValue;
                        continue;
                    }
                    stretchedScan[_9kIndex] = current8kValue;
                }
                else if (addedAmount < addedThreshold)
                {
                    stretchedScan[_9kIndex] = (current8kValue + previous8kValue) / 2;
                    addedAmount++;
                }
                previous8kValue = current8kValue;

                if (stretchedScan[_9kIndex] < Constants.ReceiverMinAmplitude)
                {
                    stretchedScan[_9kIndex] = Constants.ReceiverMinAmplitude + 10; 
                    // hack: usrp can give data below min amplitude, and we increase it manually
                }

                if (_8kIndex >= border)
                {
                    addedThreshold += 229;
                    border += 1000;
                }
            }

            return stretchedScan;
        }

        public IAmplitudeScan GetScan()
        {
            _scanSpeedWatch.Restart();
            var firstPart = ReceiveScan((short)(FrequencyMhz - UsrpBandGapMhz), Amplification);
            var secondPart = ReceiveScan((short)(FrequencyMhz + UsrpBandGapMhz), Amplification);
            RemoveCentralPeaks();
            var usrpScan = new float[UsrpSampleCount];
            /*
             * we create one 30 MHz scan from two 30.72 MHz scans from usrp
             * (where only 20 MHz are good though ._.)
             * parts by 15 MHz are taken, with 12 MHz intersection
             * numbers were taken, based on sample count divisors
             */
            Array.Copy(firstPart, UsrpSampleCount / 5, usrpScan, 0, UsrpSampleCount / 2);
            Array.Copy(secondPart, UsrpSampleCount / 10 * 3, usrpScan, UsrpSampleCount / 2, UsrpSampleCount / 2);
            _scanSpeedWatch.Stop();

            var newSpeed = (float)((Constants.BandwidthKhz / 1000) / (_scanSpeedWatch.Elapsed.TotalMilliseconds));
            if (ScanSpeed != 0)
                ScanSpeed = ScanSpeed / 2 + newSpeed / 2;
            else
                ScanSpeed = newSpeed;

            var stretchedScan = Stretch(usrpScan);
            RemoveBadFrequencies(4_799_995);

            Interlocked.Increment(ref ScanIndexFactory);
            return new AmplitudeScan(stretchedScan, BandNumber, DateTime.Now, ScanIndexFactory);

            float[] ReceiveScan(short frequency, byte amplification)
            {
                var errorCounter = 3;
                while (errorCounter >= 0)
                {
                    _receiver.RequestSpectrum(frequency, amplification);
                    if (_commandResponseEvent.WaitOne(_config.MaxResponseWaitTime))
                    {
                        _commandResponseEvent.Reset();
                        return _lastScan;
                    }
                    else
                    {
                        errorCounter--;
                        continue;
                    }
                }
                _commandResponseEvent.Reset();
                MessageLogger.Error($"Error while trying to get usrp receiver's scan at {frequency} MHz, with amplification : {amplification}");
                return new float[4000];
            }

            void RemoveCentralPeaks()
            {
                firstPart[UsrpSampleCount / 2 - 1] -= UsrpCentralPeakLowering;
                firstPart[UsrpSampleCount / 2] -= UsrpCentralPeakLowering;

                secondPart[UsrpSampleCount / 2 - 1] -= UsrpCentralPeakLowering;
                secondPart[UsrpSampleCount / 2] -= UsrpCentralPeakLowering;
            }

            void RemoveBadFrequencies(int badFrequencyKhz)
            {
                var badFrequencyBandNumber = Utilities.GetBandNumber(badFrequencyKhz);
                var startFreq = Utilities.GetBandMinFrequencyKhz(badFrequencyBandNumber);
                if (badFrequencyBandNumber == BandNumber)
                {
                    for (int i = 0; i < stretchedScan.Length; i++)
                    {
                        if (Math.Abs(startFreq + stretchedScan[i] * Constants.SamplesGapKhz - badFrequencyKhz)
                            < Constants.SamplesGapKhz * BadFrequencyStepsCount)
                            stretchedScan[i] -= UsrpBadFrequencyLowering;
                    }
                }
            }
        }
    }
}
