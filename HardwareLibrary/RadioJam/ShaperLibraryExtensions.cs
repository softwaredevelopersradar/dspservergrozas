﻿using DspDataModel.RadioJam;
using FPSLib;
using System;

namespace HardwareLibrary.RadioJam
{
    internal static class ShaperLibraryExtensions
    {
        internal static byte[] FiveValueArgsToByteArray(this FiveValEventArgs args, ShaperCommandCodes code)
        {
            //code + 3 errors + 5 for each of fiveparam
            var outputArray = new byte[1 + 3 + args.FiveParams.Count * 5];
            outputArray[0] = (byte) code;
            var errorArray = args.Errors.ErrorsToByteArray();
            Array.Copy(errorArray, 0, outputArray, 1, errorArray.Length);

            for (int i = 0; i < args.FiveParams.Count; i++)
            {
                var fiveParamArray = args.FiveParams[i].FiveParamToByteArray();
                Array.Copy(fiveParamArray, 0, outputArray, 4 + 5 * i, fiveParamArray.Length);
            }
            return outputArray;
        }

        internal static byte[] ErrorsToByteArray(this ErrorDataStructure error) =>
            new byte[3] { error.ErrorCode1, error.ErrorCode2, error.ErrorCode3 };

        private static byte[] FiveParamToByteArray(this FiveParam args) =>
            new byte[5] { args.Letter, args.Value1, args.Value2, args.Value3, args.Value4};
    }
}
