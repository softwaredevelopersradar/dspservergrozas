﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DspDataModel;
using DspDataModel.RadioJam;
using FPSLib;
using ModelsTablesDBLib;
using Settings;

namespace HardwareLibrary.RadioJam
{
    public class RadioJamShaper : IRadioJamShaper
    {
        /*
         * Side note : sending 0 as liter argument to shaper library methods (which allow it),
         * will return result for all liters.
         * Sending liter number alone will get info only for certain liter
         * sending list like {1,2,3,4} won't work (probably?) - so don't even try it
         */
        private readonly FPS _shaper;
        private readonly ManualResetEvent _commandResponseEvent;
        private readonly ManualResetEvent _powerCommandResponseEvent;
        private readonly RadioJamConfig _config;

        private readonly int CommandResponseTimeOut;

        private readonly int[] _literPowers;

        private bool _isConnected;

        public bool IsConnected
        {
            get => _isConnected;
            private set
            {
                _isConnected = value;
                ConnectionStateChangedEvent?.Invoke(this, IsConnected);
            }
        }

        public event EventHandler<byte[]> ShaperConditionReceivedEvent;
        public event EventHandler<bool> ConnectionStateChangedEvent;

        public RadioJamShaper() : this(Config.Instance.HardwareSettings.JammingConfig)
        { }

        public RadioJamShaper(RadioJamConfig config)
        {
            _config = config;
            CommandResponseTimeOut = _config.CommandResponseTimeoutMs;
            _shaper = new FPS(_config.ClientPort, _config.ClientHost, _config.ShaperPort, _config.ShaperHost, _config.ShaperErrorByteCount);
            
            _commandResponseEvent = new ManualResetEvent(false);
            _powerCommandResponseEvent = new ManualResetEvent(false);
            _literPowers = new int[_config.LittersCount];
            SubscribeToShaperEvents();
        }

        public bool Connect(string host, int port, string clientHost, int clientPort)
        {
            if (IsConnected)
                return true;
            _shaper.Connect();
            return true;
        }

        public void Disconnect()
        {
            if (!IsConnected)
            {
                return;
            }
            _shaper.Disconnect();
        }

        private void SubscribeToShaperEvents()
        {
            _shaper.ConnectNet += OnConnect;
            _shaper.DisconnectNet += OnDisconnect;

            _shaper.ReceiveCmd += CommandResponse;
            _shaper.LostAnyCmd += OnLostAnyShaperCommand;
            _shaper.LostFPS += OnLostShaper;
            _shaper.RecivedStateFPS += OnShaperStateReceived;

            _shaper.UpdatePower += OnPowerReceived;
            _shaper.UpdateState += OnStateReceived;
            _shaper.UpdateCurrent += OnCurrentReceived;
            _shaper.UpdateTemp += OnTemperatureReceived;
            _shaper.UpdateVoltage += OnVoltageReceived;
        }

        /*
         * Event handlers :
         */

        private void OnConnect(object sender, EventArgs args)
        {
            IsConnected = true;
        }

        private void OnDisconnect(object sender, EventArgs args)
        {
            IsConnected = true;
        }

        private void CommandResponse(object obj, RecieveCmdEventArgs args)
        {
            //this args are atavisms from previous library, don't know what are they still doing here...
            _commandResponseEvent.Set();
        }

        private void OnLostAnyShaperCommand(object sender, EventArgs args) => 
            MessageLogger.Error($"Lost radiojam shaper command event occured");

        private void OnLostShaper(object sender, EventArgs args) => 
            MessageLogger.Error("Lost radiojam shaper event occured");

        private void OnShaperStateReceived(object sender, StateEventArgs args) =>
            MessageLogger.Warning($"Received shaper state\r\n" +
                                      $"mode :{args.ModeFps}\r\n" +
                                      $"version : {args.Version}\r\n" +
                                      $"errors : {args.Errors.ErrorCode1}, {args.Errors.ErrorCode2}, {args.Errors.ErrorCode3}");

        private void OnPowerReceived(object sender, ThreeValEventArgs args)
        {
            var condition = new byte[args.Power.Count * 3 + 3 + 1];
            condition[0] = (byte)ShaperCommandCodes.Power;
            var errorArray = args.Errors.ErrorsToByteArray();
            Array.Copy(errorArray, 0, condition, 1, errorArray.Length);
            for (int i = 0; i < args.Power.Count; i++)
            {
                var powerArray = new byte[]
                {
                        args.Power[i].Letter,
                        args.Power[i].CommandCode,
                        args.Power[i].Power
                };
                Array.Copy(powerArray, 0, condition, 4 + i * 3, powerArray.Length);
                _literPowers[i] = args.Power[i].Power;
            }

            _powerCommandResponseEvent.Set();
            ShaperConditionReceivedEvent?.Invoke(this, condition);
        }

        private void OnStateReceived(object sender, OneValEventArgs args)
        {
            var condition = new byte[args.State.Length + 3 + 1];
            condition[0] = (byte)ShaperCommandCodes.State;
            var errorArray = args.Error.ErrorsToByteArray();
            Array.Copy(errorArray, 0, condition, 1, errorArray.Length);
            Array.Copy(args.State, 0, condition, 4, args.State.Length);
            ShaperConditionReceivedEvent?.Invoke(this, condition);
        }

        private void OnTemperatureReceived(object sender, FiveValEventArgs args)
        {
            ShaperConditionReceivedEvent?.Invoke(this,
                args.FiveValueArgsToByteArray(ShaperCommandCodes.Temperature));
        }

        private void OnCurrentReceived(object sender, FiveValEventArgs args)
        {
            ShaperConditionReceivedEvent?.Invoke(this,
                args.FiveValueArgsToByteArray(ShaperCommandCodes.Current));
        }

        private void OnVoltageReceived(object sender, TwoValEventArgs args)
        {
            var condition = new byte[args.Voltages.Count * 3 + 3 + 1];
            condition[0] = (byte)ShaperCommandCodes.Voltage;
            var errorArray = args.Errors.ErrorsToByteArray();
            Array.Copy(errorArray, 0, condition, 1, errorArray.Length);
            for (int i = 0; i < args.Voltages.Count; i++)
            {
                var voltage = BitConverter.GetBytes(args.Voltages[i].Voltage);
                condition[i * 3 + 4] = args.Voltages[i].Letter;
                condition[i * 3 + 5] = voltage[0];
                condition[i * 3 + 6] = voltage[1];
            }

            ShaperConditionReceivedEvent?.Invoke(this, condition);
        }
        
        /*
         * Shaper methods :
         */

        public ShaperResponse<int[]> GetLiterPowers()
        {
            var result = GetLiterPower(0);
            return result ? new ShaperResponse<int[]>(_literPowers, 0) : new ShaperResponse<int[]>(null, -1);
        }

        /// <summary>
        /// Get power level of a certaion liter. If liter equals 0, it will return power levels of all liters
        /// </summary>
        private bool GetLiterPower(byte liter)
        {
            _shaper.GetPowerAP(new List<byte>() { liter });
            return WaitForCommandResult();
        }

        public bool GetLiterStates()
        {
            _shaper.GetStateAP(new List<byte>() { 0 });
            return WaitForCommandResult();
        }

        public bool GetLiterVoltages()
        {
            _shaper.GetVoltageAP(new List<byte>() { 0 });
            return WaitForCommandResult();
        }

        public bool GetLiterCurrents()
        {
            _shaper.GetCurrentAP(new List<byte>() { 0 });
            return WaitForCommandResult();
        }

        public bool GetLiterTemperatures()
        {
            _shaper.GetTempAP(new List<byte>() { 0 });
            return WaitForCommandResult();
        }

        public bool SetLiterPower(byte liter, byte power)
        {
            var args = new List<SetPowerLetter>();
            if (liter != 0)
            {
                args.Add(new SetPowerLetter(liter, power));
            }
            else
                for (int i = 0; i < _config.LittersCount; i++)
                {
                    args.Add(new SetPowerLetter((byte)(i + 1), power));
                }

            var result = _shaper.SetPowerAP(args);
            _commandResponseEvent.Reset();
            return result;
        }
        
        public bool SetLiterTypeLoad(byte liter, byte typeLoad)
        {
            var result = _shaper.SetTypeLoad(liter, typeLoad);
            _commandResponseEvent.Reset();
            return result;
        }

        private bool WaitForCommandResult()
        {
            var commandResult = _commandResponseEvent.WaitOne(CommandResponseTimeOut);
            _commandResponseEvent.Reset();
            return commandResult;
        }

        public ShaperResponse StartFhssJamming(uint fftCountCode, int duration, IEnumerable<IRadioJamFhssTarget> targets)
        {
            var shaperTargets = targets.Select(t => new TableSuppressFHSS()
            {
                FreqMinKHz = t.MinFrequencyKhz,
                FreqMaxKHz = t.MaxFrequencyKhz,
                Threshold = t.Threshold,
                InterferenceParam = new InterferenceParam()
                {
                    Deviation = t.DeviationCode,
                    Duration = 1,
                    Manipulation = t.ManipulationCode,
                    Modulation = t.ModulationCode
                }
            }).ToList();

            Contract.Assert(duration > 0);
            Contract.Assert(shaperTargets.Count > 0);

            var result = _shaper.SetParamFHSS(shaperTargets, duration, (byte) fftCountCode);
            return result ? new ShaperResponse(0) : new ShaperResponse(-1);
        }

        public ShaperResponse StartFrsJamming(TimeSpan duration, IEnumerable<IRadioJamTarget> targets)
        {
            var shaperTargets = targets.Select(t => new TableSuppressFWS()
            {
                FreqKHz = t.FrequencyKhz,
                InterferenceParam = new InterferenceParam()
                {
                    Deviation = t.DeviationCode,
                    Duration = t.DurationCode,
                    Manipulation = t.ManipulationCode,
                    Modulation = t.ModulationCode
                },
                Threshold = (short)t.Threshold
            }).ToList();

            Contract.Assert(duration.TotalMilliseconds >= 0);
            Contract.Assert(shaperTargets.Count > 0);

            var result = _shaper.SetParamFWS(shaperTargets, (int)duration.TotalMilliseconds);
            return result ? new ShaperResponse(0) : new ShaperResponse(-1);
        }

        public ShaperResponse StopJamming()
        {
            var commandResult = _shaper.SetRadiatOff();
            return commandResult ? new ShaperResponse(0) : new ShaperResponse(-1);
        }

        public void StartVoiceJamming(bool emitionEnabled, int frequencyKhz, byte deviation)
        {
            _shaper.RegimeVoice((byte)(emitionEnabled ? 1 : 0), frequencyKhz, deviation);
        }

        public bool CheckIsVoiceJamAvailable(VoiceJammingMode jamMode, string filePath)
        {
            return jamMode == VoiceJammingMode.FromFile
                ? _shaper.SetVoice(filePath)
                : _shaper.SetVoice(Config.Instance.HardwareSettings.JammingConfig.VoiceJamDeviceId);
        }
    }
}
