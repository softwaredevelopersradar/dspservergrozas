﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using DspDataModel.RadioJam;
using Settings;

namespace HardwareLibrary.RadioJam
{
    public class RadioJamShaperSimulator : IRadioJamShaper
    {
        private bool _isConnected;
        public const int DefaultLiterPower = 1000;

        public bool IsConnected
        {
            get => _isConnected;
            private set
            {
                _isConnected = value; 
                ConnectionStateChangedEvent?.Invoke(this, IsConnected);
            }
        }

        public event EventHandler<bool> ConnectionStateChangedEvent;
        public event EventHandler<byte[]> ShaperConditionReceivedEvent;

        public bool Connect(string host, int port, string clientHost, int clientPort)
        {
            if (IsConnected)
            {
                return true;
            }
            IsConnected = true;
            return true;
        }

        public ShaperResponse StartFrsJamming(TimeSpan duration, IEnumerable<IRadioJamTarget> targets)
        {
            Contract.Assert(duration.TotalMilliseconds >= 0);
            MessageLogger.ShaperLog($">> Start jamming: emition duration: {duration.TotalMilliseconds:F0}, targets count: {targets.Count()}");
            MessageLogger.ShaperLog($"<< Start jamming: error code: 0");
            return new ShaperResponse(0);
        }

        public ShaperResponse StartFhssJamming(uint fftCountCode, int duration, IEnumerable<IRadioJamFhssTarget> targets)
        {
            return new ShaperResponse(0);
        }

        public void Disconnect()
        {
            if (!IsConnected)
            {
                return;
            }
            IsConnected = false;
        }

        public ShaperResponse StopJamming()
        {
            MessageLogger.ShaperLog(">> Stop jamming");
            MessageLogger.ShaperLog("<< Stop jamming: error code: 0");
            return new ShaperResponse(0);
        }

        public ShaperResponse<int[]> GetLiterPowers()
        {
            MessageLogger.ShaperLog(">> Get power");
            MessageLogger.ShaperLog("<< Get power: error code: 0");
            return new ShaperResponse<int[]>(
                result: Enumerable.Repeat(DefaultLiterPower, Config.Instance.HardwareSettings.JammingConfig.LittersCount).ToArray(), 
                errorCode: 0);
        }

        public bool GetLiterStates()
        {
            MessageLogger.ShaperLog(">> Get state");
            MessageLogger.ShaperLog("<< Get state: error code: 0");
            return true;
        }

        public void StartVoiceJamming(bool emitionEnabled, int frequencyKhz, byte deviation)
        {
            MessageLogger.ShaperLog(">> Start voice jamming");
            MessageLogger.ShaperLog("<< Start voice jamming: 0");
        }

        public bool CheckIsVoiceJamAvailable(VoiceJammingMode jamMode, string filePath)
        {
            MessageLogger.ShaperLog(">> Start voice jamming");
            MessageLogger.ShaperLog("<< Start voice jamming: 0");
            return true;
        }

        public bool GetLiterVoltages()
        {
            return true;
        }

        public bool GetLiterCurrents()
        {
            return true;
        }

        public bool GetLiterTemperatures()
        {
            return true;
        }

        public bool SetLiterPower(byte liter, byte power)
        {
            return true;
        }

        public bool SetLiterTypeLoad(byte liter, byte typeLoad)
        {
            return true;
        }
    }
}
