﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.Hardware;
using HardwareLibrary.TransmitionChannels;
using NetLib;
using SimulatorProtocols;

namespace HardwareLibrary.FpgaDevices
{
    public class FakeFpgaDeviceManager : IFpgaDeviceManager
    {
        public IReadOnlyList<IFpgaDevice> Devices => _devices;
        public FpgaMode Mode { get; private set; } = FpgaMode.Intelligence;
        public bool GetBlankScan { get; set; }
        public double GetScanTimeMs { get; private set; }

        private readonly FakeFpgaDevice[] _devices;

        private readonly NetClientSlim _client;

        private bool _isPlayingRecord;

        private readonly IDataProcessor _dataProcessor;
        private readonly Stopwatch _scanSpeedWatch = new Stopwatch();

        public FpgaOffsetsSetup OffsetsSetup { get; }

        public FpgaDeviceBandsSettingsSource BandsSettingsSource { get; }

        private readonly Config _config;

        public FakeFpgaDeviceManager(IDataProcessor dataProcessor, NetClientSlim simulatorClient) 
            : this(dataProcessor, simulatorClient, Config.Instance)
        { }

        public FakeFpgaDeviceManager(IDataProcessor dataProcessor, NetClientSlim simulatorClient, Config config)
        {
            _config = config;

            _devices = new FakeFpgaDevice[config.HardwareSettings.FpgaDevicesCount];
            _dataProcessor = dataProcessor;
            OffsetsSetup = new FpgaOffsetsSetup();
            BandsSettingsSource = new FpgaDeviceBandsSettingsSource();
            for (var i = 0; i < _devices.Length; ++i)
            {
                _devices[i] = new FakeFpgaDevice(BandsSettingsSource, i);
            }
            _client = simulatorClient;
            _isPlayingRecord = true;
        }

        public bool Initialize()
        {
            if (!_client.IsWorking)
                _client.Connect(_config.SimulatorSettings.SimulatorHost, _config.SimulatorSettings.SimulatorPort).Wait(200);
            return true;
        }

        public IFpgaDataScan GetScan(IReadOnlyList<int> bandNumbers)
        {
            _scanSpeedWatch.Restart();
            if (!StartDevices())
            {
                return null;
            }
            if (!WaitData())
            {
                return null;
            }
            var filteredBandNumbers = bandNumbers
                .Select(n => n == -1 ? 0 : n)
                .ToArray();
            var scan = ReadDataFromDevices(filteredBandNumbers);
            _scanSpeedWatch.Stop();
            GetScanTimeMs = _scanSpeedWatch.Elapsed.TotalMilliseconds;
            return scan;
        }

        public ITransmissionChannel GetFpgaTransmissionChannel()
        {
            return new FakeTransmitionChannel(_client);
        }

        private FpgaDataScan ReadDataFromDevices(IReadOnlyList<int> bandNumbers)
        {
            lock (_client)
            {
                var byteBandNumbers = bandNumbers
                    .Select(n => (byte) n)
                    .ToArray();

                _client.Write(DataRequest.ToBinary(byteBandNumbers));
                var responseData = new byte[DataResponse.BinarySize];
                _client.ReadExactBytesCount(responseData, 0, responseData.Length);
                var response = DataResponse.Parse(responseData);

                var scan = new FpgaDataScan();
                var skip = 0;
                for (var i = 0; i < _devices.Length; i++)
                {
                    var device = _devices[i];
                    var channels = response.Channels
                        .Skip(skip)
                        .Take(device.ChannelsCount)
                        .ToArray();
                    if (!device.ReadData(scan, channels, skip, bandNumbers))
                    {
                        return null;
                    }
                    skip += device.ChannelsCount;
                }
                if (response.IsPlayingRecord != _isPlayingRecord)
                {
                    _isPlayingRecord = response.IsPlayingRecord;
                    _dataProcessor.ReloadRadioPathTable(_isPlayingRecord ? _config.CalibrationSettings.RadioPathTableFilename : "");
                }
                return scan;
            }
        }

        public bool StartDevices()
        {
            foreach (var device in Devices)
            {
                if (!device.StartAdc(device.GetAdcFlag()))
                {
                    return false;
                }
            }
            return true;
        }

        public bool WaitData()
        {
            foreach (var device in Devices)
            {
                if (!device.WaitData())
                {
                    return false;
                }
            }
            return true;
        }

        public bool StartFhssMode(FhssSetup setup)
        {
            Mode = FpgaMode.Fhss;
            Devices[0].InitFpga(ChannelMode.Fhss, setup.FftSize);
            return true;
        }

        public bool StartFhssDurationMeasurement(FhssSetup setup)
        {
            Mode = FpgaMode.FhssDurationMeasurement;
            Devices[0].InitFpga(ChannelMode.FhssDurationMeasurement, setup.FftSize);
            return true;
        }

        public bool StopFhssMode()
        {
            Mode = FpgaMode.Intelligence;
            Devices[0].InitFpga(ChannelMode.Intelligence, FftResolution.N16384);
            return true;
        }

        public void Close()
        { }

        public int GetDriverVersion() => 0;
    }
}
