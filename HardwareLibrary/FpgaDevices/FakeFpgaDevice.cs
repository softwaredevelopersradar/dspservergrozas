﻿using System;
using System.Collections.Generic;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using Settings;
using SimulatorProtocols;

namespace HardwareLibrary.FpgaDevices
{
    public class FakeFpgaDevice : IFpgaDevice
    {
        public IntPtr DeviceHandler => IntPtr.Zero;

        public int ChannelsCount { get; }
        public bool IsOpened { get; private set; }
        public bool IsFhssModeRunning { get; private set; }

        private readonly FpgaDeviceBandsSettingsSource _bandsSettingsSource;
        private readonly Random _random;

        private ChannelMode _channelMode = ChannelMode.Intelligence;

        private readonly (float Amplitude, int SampleIndex)[] _peaks =
            new (float Amplitude, int SampleIndex)[Constants.FhssChannelsCount];

        public FakeFpgaDevice(FpgaDeviceBandsSettingsSource bandsSettingsSource, int deviceConfigIndex)
        {
            _bandsSettingsSource = bandsSettingsSource;
            var deviceConfig = Config.Instance.HardwareSettings.DeviceSettings[deviceConfigIndex];
            ChannelsCount = deviceConfig.ChannelsCount;
            IsOpened = false;

            _random = new Random(DateTime.Now.Millisecond);
        }

        public bool Initialize()
        {
            return true;
        }

        public bool StartFhssMode(FhssSetup setup)
        {
            IsFhssModeRunning = true;
            return true;
        }

        public bool StartFhssDurationMeasurement(FhssSetup setup)
        {
            IsFhssModeRunning = true;
            return true;
        }

        public bool StopFhssMode()
        {
            IsFhssModeRunning = false;
            return true;
        }

        public void Open()
        {
            IsOpened = true;
        }

        public void Close()
        {
            IsOpened = false;
        } 

        public bool InitFpga(ChannelMode channelMode, FftResolution fftSize)
        {
            _channelMode = channelMode;
            return true;
        }

        public bool InitFpgaTacting(TactingMode mode)
        {
            return true;
        }

        public uint GetAdcFlag()
        {
            return 0;
        }

        public bool StartAdc(uint flag)
        {
            return true;
        }

        public bool Read(byte[] buf)
        {
            return true;
        }

        public byte[] Read()
        {
            return null;
        }

        public bool WaitData()
        {
            return true;
        }

        public int GetFhssPeakIndex(int channelIndex)
        {
            Contract.Assert(channelIndex < Constants.FhssChannelsCount);
            var peak = _peaks[channelIndex];
            return peak.SampleIndex;
        }

        public int GetScanIndex()
        {
            return 0;
        }

        public int GetDriverVersion() => 0;

        public bool ReadData(IFpgaDataScan scan, IReadOnlyList<int> bandNumbers)
        {
            return true;
        }

        public bool InitializeFpgaNet()
        {
            return true;
        }

        public bool ReadData(IFpgaDataScan scan, ChannelData[] channels, int skip, IReadOnlyList<int> bandNumbers)
        {
            var now = DateTime.UtcNow;
            for (var i = 0; i < ChannelsCount; ++i)
            {
                var data = GetReceiverScan(channels[i], bandNumbers[skip + i], i, now, scan.ScanIndex);
                scan.SetScan(skip + i, data);
            }
            return true;
        }

        public void WriteWord(byte bar, uint offset, uint data)
        {
        }

        public uint ReadWord(byte bar, uint offset)
        {
            return 0;
        }

        private IReceiverScan GetReceiverScan(ChannelData data, int bandNumber, int channelIndex, DateTime creationTime, int scanIndex)
        {
            var amplitudes = new float[Constants.BandSampleCount];
            var phases = new float[Constants.BandSampleCount];
            var attenuatorSettings =_bandsSettingsSource.Settings[bandNumber];

            var attenuation = attenuatorSettings.IsConstantAttenuatorEnabled
                ? 10
                : 0f;
            attenuation += attenuatorSettings.AttenuatorLevel;

            // height of noise from Constants.ReceiverMinAmplitude point
            const float noiseHeight = 25;

            for (var i = 0; i < Constants.BandSampleCount; ++i)
            {
                phases[i] = data.Phases[i] * 0.1f;
                var amplitude = (float) data.Amplitudes[i];
                if (amplitude > noiseHeight)
                {
                    amplitude -= attenuation;
                    if (amplitude < noiseHeight)
                    {
                        amplitude = noiseHeight + _random.Next(-2, 3);
                    }
                }
                amplitudes[i] = amplitude + Constants.ReceiverMinAmplitude;
            }
            if (_channelMode == ChannelMode.Fhss)
            {
                var maxAmplitude = amplitudes[0];
                var maxIndex = 0;
                for (var i = 1; i < Constants.BandSampleCount; ++i)
                {
                    if (amplitudes[i] > maxAmplitude)
                    {
                        maxIndex = i;
                        maxAmplitude = amplitudes[i];
                    }
                }
                _peaks[channelIndex] = (maxAmplitude, maxIndex);
            }

            //hack
            if (bandNumber == Constants.UsrpFirstBandNumber - 1)
            {
                for (int i = 1638; i < amplitudes.Length; i++)
                    amplitudes[i] = Constants.ReceiverMinAmplitude;
            }

            return new ReceiverScan(amplitudes, phases, bandNumber, creationTime, scanIndex);
        }

        public void DropFhssMeasurementFlag()
        {}
    }
}
