﻿using System;
using System.Collections.Generic;
using DspDataModel.Hardware;

namespace HardwareLibrary.TransmitionChannels
{
    public class FpgaTransmitionChannel : ITransmissionChannel
    {
        public bool IsConnected { get; private set; }

        private readonly IFpgaDevice _device;

        private readonly byte _bar;

        private readonly uint _messageOffset1;

        private readonly uint _messageOffset2;

        private readonly uint _startFlagOffset;

        private readonly uint _resultFlagOffset;

        public FpgaTransmitionChannel(IFpgaDevice device, byte bar, uint messageOffset1, uint messageOffset2, uint startFlagOffset, uint resultFlagOffset)
        {
            _device = device;
            _bar = bar;

            _messageOffset1 = messageOffset1;
            _messageOffset2 = messageOffset2;
            _startFlagOffset = startFlagOffset;
            _resultFlagOffset = resultFlagOffset;

            IsConnected = false;
        }

        public bool Connect(params object[] args)
        {
            IsConnected = true;
            return true;
        }

        public void Disconnect()
        {
            IsConnected = false;
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            var length = count > 4 ? 4 : count;
            _device.WriteWord(_bar, _messageOffset1, Decode(offset, length));
            if (count > 4)
            {
                _device.WriteWord(_bar, _messageOffset2, Decode(offset + 4, count - 4));
            }

            _device.WriteWord(_bar, _startFlagOffset, 1);
            _device.WriteWord(_bar, _startFlagOffset, 0);

            uint Decode(int shiftIndex, int byteCount)
            {
                var shift = 24;
                uint value = 0;

                for (var i = 0; i < byteCount; ++i)
                {
                    value += (uint) buffer[shiftIndex + i] << shift;
                    shift -= 8;
                }
                return value;
            }
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            // waiting for device's answer
            while (_device.ReadWord(_bar, _resultFlagOffset) != 0xFFFF_FFFF)
            {
                // empty body is ok
            }

            var message1Length = count > 3 ? 3 : count;
            var message2Length = count - message1Length;

            var message1 = _device.ReadWord(_bar, _messageOffset1);
            GetBytes(buffer, message1, message1Length, offset, message1Length - 1);

            if (message2Length > 0)
            {
                var message2 = _device.ReadWord(_bar, _messageOffset2);
                GetBytes(buffer, message2, message2Length, offset + message1Length, 3);
            }

            return count;
        }

        void GetBytes(byte[] buffer, uint value, int byteCount, int shiftIndex, int startByteShift)
        {
            var shift = 8 * startByteShift;
            for (int i = 0; i < byteCount; ++i)
            {
                buffer[shiftIndex + i] = (byte)(value >> shift);
                shift -= 8;
            }
        }

        public event EventHandler<IReadOnlyList<byte>> OnDataRead;
        public int Timeout { get; set; }
    }
}
