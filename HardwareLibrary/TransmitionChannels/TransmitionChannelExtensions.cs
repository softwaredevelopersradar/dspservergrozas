﻿using DspDataModel;
using DspDataModel.Hardware;
using llcss;

namespace HardwareLibrary
{
    public static class TransmitionChannelExtensions
    {
        // sending and receiving answer of selected type
        public static T SendReceive<T>(this ITransmissionChannel channel, byte[] request, int responseSize) where T : IBinarySerializable, new()
        {
            var response = new byte[responseSize];
            lock (channel)
            {
                channel.Write(request, 0, request.Length);
                if (MessageLogger.IsReceiversLogEnabled)
                {
                    MessageLogger.ReceveirsLog(">>", request);
                }
                channel.Read(response, 0, responseSize);
                if (MessageLogger.IsReceiversLogEnabled)
                {
                    MessageLogger.ReceveirsLog("<<", response);
                }
            }
            var result = new T();
            result.Decode(response, 0);
            return result;
        }

        public static byte[] SendReceive(this ITransmissionChannel channel, byte[] request, int responseSize)
        {
            var response = new byte[responseSize];
            lock (channel)
            {
                channel.Write(request, 0, request.Length);
                channel.Read(response, 0, responseSize);
            }
            return response;
        }
    }
}