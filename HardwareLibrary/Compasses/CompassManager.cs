﻿using DspDataModel;
using DspDataModel.Hardware;

namespace HardwareLibrary.Compasses
{
    public class CompassManager : ICompassManager
    {
        public ICompass FirstCompass { get; private set; }

        public ICompass SecondCompass { get; private set; }

        public CompassManager(HardwareConfig config)
        {
            FirstCompass = CreateCompass(config.FirstCompassSettings);
            SecondCompass = CreateCompass(config.SecondCompassSettings);
        }

        public CompassManager() : this(Config.Instance.HardwareSettings)
        { }

        private ICompass CreateCompass(CompassConfig config) => config.IsSimulation 
            ? new CompassMock() as ICompass
            : new RionCompass(config.ComPortSettings.ComPortName, config.ComPortSettings.BaudRate);
    }
}
