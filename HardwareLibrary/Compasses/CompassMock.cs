﻿using DspDataModel.Hardware;

namespace HardwareLibrary.Compasses
{
    public class CompassMock : ICompass
    {
        public bool IsWorking => true;

        public double GetHeading() => -1;

        public double GetPitch() => -1;

        public double GetRoll() => -1;
    }
}
