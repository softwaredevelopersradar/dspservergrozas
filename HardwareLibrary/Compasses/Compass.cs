﻿using Compass;
using DspDataModel;
using DspDataModel.Hardware;

namespace HardwareLibrary.Compasses
{
    public class RionCompass : ICompass
    {
        private readonly CompassClass _compass;
        private readonly string _comPortName;
        private readonly int _baudRate;

        public bool IsWorking { get; private set; }

        public RionCompass(string comPortName, int baudRate = 9600)
        {
            _comPortName = comPortName;
            _baudRate = baudRate;
            _compass = new CompassClass(comPortName, (Speed)baudRate);
            _compass.IsPortOpened += OnPortOpened;
            OpenPort();
        }

        private void OpenPort()
        {
            var result = _compass.Open();
            if (result == false)
                MessageLogger.Warning($"Couldn't open port {_comPortName}:{_baudRate}. The compass is thus disabled");
        }

        private void OnPortOpened(object sender, bool portState)
        {
            IsWorking = portState;
        }

        public RionCompass() : this(Config.Instance.HardwareSettings.FirstCompassSettings.ComPortSettings.ComPortName,
            Config.Instance.HardwareSettings.FirstCompassSettings.ComPortSettings.BaudRate)
        {}

        public double GetHeading() => IsWorking ? _compass.GetHeading() : -1;

        public double GetPitch() => IsWorking ? _compass.GetPitch() : -1;

        public double GetRoll() => IsWorking ? _compass.GetRoll() : -1;
    }
}
