﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientDataBase;
using DataStorages;
using DspDataModel;
using DspDataModel.Database;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using InheritorsEventArgs;
using GrozaSModelsDBLib;

namespace DatabaseLibrary
{
    public class DatabaseController : IDatabaseController
    {
        public bool IsConnected { get; private set; }

        public event EventHandler<IEnumerable<Filter>> IntelligenceFiltersReceivedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> KnownRangesReceivedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenRangesReceivedEvent;

        private readonly DatabaseConfig _config;
        private int _reconnectionAttemptsCount;
        private const int _sliceSize = 500;

        private readonly ClientDB _client;
        private readonly IClassTables IntelligenceFiltersTable;
        private readonly IClassTables KnownFrequenciesTable;
        private readonly IClassTables ForbiddenFrequenciesTable;
        private readonly IClassTables RadiosourceTable;
        private readonly IClassTables LinkedStationsTable;

        /// <summary>
        /// number of tickets equals number of operations with database, usually two (clear and write)
        /// </summary>
        private readonly List<Ticket> _tickets = new List<Ticket>(5);
        private readonly object _reconnectLockObject = new object();

        private TableJammerStation _ownStationAsp;

        public DatabaseController()
        {
            _config = Config.Instance.DatabaseSettings;
            _client = new ClientDB("DspServer", $"{_config.DatabaseHost}:{_config.DatabasePort}");
            _reconnectionAttemptsCount = _config.ReconnectionAttemptsCount;
            
            RadiosourceTable = _client.Tables[NameTable.TableSource];
            IntelligenceFiltersTable = _client.Tables[NameTable.TableFreqRangesRecon];
            KnownFrequenciesTable = _client.Tables[NameTable.TableFreqKnown];
            ForbiddenFrequenciesTable = _client.Tables[NameTable.TableFreqForbidden];
            LinkedStationsTable = _client.Tables[NameTable.TableJammerStation];
        }

        private void SubscribeToDatabaseEvents()
        {
            _client.OnConnect += OnConnect;
            _client.OnDisconnect += OnDisconnect;
            _client.OnErrorDataBase += OnError;

            (IntelligenceFiltersTable as ITableUpdate<TableFreqRangesRecon>).OnUpTable += OnIntelligenceFiltersReceived;
            (LinkedStationsTable as ITableUpdate<TableJammerStation>).OnUpTable += OnLinkedStationReceived;
            (ForbiddenFrequenciesTable as ITableUpdate<TableFreqForbidden>).OnUpTable += OnForbiddenFrequenciesReceived;
            (KnownFrequenciesTable as ITableUpdate<TableFreqKnown>).OnUpTable += OnKnownFrequenciesReceived;
        }

        private void UnsubscribeFromDatabaseEvents()
        {
            _client.OnConnect -= OnConnect;
            _client.OnDisconnect -= OnDisconnect;
            _client.OnErrorDataBase -= OnError;

            (IntelligenceFiltersTable as ITableUpdate<TableFreqRangesRecon>).OnUpTable -= OnIntelligenceFiltersReceived;
            (LinkedStationsTable as ITableUpdate<TableJammerStation>).OnUpTable -= OnLinkedStationReceived;
            (ForbiddenFrequenciesTable as ITableUpdate<TableFreqForbidden>).OnUpTable -= OnForbiddenFrequenciesReceived;
            (KnownFrequenciesTable as ITableUpdate<TableFreqKnown>).OnUpTable -= OnKnownFrequenciesReceived;
        }

        private void OnConnect(object sender, ClientEventArgs args)
        {
            IsConnected = true;
            _reconnectionAttemptsCount = _config.ReconnectionAttemptsCount;
            MessageLogger.Log($"Connected to database {_config.DatabaseHost}:{_config.DatabasePort}");
            LoadAllTables();
        }

        private void OnDisconnect(object sender, ClientEventArgs args)
        {
            IsConnected = false;
            MessageLogger.Warning($"Disconnected from database : {args.GetMessage}, trying to reconnect");
            UnsubscribeFromDatabaseEvents();
            Task.Run(Reconnect);
        }

        private void OnError(object sender, OperationTableEventArgs args)
        {
            MessageLogger.Warning($"Database error : {args.TypeError}, " +
                                  $"message : {args.GetMessage}, " +
                                  $"table name : {args.TableName}, " +
                                  $"operation : {args.Operation}");
        }

        private void OnIntelligenceFiltersReceived(object sender, TableEventArgs<TableFreqRangesRecon> args)
        {
            var filters = args.Table
                .Where(filter => filter.IsActive)
                .Select(f => new Filter((int)f.FreqMinKHz, (int)f.FreqMaxKHz, 0, 360));
            IntelligenceFiltersReceivedEvent?.Invoke(sender, filters);
        }

        private void OnLinkedStationReceived(object sender, TableEventArgs<TableJammerStation> args)
        {
            if(CheckTicket(NameTable.TableJammerStation))
                return;
            if (args.Table.Count == 0)
            {
                //clearing everything
                IntelligenceFiltersReceivedEvent?.Invoke(this, new Filter[0]);
                KnownRangesReceivedEvent?.Invoke(this, new FrequencyRange[0]);
                ForbiddenRangesReceivedEvent?.Invoke(this, new FrequencyRange[0]);
                return;
            }
            
            var own = args.Table.FirstOrDefault(station => station.IsOwn == true);
            if (own == null)
            {
                MessageLogger.Warning($"There is no station with property IsOwn == true. Update table asp");
                return;
            }

            _ownStationAsp = own;
            Config.Instance.StationsSettings.OwnPosition = new StationPositionConfig()
            {
                StationId = own.Id,
                Altitude = own.Coordinates.Altitude,
                Longitude = own.Coordinates.Longitude,
                Latitude = own.Coordinates.Latitude
            };
            //todo : remove
            //Config.Instance.StationsSettings.Role = (StationRole)own.Role;

            var linkedStations = Config.Instance.StationsSettings.LinkedPositions;
            linkedStations.Clear();
            foreach (var station in args.Table.Where(station => station.IsOwn == false))
            {
                linkedStations.Add(new StationPositionConfig()
                {
                    StationId = station.Id,
                    Altitude = station.Coordinates.Altitude,
                    Latitude = station.Coordinates.Latitude,
                    Longitude = station.Coordinates.Longitude
                });
            }
        }
        
        private void OnKnownFrequenciesReceived(object sender, TableEventArgs<TableFreqKnown> args)
        {
            KnownRangesReceivedEvent?.Invoke(sender,
                args.Table
                    //.Where(t => t.NumberASP == _ownStationAsp.Id)
                    .Select(r => new FrequencyRange((float)r.FreqMinKHz, (float)r.FreqMaxKHz)).ToList());
        }

        private void OnForbiddenFrequenciesReceived(object sender, TableEventArgs<TableFreqForbidden> args)
        {
            ForbiddenRangesReceivedEvent?.Invoke(sender, 
                args.Table
                    //.Where(t => t.NumberASP == _ownStationAsp.Id)
                    .Select(r => new FrequencyRange((float)r.FreqMinKHz, (float)r.FreqMaxKHz)).ToList());
        }

        private void LoadAllTables()
        {
            OnLinkedStationReceived(this, new TableEventArgs<TableJammerStation>(LinkedStationsTable.Load<TableJammerStation>()));
            OnIntelligenceFiltersReceived(this, new TableEventArgs<TableFreqRangesRecon>(IntelligenceFiltersTable.Load<TableFreqRangesRecon>()));
            OnKnownFrequenciesReceived(this, new TableEventArgs<TableFreqKnown>(KnownFrequenciesTable.Load<TableFreqKnown>()));
            OnForbiddenFrequenciesReceived(this, new TableEventArgs<TableFreqForbidden>(ForbiddenFrequenciesTable.Load<TableFreqForbidden>()));

            ClearRadioSourceTable();
        }

        public async Task Connect()
        {
            try
            {
                if (!IsConnected)
                {
                    SubscribeToDatabaseEvents();
                    _client.Connect();
                }

                await Task.Delay(10);
            }
            catch (Exception e)
            {
                MessageLogger.Error($"Database connection error {e}");
            }
        }

        public void Disconnect()
        {
            try
            {
                if (IsConnected)
                {
                    UnsubscribeFromDatabaseEvents();
                    _client.Disconnect();
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error($"Error while trying to disconnect from database {e}");
            }
        }

        private async Task Reconnect()
        {
            lock (_reconnectLockObject)
            {
                while (_reconnectionAttemptsCount > 0 && !IsConnected)
                {
                    _reconnectionAttemptsCount--;
                    Connect();
                };

                if (!IsConnected)
                    MessageLogger.Error("Couldn't reconnect to database");
            }
        }

        public void RadioSourceTableAddRange(IEnumerable<IRadioSource> radioSources)
        {
            try
            {
                if (IsConnected)
                {
                    if (radioSources.Count() < _sliceSize)
                    {
                        RadiosourceTable.AddRange(radioSources.Select(RadioSourceToTempFWS).ToList());
                    }
                    else
                    {
                        /*
                         * we cut in slices, because wcf has max package size and ~600 sources is way to much for it
                         * and wcf connection dies.
                         */
                        var radioSourceArray = radioSources.ToArray();
                        
                        for (int i = 0; i < radioSourceArray.Length; i += _sliceSize)
                        {
                            RadiosourceTable.AddRange(SplitArray(radioSourceArray, i, _sliceSize).Select(RadioSourceToTempFWS).ToList());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error("Error while adding frs range" + e);
            }
        }

        public void ClearRadioSourceTable()
        {
            if (IsConnected)
                RadiosourceTable.Clear();
        }
        
        private TableSource RadioSourceToTempFWS(IRadioSource radioSource)
        {
            //var radioSourceDataList = new ObservableCollection<TableTrack>()
            //{
            //    new TableTrack()
            //    {
            //        Bearing = radioSource.IsDirectionReliable() ? radioSource.Direction : -1,
            //        DistanceKM = 0,
            //        Level = (short)radioSource.Amplitude,
            //        NumberASP = _ownStationAsp.Id,
            //        IsOwn = true,
            //        Std = radioSource.StandardDeviation
            //    }
            //};
            //var radioSourceCoordinates = new Coord()
            //{
            //    Altitude = (float)radioSource.Altitude,
            //    Latitude = radioSource.Latitude,
            //    Longitude = radioSource.Longitude
            //};
            //foreach (var linkedStationResult in radioSource.LinkedInfo)
            //{
            //    radioSourceDataList.Add(new JamDirect()
            //    {
            //        Bearing = linkedStationResult.Value.reliability > Constants.ReliabilityThreshold ? linkedStationResult.Value.direction : -1,
            //        DistanceKM = 0,
            //        Level = (short)radioSource.Amplitude,
            //        NumberASP = linkedStationResult.Key,
            //        IsOwn = false,
            //        Std = radioSource.StandardDeviation
            //    });
            //}

            //foreach (var jamDirect in radioSourceDataList)
            //{
            //    if (jamDirect.IsOwn)
            //    {
            //        CalculateDistance(jamDirect, _ownStationAsp.Coordinates, radioSourceCoordinates);
            //    }
            //    else
            //    {
            //        var station =
            //            Config.Instance.StationsSettings.LinkedPositions.FirstOrDefault(s =>
            //                s.StationId == jamDirect.NumberASP);
            //        CalculateDistance(
            //            jamDirect: jamDirect, 
            //            stationCoordinates: new Coord()
            //            {
            //                Altitude = station.Altitude,
            //                Latitude = station.Latitude,
            //                Longitude = station.Longitude
            //            }, 
            //            radioSourceCoord: radioSourceCoordinates
            //            );
            //    }
            //}
            
            return new TableSource
            {
                Id = radioSource.Id,
                //IdMission = 1,
                //Coordinates = radioSourceCoordinates,
                //Deviation = radioSource.BandwidthKhz,
                //FreqKHz = radioSource.FrequencyKhz,
                //ListQ = radioSourceDataList,
                //Time = radioSource.FirstBroadcastStartTime,
                //Type = (byte)radioSource.SourceType,
                //Checking = radioSource.IsActive,
                //Control = radioSource.IsActive ? Led.Green : Led.Empty
            };
            
            //void CalculateDistance(JamDirect jamDirect, Coord stationCoordinates, Coord radioSourceCoord)
            //{
            //    if (radioSourceCoord.Altitude == -1
            //        && radioSourceCoord.Latitude == -1
            //        && radioSourceCoord.Longitude == -1)
            //        return;

            //    jamDirect.DistanceKM = (float)ClassBearing.Distance(
            //            CoordJam: new List<Coord>()
            //            {
            //                stationCoordinates
            //            },
            //            CoordSource: radioSourceCoordinates)
            //        .First();
            //}
        }

        private IEnumerable<IRadioSource> SplitArray(IRadioSource[] radioSources, int startIndex, int count)
        {
            var amount = Math.Min(startIndex + count, radioSources.Length);
            var slice = new IRadioSource[amount - startIndex];
            for (int i = startIndex; i < amount; i++)
                slice[i - startIndex] = radioSources[i];
            return slice;
        }

        public void UpdateStationMode(DspServerMode mode)
        {
            _tickets.Add(new Ticket(NameTable.TableJammerStation));

            if (_ownStationAsp == null)
                return;

            //_ownStationAsp.Mode = ServerModeToByte(mode);
            LinkedStationsTable.Change(_ownStationAsp);

            byte ServerModeToByte(DspServerMode serverMode)
            {
                switch (serverMode)
                {
                    case DspServerMode.Stop: return 0;
                    case DspServerMode.RadioIntelligence: return 1;
                    case DspServerMode.RadioIntelligenceWithDf: return 1;
                    default: return 2;
                }
            }
        }

        private bool CheckTicket(NameTable tableToCheck)
        {
            var ticket = _tickets.FirstOrDefault(t => t.IsExpired() == false && t.Table == tableToCheck);
            if (ticket.IsDefault() == false)
            {
                _tickets.Remove(ticket);
                return true;
            }
            return false;
        }

        public void DeleteRadioSourceRecord(int id)
        {
            if(IsConnected)
                RadiosourceTable.Delete(new TableSource()
                {
                    Id = id
                });
        }
    }
}
