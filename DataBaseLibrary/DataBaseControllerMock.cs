﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Database;
using DspDataModel.Hardware;
using DspDataModel.LinkedStation;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;

namespace DatabaseLibrary
{
    public class DatabaseControllerMock : IDatabaseController
    {
        public bool IsConnected { get; private set; }
        public StationRole Role { get; set; } = StationRole.Standalone;
        public int OwnStationId { get; }
        public IReadOnlyList<int> LinkedStationsId { get; }
    
        public event EventHandler<IEnumerable<Filter>> IntelligenceFiltersReceivedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> KnownRangesReceivedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenRangesReceivedEvent;

        public void RadioSourceTableAddRange(IEnumerable<IRadioSource> radioSources)
        { }

        public void ClearRadioSourceTable()
        { }

        public async Task Connect()
        {
            IsConnected = true;
        }

        public void Disconnect()
        {
            IsConnected = false;
        }

        public void UpdateStationMode(DspServerMode mode)
        {}

        public void DeleteRadioSourceRecord(int id)
        {}
    }
}
