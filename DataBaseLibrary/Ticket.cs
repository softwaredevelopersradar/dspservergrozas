﻿using GrozaSModelsDBLib;
using Settings;
using System;

public struct Ticket
{
    public NameTable Table { get; }
    public DateTime CreationTime { get; }

    public Ticket(NameTable table)
    {
        Table = table;
        CreationTime = DateTime.Now;
    }

    public bool IsExpired() => 
        DateTime.Now.Subtract(CreationTime).TotalSeconds > Constants.DatabaseTicketExpirationTimeSec;

    public bool IsDefault() => Table == NameTable.TableTrack &&
                               CreationTime == new DateTime();
}