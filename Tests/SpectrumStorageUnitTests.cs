﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Settings;
using DataStorages;
using DspDataModel;
using DspDataModel.Data;
using NUnit.Framework;
using SharpExtensions;

namespace Tests
{
    [TestFixture]
    public class SpectrumStorageUnitTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [Test]
        public void TestRadioSourceStorageCreation()
        {
            var storage = new SpectrumStorage();

            var amplitudes = new float[Constants.BandSampleCount];
            for (var i = 0; i < Constants.BandSampleCount; ++i)
            {
                amplitudes[i] = Constants.ReceiverMinAmplitude;
            }

            for (int i = 0; i < Config.Instance.BandSettings.BandCount; ++i)
            {
                Assert.AreEqual(storage.GetSpectrum(i).Count, Constants.BandSampleCount);
            }
            // testing default values for spectrums (it should be just single line at Config.ReceiverMinAmplitude level)
            TestBands(storage.GetSpectrum(0).Amplitudes, amplitudes);
            TestBands(storage.GetSpectrum(Config.Instance.BandSettings.BandCount - 1).Amplitudes, amplitudes);
        }

        [Test]
        public void TestRadioSourceStorageGetSpectrumByBandNumber()
        {
            var storage = new SpectrumStorage();

            var amplitudes = new float[Constants.BandSampleCount];
            var now = DateTime.UtcNow;

            for (var i = 0; i < Constants.BandSampleCount; ++i)
            {
                amplitudes[i] = Constants.ReceiverMinAmplitude + (i % 10);
            }
            var scan = new AmplitudeScan(amplitudes, 1, now, 0);
            
            storage.Put(scan);
            TestBands(storage.GetSpectrum(scan.BandNumber).Amplitudes, amplitudes);

            storage.Put(scan);
            TestBands(storage.GetSpectrum(scan.BandNumber).Amplitudes, amplitudes);
        }

        private static void TestBands(IReadOnlyList<float> band1, IReadOnlyList<float> band2)
        {
            Assert.AreEqual(band1.Count, band2.Count);
            for (var i = 0; i < band1.Count; ++i)
            {
                Assert.IsTrue(band1[i].ApproxEquals(band2[i], 1e-2f));
            }
        }

        [Test]
        public void TestRadioSourceStorageGetSpectrum()
        {
            var storage = new SpectrumStorage();

            var amplitudes = new float[Constants.BandSampleCount];
            var now = DateTime.UtcNow;

            for (var i = 0; i < Constants.BandSampleCount; ++i)
            {
                amplitudes[i] = i;
            }
            var scan = new AmplitudeScan(amplitudes, 1, now, 0);
            var scan2 = new AmplitudeScan(amplitudes, 2, now, 0);

            storage.Put(scan);
            storage.Put(scan2);

            // testing reducing data size
            var band = storage.GetSpectrum(Constants.FirstBandMinKhz, Constants.FirstBandMinKhz + Constants.BandwidthKhz, 100);
            Assert.AreEqual(band.Count, 100);
            TestBands(band.Amplitudes, Enumerable.Repeat(Constants.ReceiverMinAmplitude, 100).ToArray());

            band = storage.GetSpectrum(Utilities.GetBandMinFrequencyKhz(1), Utilities.GetBandMaxFrequencyKhz(2), 100);
            // band should be symmetric and monotomically increasing
            for (int i = 1; i < 49; ++i)
            {
                Assert.IsTrue(band.Amplitudes[i] > band.Amplitudes[i - 1]);
                Assert.IsTrue(band.Amplitudes[50 + i + 1] > band.Amplitudes[50 + i]);
            }

            // testing passing data as is
            band = storage.GetSpectrum(Constants.FirstBandMinKhz, Constants.FirstBandMinKhz + Constants.BandwidthKhz, Constants.BandSampleCount);
            TestBands(band.Amplitudes, Enumerable.Repeat(Constants.ReceiverMinAmplitude, Constants.BandSampleCount).ToArray());

            band = storage.GetSpectrum(Utilities.GetBandMinFrequencyKhz(1), Utilities.GetBandMaxFrequencyKhz(2), Constants.BandSampleCount * 2);
            TestBands(band.Amplitudes, amplitudes.Concat(amplitudes).ToArray());

            // testing enlarging data size
            band = storage.GetSpectrum(Constants.FirstBandMinKhz, Constants.FirstBandMinKhz + Constants.BandwidthKhz, Constants.BandSampleCount * 2);
            TestBands(band.Amplitudes, Enumerable.Repeat(Constants.ReceiverMinAmplitude, Constants.BandSampleCount * 2).ToArray());

            band = storage.GetSpectrum(Utilities.GetBandMinFrequencyKhz(1), Utilities.GetBandMaxFrequencyKhz(2), Constants.BandSampleCount * 4);

            amplitudes = new float[Constants.BandSampleCount * 4];
            for (var i = 0; i < amplitudes.Length; ++i)
            {
                amplitudes[i] = (i % (Constants.BandSampleCount * 2)) * 0.5f;
            }
            amplitudes[19661] = 4915; // hack
            amplitudes[amplitudes.Length - 1] = Constants.BandSampleCount - 1; // hack
            TestBands(band.Amplitudes, amplitudes);

        }
    }
}
