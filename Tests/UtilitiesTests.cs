﻿using System.IO;
using DataStorages;
using DspDataModel;
using Settings;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class UtilitiesTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [Test]
        public void TestFrequencyToPointConversion()
        {
            var bandNumber = Config.Instance.BandSettings.BandCount - 2;
            var pointNumber = Constants.BandSampleCount * 7 / 10;
            var frequency = Utilities.GetFrequencyKhz(bandNumber, pointNumber);

            Assert.AreEqual(Utilities.GetBandNumber(frequency), bandNumber);
            Assert.AreEqual(Utilities.GetSampleNumber(frequency), pointNumber);
            Assert.AreEqual(Utilities.GetSamplesCount(frequency, frequency + Constants.SamplesGapKhz), 1);
            Assert.AreEqual(Utilities.GetSamplesCount(frequency, frequency + 100 * Constants.SamplesGapKhz), 100);
        }

        [Test]
        public void TestGetLiter()
        {
            Assert.AreEqual(1, Utilities.GetLiter(30_000));
            Assert.AreEqual(1, Utilities.GetLiter(35_000));
            Assert.AreEqual(2, Utilities.GetLiter(50_000));
            Assert.AreEqual(9, Utilities.GetLiter(2000_000));
            Assert.AreEqual(9, Utilities.GetLiter(3000_000));
        }

        [Test]
        public void GetBandNumberTest()
        {
            // check min frequency
            var frequency = Constants.FirstBandMinKhz;
            Assert.AreEqual(0, Utilities.GetBandNumber(frequency, BandBorderSelectionPolicy.TakeLeft, 100));
            Assert.AreEqual(0, Utilities.GetBandNumber(frequency, BandBorderSelectionPolicy.TakeRight, 100));

            // check usual frequency in the middle of the band
            frequency = Constants.FirstBandMinKhz + 10_000;
            Assert.AreEqual(0, Utilities.GetBandNumber(frequency, BandBorderSelectionPolicy.TakeLeft, 100));
            Assert.AreEqual(0, Utilities.GetBandNumber(frequency, BandBorderSelectionPolicy.TakeRight, 100));

            // check frequency in the bands border
            frequency = Constants.FirstBandMinKhz + Constants.BandwidthKhz;
            Assert.AreEqual(0, Utilities.GetBandNumber(frequency, BandBorderSelectionPolicy.TakeLeft, 100));
            Assert.AreEqual(1, Utilities.GetBandNumber(frequency, BandBorderSelectionPolicy.TakeRight, 100));

            // check max frequency
            frequency = (int) Utilities.GetFrequencyKhz(99, Constants.BandSampleCount - 1);
            Assert.AreEqual(99, Utilities.GetBandNumber(frequency, BandBorderSelectionPolicy.TakeLeft, 100));
            Assert.AreEqual(99, Utilities.GetBandNumber(frequency, BandBorderSelectionPolicy.TakeRight, 100));
        }

        [Test]
        public void TestGetSamplesCount()
        {
            var frequency = Constants.FirstBandMinKhz;

            Assert.AreEqual(Utilities.GetSamplesCount(frequency, frequency + Constants.SamplesGapKhz), 1);
            Assert.AreEqual(Utilities.GetSamplesCount(frequency, frequency + 100 * Constants.SamplesGapKhz), 100);
            Assert.AreEqual(Utilities.GetSamplesCount(
                    Utilities.GetBandMinFrequencyKhz(0),
                    Utilities.GetBandMaxFrequencyKhz(0)), Constants.BandSampleCount - 1);
        }

        [Test]
        public void TestAreRangesIntersected()
        {
            var r1 = new FrequencyRange(1000, 1100);
            var r2 = new FrequencyRange(1200, 1300);
            Assert.IsFalse(FrequencyRange.AreIntersected(r1, r2));
            Assert.IsFalse(FrequencyRange.AreIntersected(r2, r1));

            r2 = new FrequencyRange(1050, 1060);
            Assert.IsTrue(FrequencyRange.AreIntersected(r1, r2));
            Assert.IsTrue(FrequencyRange.AreIntersected(r2, r1));

            r2 = new FrequencyRange(1100, 1200);
            Assert.IsTrue(FrequencyRange.AreIntersected(r1, r2));
            Assert.IsTrue(FrequencyRange.AreIntersected(r2, r1));

            r2 = new FrequencyRange(1050, 1250);
            Assert.IsTrue(FrequencyRange.AreIntersected(r1, r2));
            Assert.IsTrue(FrequencyRange.AreIntersected(r2, r1));
        }

        [Test]
        public void TestRangesIntersection()
        {
            var r1 = new FrequencyRange(1000, 1100);
            var r2 = new FrequencyRange(1200, 1300);
            Assert.AreEqual(FrequencyRange.Intersection(r1, r2), null);
            Assert.AreEqual(FrequencyRange.Intersection(r2, r1), null);

            r2 = new FrequencyRange(1050, 1060);
            Assert.AreEqual(FrequencyRange.Intersection(r1, r2), r2);
            Assert.AreEqual(FrequencyRange.Intersection(r2, r1), r2);

            r2 = new FrequencyRange(1100, 1200);
            var result = new FrequencyRange(1100, 1100);
            Assert.AreEqual(FrequencyRange.Intersection(r1, r2), result);
            Assert.AreEqual(FrequencyRange.Intersection(r2, r1), result);

            r2 = new FrequencyRange(1050, 1150);
            result = new FrequencyRange(1050, 1100);
            Assert.AreEqual(FrequencyRange.Intersection(r1, r2), result);
            Assert.AreEqual(FrequencyRange.Intersection(r2, r1), result);
        }

        [Test]
        public void TestRangesIntersectionFactor()
        {
            var r1 = new FrequencyRange(1000, 1100);
            var r2 = new FrequencyRange(1200, 1300);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r1, r2), 0, 1e-3);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r2, r1), 0, 1e-3);

            r2 = new FrequencyRange(1050, 1060);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r1, r2), 1, 1e-3);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r2, r1), 1, 1e-3);

            r2 = new FrequencyRange(1100, 1200);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r1, r2), 0, 1e-3);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r2, r1), 0, 1e-3);

            r2 = new FrequencyRange(1050, 1150);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r1, r2), 0.5, 1e-3);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r2, r1), 0.5, 1e-3);
        }
    }
}
