﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DataStorages;
using DspDataModel.RadioJam;
using DspDataModel.Tasks;
using NUnit.Framework;
using Simulator;
using TasksLibrary.Tasks;

namespace Tests
{
    [TestFixture]
    public class TasksUnitTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [Test]
        public async Task TestRadioSourcesControlTask()
        {
            Utils.CreateClientAndServer(out var client, out var server, out var simulator);
            await Utils.Connect(client);
            var taskManager = server.TaskManager;

            // check task work without targets
            var task = RadioSourcesControlTask.Create(taskManager, new IRadioJamTarget[0]);
            await RunTask(task);
            Assert.AreEqual(task.TypedResult.Signals.Count, 0);

            // check task work with several targets on same band
            var signals = new[]
            {
                new Signal(frequencyKhz: 26000, bandwidthKhz: 30, direction: 20, amplitude: -55),
                new Signal(frequencyKhz: 30000, bandwidthKhz: 50, direction: 0, amplitude: -45),
            };
            foreach (var signal in signals)
            {
                simulator.SignalSimulator.Signals.Add(signal);
            }
            var targetConfig = new RadioJamTargetConfig(100);
            var targets = new[]
            {
                new RadioJamTarget(26000, 0, -70, 0, false, 0, 0, 0, 0, id: 0, liter: 0, targetConfig: targetConfig), // signal that should be found
                new RadioJamTarget(30000, 0, -30, 0, false, 0, 0, 0, 0, id: 1, liter: 0, targetConfig: targetConfig), // signal with too high threshold
                new RadioJamTarget(34000, 0, -70, 0, false, 0, 0, 0, 0, id: 2, liter: 0, targetConfig: targetConfig), // non existing signal
            };

            task = RadioSourcesControlTask.Create(taskManager, targets);
            await RunTask(task);
            Assert.AreEqual(task.TypedResult.Signals.Count, 1);
            Assert.AreEqual(task.TypedResult.Signals[0].Target, targets[0]);

            // check task work with more than 5 bands used
            signals = new[]
            {
                new Signal(frequencyKhz: 30000, bandwidthKhz: 1, direction: 0, amplitude: -45),
                new Signal(frequencyKhz: 70000, bandwidthKhz: 10, direction: 0, amplitude: -45),
                new Signal(frequencyKhz: 120_000, bandwidthKhz: 50, direction: 0, amplitude: -45),
                new Signal(frequencyKhz: 200_000, bandwidthKhz: 100, direction: 0, amplitude: -45),
                new Signal(frequencyKhz: 300_000, bandwidthKhz: 150, direction: 0, amplitude: -45),
                new Signal(frequencyKhz: 2_000_000, bandwidthKhz: 300, direction: 0, amplitude: -45),
            };
            simulator.SignalSimulator.Signals.Clear();
            foreach (var signal in signals)
            {
                simulator.SignalSimulator.Signals.Add(signal);
            }
            targets = new[]
            {
                new RadioJamTarget(30000, 0, -70, 0, false, 0, 0, 0, 0, id: 0, liter: 0, targetConfig: targetConfig),
                new RadioJamTarget(70000, 0, -70, 0, false, 0, 0, 0, 0, id: 1, liter: 0, targetConfig: targetConfig),
                new RadioJamTarget(120_000, 0, -70, 0, false, 0, 0, 0, 0, id: 2, liter: 0, targetConfig: targetConfig),
                new RadioJamTarget(200_000, 0, -70, 0, false, 0, 0, 0, 0, id: 3, liter: 0, targetConfig: targetConfig),
                new RadioJamTarget(300_000, 0, -70, 0, false, 0, 0, 0, 0, id: 4, liter: 0, targetConfig: targetConfig),
                new RadioJamTarget(2_000_000, 0, -70, 0, false, 0, 0, 0, 0, id: 5, liter: 0, targetConfig: targetConfig),
                new RadioJamTarget(31000, 0, -70, 0, false, 0, 0, 0, 0, id: 6, liter: 0, targetConfig: targetConfig), // non existing signal
            };

            task = RadioSourcesControlTask.Create(taskManager, targets);
            await RunTask(task);
            Assert.AreEqual(task.TypedResult.Signals.Count, 6);
            Assert.AreEqual(task.TypedResult.Signals.Select(s => s.Target), targets.Take(6));

            await Utils.StopClientAndServer(client, server, simulator);

            async Task RunTask(IReceiverTask t)
            {
                taskManager.AddTask(t);
                await t.WaitForResult();
            }
        }
    }
}
