﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataStorages;
using DspDataModel;
using DspDataModel.LinkedStation;
using DspDataModel.RadioJam;
using DspDataModel.Server;
using DspDataModel.Tasks;
using Nito.AsyncEx;
using TasksLibrary;
using TasksLibrary.Modes;
using TasksLibrary.Tasks;
using DataTransferLibrary;
using DspDataModel.Storages;
using FrequencyRange = DspDataModel.FrequencyRange;
using DspDataModel.DataTransfer;
using TasksLibrary.JamStrategies;

namespace DspServer
{
    public class ServerController : IServerController
    {
        public ITaskManager TaskManager { get; }
        public IRadioJamManager RadioJamManager { get; }
        public IDataTransferController DataTransferController { get; }

        public DspServerMode CurrentMode { get; private set; } = DspServerMode.Stop;
        public DspServerMode PreDurationMeasurementMode { get; private set; } = DspServerMode.Stop;


        public event EventHandler<IServerCommandResult> ModeChangedEvent;
        public event EventHandler<SectorAndRangesChangedEventArgs> SectorsAndRangesChangedEvent;
        public event EventHandler<RadioJamTargetsChangedEventArgs> RadioJamFrsTargetsChangedEvent;
        public event EventHandler<SpecialRangesChangedEventArgs> SpecialRangesChangedEvent;
        public event EventHandler<DspServerMode> SetLinkedStationSlaveModeEvent;
        public event EventHandler<RadioJamFhssTargetsChangedEventArgs> RadioJamFhssTargetsChangedEvent;
        public event EventHandler RequestFrsTargetsEvent;
        public event EventHandler<IReadOnlyCollection<IRadioSource>> ResponseFrsTargetsEvent;

        private readonly AsyncLock _setModeLock = new AsyncLock();

        private readonly Config _config;

        public ServerController(UpdateFrsJamStateDelegate updateFrsJamState, UpdateFhssJamStateDelegate updateFhssJamState)
            : this(updateFrsJamState, updateFhssJamState, Config.Instance)
        { }

        public ServerController(UpdateFrsJamStateDelegate updateFrsJamState, UpdateFhssJamStateDelegate updateFhssJamState, Config config)
        {
            _config = config;
            TaskManager = new TaskManager(_config);
            DataTransferController = new DataTransferController();
            SubscribeOnLinkedStationEvents();
            DataTransferController.Connect();

            var radioJamStorage = new RadioJamTargetStorage();
            RadioJamManager = new RadioJamManager(this, radioJamStorage, updateFrsJamState, updateFhssJamState);

            //todo : change to internal events with dbc as reference
            TaskManager.FhssDurationMeasuringStartEvent += OnFhssDurationMeasuringStart;
            TaskManager.HardwareController.GpsReceiver.UpdateLocalTimeEvent += UpdateLocalTime;
        }

        private void UpdateLocalTime(object sender, DateTime localTime)
        {
            DataTransferController.SendLocalTime(localTime);
        }

        private void OnFhssDurationMeasuringStart(object sender, EventArgs e)
        {
            SetMode(DspServerMode.FhssDurationMeasuring).Wait(10000);//todo : check wait
        }

        public bool Initialize() => TaskManager.Initialize();

        private void SubscribeOnLinkedStationEvents()
        {
            DataTransferController.PerformExecutiveDfRequest += (sender, args) =>
                Task.Run(async () =>
                {
                    var signal = await GetSignal(args.startFrequencyKhz, args.endFrequencyKhz, args.phaseAveragingCount, args.directionAveragingCount);
                    float direction = -1;
                    if (signal?.IsDirectionReliable() ?? false)
                    {
                        direction = signal.Direction;
                    }
                    DataTransferController.SendExecutiveDfResponse(Config.Instance.StationsSettings.OwnPosition.StationId, signal.FrequencyKhz, direction);
                });
            DataTransferController.SetModeRequest += (sender, mode) => SetMode(mode);
            DataTransferController.SignalsResultsReceivedEvent += (sender, tuple) =>
            {
                foreach (var signal in tuple.signals)
                {
                    var source = TaskManager.RadioSourceStorage.FindSameLinkedStationRadioSource(signal);
                    if(source != null)
                        source.UpdateLocation(signal.Latitude, signal.Longitude, signal.Altitude);
                }
            };
        }

        private void OnFrsJammingTargetsChanged(object sender, IReadOnlyCollection<IRadioJamTarget> targets)
        {
            var targetsWithConfig = targets.Select(t => new RadioJamTarget(
                frequencyKhz: t.FrequencyKhz,
                priority: t.Priority,
                threshold: t.Threshold,
                direction: t.Direction,
                useAdaptiveThreshold: false,
                modulationCode: t.ModulationCode,
                deviationCode: t.DeviationCode,
                manipulationCode: t.ManipulationCode,
                durationCode: t.DurationCode,
                id: t.Id,
                liter: t.Liter,
                targetConfig: RadioJamManager
            )).ToArray();

            SetFrsJammingTargets(targetsWithConfig, TargetStation.Current);
        }

        private void OnLinkedStationSetFhssJammingTargets(object sender, IReadOnlyCollection<IRadioJamFhssTarget> targets)
        { 
            SetFhssJammingTargets(targets, TargetStation.Current);
        }

        public void UpdateFrsRadioJamTargets(IReadOnlyList<IRadioJamTarget> targets, int clientId = -1)
        {
            var areTargetsChanged = RadioJamManager.Storage.UpdateFrsTargets(targets);
            if (areTargetsChanged)
            {
                var eventArgs = new RadioJamTargetsChangedEventArgs(targets, TargetStation.Current, RequestResult.Ok,  clientId, true);
                RadioJamFrsTargetsChangedEvent?.Invoke(this, eventArgs);
            }
        }
        
        public async Task<RequestResult> SetMode(DspServerMode serverMode, int clientId = -1)
        {
            if (CurrentMode == serverMode && serverMode != DspServerMode.Calibration)
            {
                return RequestResult.Ok;
            }

            using (await _setModeLock.LockAsync())
            {
                if (!serverMode.IsRadioJamMode() && RadioJamManager.IsJammingActive)
                {
                    await RadioJamManager.Stop();
                }
                if (serverMode == DspServerMode.Stop)
                {
                    TaskManager.ClearTasks();
                }
                else if (serverMode.IsRadioJamMode())
                {
                    TaskManager.FilterManager.WorkingRangeType = RangeType.RadioJamming;
                    if (TaskManager.FilterManager.Filters.Count == 0)
                    {
                        TaskManager.FilterManager.WorkingRangeType = RangeType.Intelligence;
                        return RequestResult.InvalidRequest;
                    }

                    var jammingMode = ServerModeToRadioJamMode(serverMode);
                    if (jammingMode != RadioJamMode.Fhss &&
                        jammingMode != RadioJamMode.Voice &&
                        RadioJamManager.EmitDuration < 0)
                    {
                        return RequestResult.InvalidRequest;
                    }
                    TaskManager.ClearTasks();
                    RadioJamManager.Start(jammingMode);
                }
                else if (serverMode == DspServerMode.FhssDurationMeasuring)
                {
                    var durationMeasuringTask = new FhssDurationMeasureStrategy(TaskManager, this);
                    TaskManager.ClearTasks();
                    Task.Run(() => durationMeasuringTask.JammingTask());
                }
                else
                {
                    // Intelligence
                    if (CurrentMode != DspServerMode.FhssDurationMeasuring)//no clears after measuring
                    {
                        TaskManager.SignalStorage.Clear();
                        TaskManager.FhssNetworkStorage.Clear();
                        TaskManager.RadioSourceStorage.Clear();
                    }

                    if (serverMode != DspServerMode.Calibration && TaskManager.FilterManager.Filters.Count == 0)
                    {
                        return RequestResult.InvalidRequest;
                    }

                    TaskManager.FilterManager.WorkingRangeType = RangeType.Intelligence;
                    var modeController = ServerModeToModeController(serverMode);
                    await modeController.Initialize();
                    TaskManager.SetMode(modeController);
                }

                if (serverMode == DspServerMode.FhssDurationMeasuring &&
                    PreDurationMeasurementMode == DspServerMode.Stop)
                    PreDurationMeasurementMode = CurrentMode;
                else
                    PreDurationMeasurementMode = DspServerMode.Stop; //nullifying preMode just in case of something

                var sendEvent = serverMode != DspServerMode.FhssDurationMeasuring && 
                                CurrentMode != DspServerMode.FhssDurationMeasuring &&
                                serverMode != DspServerMode.Calibration &&
                                CurrentMode != DspServerMode.Calibration;
                CurrentMode = serverMode;

                if (sendEvent)
                {
                    ModeChangedEvent?.Invoke(this, new ServerCommandResult(RequestResult.Ok, true, clientId));
                    TaskManager.DatabaseController.UpdateStationMode(serverMode);
                    if (Config.Instance.StationsSettings.Role == StationRole.Master)
                        DataTransferController.SetMode(CurrentMode);
                }

                return RequestResult.Ok;
            }

            RadioJamMode ServerModeToRadioJamMode(DspServerMode mode)
            {
                switch (mode)
                {
                    case DspServerMode.RadioJammingFrs:
                        return RadioJamMode.Frs;
                    case DspServerMode.RadioJammingAfrs:
                        return RadioJamMode.Afrs;
                    case DspServerMode.RadioJammingVoice:
                        return RadioJamMode.Voice;
                    case DspServerMode.RadioJammingFhss:
                        return RadioJamMode.Fhss;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
                }
            }

            IModeController ServerModeToModeController(DspServerMode mode)
            {
                switch (mode)
                {
                    case DspServerMode.RadioIntelligence:
                        return new SequenceReceiversMode(TaskManager);
                    case DspServerMode.RadioIntelligenceWithDf:
                        return new RdfMode(TaskManager, DataTransferController);
                    case DspServerMode.Calibration:
                        return new CalibrationMode(TaskManager);
                    default:
                        throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
                }
            }
        }

        public bool PerformStorageAction(StorageType storage, SignalAction action, int[] signalIds)
        {
            var isStorageChanged = false;

            if (action == SignalAction.Hide && signalIds.Length == 0)
            {
                ClearStorage();
            }
            else
            {
                PerformAction();
            }

            return isStorageChanged;

            void PerformAction()
            {
                if (signalIds.Length == 0)
                {
                    return;
                }
                switch (storage)
                {
                    case StorageType.Frs:
                        TaskManager.RadioSourceStorage.PerformAction(action, signalIds);
                        break;
                    case StorageType.Fhss:
                        TaskManager.FhssNetworkStorage.PerformAction(action, signalIds);
                        break;
                    default:
                        MessageLogger.Error("perform action storage request error: unkwown storage type!");
                        throw new ArgumentOutOfRangeException();
                }

                isStorageChanged = true;
            }
            void ClearStorage()
            {
                switch (storage)
                {
                    case StorageType.Frs:
                        isStorageChanged = TaskManager.RadioSourceStorage.GetRadioSources().Any();
                        TaskManager.RadioSourceStorage.Clear();
                        break;
                    case StorageType.Fhss:
                        isStorageChanged = TaskManager.FhssNetworkStorage.GetFhssNetworks().Any();
                        TaskManager.FhssNetworkStorage.Clear();
                        break;
                    default:
                        MessageLogger.Error("Clear storage request error: unknown storage type!");
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        
        public async Task<RequestResult> SetFrsJammingTargets(IReadOnlyCollection<IRadioJamTarget> targets, TargetStation station, int clientId = -1)
        {
            SetTargets();
            switch (station)
            {
                case TargetStation.Current:
                    return RequestResult.Ok;
                default:
                    throw new ArgumentOutOfRangeException(nameof(station), station, null);
            }

            void SetTargets()
            {
                var storage =
                    station == TargetStation.Current ?
                    RadioJamManager.Storage : RadioJamManager.LinkedStationStorage;
                var areTargetsChanged = storage.UpdateFrsTargets(targets.ToList());
                if (areTargetsChanged)
                {
                    var eventArgs = new RadioJamTargetsChangedEventArgs(targets.ToList(), station, RequestResult.Ok,  clientId, true);
                    RadioJamFrsTargetsChangedEvent?.Invoke(this, eventArgs);
                }
            }
        }

        public async Task<RequestResult> SetFhssJammingTargets(IReadOnlyCollection<IRadioJamFhssTarget> targets, TargetStation station, int clientId = -1)
        {
            SetTargets();
            switch (station)
            {
                case TargetStation.Current:
                    return RequestResult.Ok;
                default:
                    throw new ArgumentOutOfRangeException(nameof(station), station, null);
            }

            void SetTargets()
            {
                var storage = 
                    station == TargetStation.Current ? 
                    RadioJamManager.Storage : RadioJamManager.LinkedStationStorage;
                var areTargetsChanged = storage.UpdateFhssTargets(targets.ToList());
                if (areTargetsChanged)
                {
                    var eventArgs = new RadioJamFhssTargetsChangedEventArgs(targets.ToList(), station, RequestResult.Ok, clientId, true);
                    RadioJamFhssTargetsChangedEvent?.Invoke(this, eventArgs);
                }
            }
        }

        public async Task<IReadOnlyList<ISignal>> GetSignals(IReadOnlyList<FrequencyRange> frequencyRanges, int phaseAveragingCount, int directionAveragingCount, bool calculateHighQualityPhases = false)
        {
            var taskSetup = new DirectionFindingTaskSetup
            {
                TaskManager = TaskManager,
                FrequencyRanges = frequencyRanges,
                Priority = 1,
                ReceiversIndexes = new[] { 0, 1, 2, 3, 4 },
                ScanAveragingCount = phaseAveragingCount,
                BearingAveragingCount = directionAveragingCount,
                IsEndless = false,
                AutoUpdateObjectives = false,
                CalculateHighQualityPhases = calculateHighQualityPhases
            };
            var task = new DirectionFindingTask(taskSetup);
            TaskManager.AddTask(task);
            await task.WaitForResult().ConfigureAwait(false);

            return task.TypedResult.Signals;
        }

        public async Task<ISignal> GetSignal(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount, bool calculateHighQualityPhases = false)
        {
            var frequencyRanges = new[] {new FrequencyRange(startFrequencyKhz, endFrequencyKhz)};
            var signals = await GetSignals(frequencyRanges, phaseAveragingCount, directionAveragingCount, calculateHighQualityPhases);
            return signals?.OrderByDescending(s => s.Amplitude).FirstOrDefault();
        }
    }
}
