﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Correlator.CalibrationCorrection;
using Settings;
using DataProcessor;
using DataStorages;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using NetLib;
using Protocols;
using RadioRecorder;
using SharpExtensions;
using TasksLibrary.Modes;
using TasksLibrary.Tasks;
using Phases;
using DspDataModel.RadioJam;
using DspDataModel.Correlator;
using DspDataModel.LinkedStation;
using DspDataModel.Server;
using DspProtocols;
using llcss;
using System.Collections.Concurrent;
using FrequencyType = DspDataModel.FrequencyType;
using RadioSource = Protocols.RadioSource;

namespace DspServer
{
    public class DspServer
    {
        private readonly NetServer _server;

        public ITaskManager TaskManager { get; }

        public IRadioJamManager RadioJamManager { get; }

        public ISpectrumStorage SpectrumStorage { get; }

        public IRadioSourceStorage RadioSourceStorage { get; }

        public IServerController Controller { get; }

        public bool IsRecording => Recorder.IsRecording;

        public Recorder Recorder { get; }

        public event EventHandler ServerStartedEvent;

        public bool IsWorking => _server.IsWorking;

        private IPhaseCorrelator PhaseCorrelator => TaskManager.DataProcessor.PhaseCorrelator;

        public string Host => _server.Host;

        public int Port => _server.Port;

        private readonly TaskCompletionSource<bool> _workTaskCompletionSource;

        private readonly Config _config;

        private readonly ConcurrentDictionary<int, DateTime> _lastRequestTimeDictionary;

        public DspServer() : this(Config.Instance)
        { }

        public DspServer(Config config)
        {
            _config = config;

            _server = new NetServer(bufferSize: MessageHeader.BinarySize);
            _server.DataReadEvent += OnDataRead;
            _server.ConnectEvent += OnClientConnect;
            _server.StopEvent += OnServerStopped;
            _lastRequestTimeDictionary = new ConcurrentDictionary<int, DateTime>();

            _server.Server.StartEvent += (s, e) =>
            {
                ServerStartedEvent?.Invoke(this, EventArgs.Empty);
            };

            
            _workTaskCompletionSource = new TaskCompletionSource<bool>();
            Controller = new ServerController(SendFrsRadioJamStateEvent, SendFhssRadioJamStateEvent, _config);
            SubscribeOnServerControllerEvents();

            TaskManager = Controller.TaskManager;
            RadioJamManager = Controller.RadioJamManager;

            SpectrumStorage = TaskManager.SpectrumStorage;
            RadioSourceStorage = TaskManager.RadioSourceStorage;

            Recorder = new Recorder(_config.RecorderSettings.RecorderQuality);

            TaskManager.HardwareController.ScanReadEvent += OnScanRead;
            RadioJamManager.Shaper.ConnectionStateChangedEvent += OnShaperConnectionChanged;
            RadioJamManager.Shaper.ShaperConditionReceivedEvent += OnShaperConditionReceived;

            TaskManager.HardwareController.GpsReceiver.UpdateLocalTimeEvent += UpdateLocalTime;
            Controller.DataTransferController.SetLocalTimeRequest += UpdateLocalTime;
        }

        private void OnRadioJamFrsTargetsChanged(object sender, RadioJamTargetsChangedEventArgs e)
        {
            try
            {
                var commandResult = e.Result;
                if (commandResult != RequestResult.Ok || !e.SendBroadcastEvent)
                {
                    return;
                }
                var targets = GetFrsJammingTargets(e.RadioJamTargets);

                var header = new MessageHeader(0, 0, (byte) CommandCode.SetFrsRadioJamTargets, 0, targets.Length * FRSJammingSetting.BinarySize + 1);
                var message = FrsJammingMessage.ToBinary(header, e.Station, targets);
                var client = _server.ClientHandlers.FirstOrDefault(c => c.Id == e.ClientId);

                SendBroadcastEvent(client, message);
                LogResponse(header.Code, $"{e.Station.ToString()} station frs radio jam targets changed", (RequestResult)header.ErrorCode);
            }
            catch (Exception ex)
            {
                MessageLogger.Warning("Frs radio fam targets changed broadcast event error! " + ex.StackTrace);
            }
        }

        private void OnRadioJamFhssTargetsChanged(object sender, RadioJamFhssTargetsChangedEventArgs e)
        {
            try
            {
                var commandResult = e.Result;
                if (commandResult != RequestResult.Ok || !e.SendBroadcastEvent)
                {
                    return;
                }
                var storage = e.Station == TargetStation.Current ?
                    RadioJamManager.Storage : RadioJamManager.LinkedStationStorage;
                var targets = GetFhssJammingSetting(storage);
                var targetsLength = targets.Sum(t => t.StructureBinarySize);
                var responseHeader = new MessageHeader(0,0,7,0, targetsLength + 7);
                var response = FhssJammingMessage.ToBinary(responseHeader, RadioJamManager.EmitDuration, RadioJamManager.FhssFftResoultion, e.Station, (byte)targets.Length, targets);

                var client = _server.ClientHandlers.FirstOrDefault(c => c.Id == e.ClientId);

                SendBroadcastEvent(client, response);
                LogResponse(responseHeader.Code, $"{e.Station.ToString()} station fhss radio jam targets changed", (RequestResult)responseHeader.ErrorCode);
            }
            catch (Exception ex)
            {
                MessageLogger.Warning("Frs radio fam targets changed broadcast event error! " + ex.StackTrace);
            }
        }

        private void OnShaperConnectionChanged(object sender, bool isConnected)
        {
            SendShaperStateEvent(isConnected);
        }

        private void OnShaperNotRespondingEvent(object sender, EventArgs args)
        {
            try
            {
                Controller.SetMode(DspServerMode.Stop);
                var header = new MessageHeader(0, 0, 64, 0, 0);
                var message = DefaultMessage.ToBinary(header);
                SendBroadcastEvent(message);
            }
            catch (Exception e)
            {
                MessageLogger.Warning("Shaper is not responding! " + e.StackTrace);
            }
        }

        private void OnShaperConditionReceived(object sender, byte[] args)
        {
            var errors = new byte[] { args[1], args[2], args[3]};
            switch ((ShaperCommandCodes)args[0])
            {
                case ShaperCommandCodes.Power:
                {
                    var power = new short[(args.Length - 4) / 3];
                    for (int i = 0; i < power.Length; i++)
                    {
                        power[i] = args[i * 3 + 6];
                    }

                    OnShaperPowerUpdateEvent(sender, errors, power);
                    break;
                }
                case ShaperCommandCodes.State:
                {
                    var state = new byte[args.Length - 4];
                    for (int i = 0; i < state.Length; i++)
                    {
                        state[i] = args[i + 4];
                    }

                    OnShaperStateUpdateEvent(sender, errors, state);
                    break;
                }
                case ShaperCommandCodes.Voltage:
                {
                    var power = new float[(args.Length - 4) / 3];
                    for (int i = 0; i < power.Length; i++)
                    {
                        power[i] = BitConverter.ToUInt16(args, 5 + i * 3);
                    }

                    OnShaperVoltageUpdateEvent(sender, errors, power);
                    break;
                }
                case ShaperCommandCodes.Current:
                {
                    var amperage = new byte[args.Length - 4];
                    for (int i = 0; i < amperage.Length; i++)
                    {
                        amperage[i] = args[i + 4];
                    }

                    OnShaperCurrentUpdateEvent(sender, errors, amperage);
                    break;
                }
                case ShaperCommandCodes.Temperature:
                {
                    var temperature = new byte[args.Length - 4];
                    for (int i = 0; i < temperature.Length; i++)
                    {
                        temperature[i] = args[i + 4];
                    }

                    OnShaperTemperatureUpdateEvent(sender, errors, temperature);
                    break;
                }
                default:
                {
                    MessageLogger.Warning("Wrong shaper command code, while trying to broadcast shaper condition");
                    break;
                }
            }
        }

        private void OnSetLinkedStationSlaveMode(object sender, DspServerMode mode)
        {
            try
            {
                var header = new MessageHeader(0, 0, 29, 0, ModeMessage.BinarySize - MessageHeader.BinarySize);
                var message = ModeMessage.ToBinary(header, mode);
                var client = _server.ClientHandlers.FirstOrDefault();
                if (client == null)
                    return;
                SendToClient(client,message,header,"Set linked station client mode");
            }
            catch (Exception e)
            {
                MessageLogger.Warning("Set linked station mode error! " + e.StackTrace);
            }
        }

        public async Task Stop()
        {
            TaskManager.SpectrumHistoryStorage.StopSpectrumUpdate();
            await _server.Stop().ConfigureAwait(false);
        }

        private void OnScanRead(object sender, IFpgaDataScan fpgaDataScan)
        {
            if (IsRecording)
            {
                Recorder.Record(fpgaDataScan);
            }
        }

        private void OnServerStopped(object sender, EventArgs e)
        {
            _workTaskCompletionSource.SetResult(true);
        }

        private void OnClientConnect(object sender, TcpClient tcpClient)
        {
            MessageLogger.ServerLog("Client connected.");
            try
            {
                /*
                 * since shaper is always connected and connection is performed,
                 * before any client may connect, clients can't receive shaper connection 
                 * state unless shaper disconnects or (connects) by itself
                */
                var client = _server.ClientHandlers.FirstOrDefault(c => c.Client == tcpClient);
                var header = new MessageHeader(0, 0, Code: 45,
                    ErrorCode: 0, InformationLength: ShaperStateUpdateEvent.BinarySize - MessageHeader.BinarySize);
                var message = ShaperStateUpdateEvent.ToBinary(header, Controller.RadioJamManager.Shaper.IsConnected);
                SendToClient(client, message, header, "Send shaper connection state");

                //TODO : deal with time sync
                /*
                var now = DateTime.UtcNow;
                var header = new MessageHeader(0, 0, 151, 0, SetTimeRequest.BinarySize - MessageHeader.BinarySize);
                var message = SetTimeRequest.ToBinary(header, (byte)now.Hour, (byte)now.Minute, (byte)now.Second, (short)now.Millisecond);
                var client = _server.ClientHandlers.FirstOrDefault(c => c.Client == tcpClient);
                SendToClient(client, message, header, "Time synchronization with dsp server");*/
            }
            catch (Exception e)
            {
                MessageLogger.Error($"Couldn't send time synchronization to client : {tcpClient.Client.RemoteEndPoint}");
            }
        }

        public void Start()
        {
            Start(_config.ServerSettings.ServerHost, _config.ServerSettings.ServerPort);
        }

        public void Start(string host, int port)
        {
           if (!Controller.Initialize())
           {
               throw new Exception("Can't initialize hardware");
           }

            _server.Start(host, port);
        }

        private void OnDataRead(object sender, NetServerDataReadEventArgs e)
        {
            if (!MessageHeader.TryParse(e.Data, out var header))
            {
                e.ClientHandler.Stop();
                return;
            }
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            try
            {
                e.ClientHandler.ReadExactBytesCount(buffer, MessageHeader.BinarySize, header.InformationLength);
            }
            catch (Exception)
            {
                e.ClientHandler.Stop();
                return;
            }
            Array.Copy(e.Data, 0, buffer, 0, MessageHeader.BinarySize);

            CommandSwitch(header, buffer, e.ClientHandler);
        }

        private void SubscribeOnServerControllerEvents()
        {
            Controller.ModeChangedEvent += OnModeChanged;
            Controller.SectorsAndRangesChangedEvent += OnSectorsAndRangesChanged;
            Controller.RadioJamFrsTargetsChangedEvent += OnRadioJamFrsTargetsChanged;
            Controller.SetLinkedStationSlaveModeEvent += OnSetLinkedStationSlaveMode;
            Controller.SpecialRangesChangedEvent += OnSpecialRangesChanged;
            Controller.RadioJamFhssTargetsChangedEvent += OnRadioJamFhssTargetsChanged;
            Controller.ResponseFrsTargetsEvent += OnResponseGetFrsTargets;
        }

        private void CommandSwitch(MessageHeader header, byte[] data, NetClientSlim clientHandler)
        {
            switch (header.Code)
            {
                case (byte) CommandCode.SetMode:
                    SetMode(header, data, clientHandler);
                    break;
                case (byte) CommandCode.SetAttenuatorsValues:
                    SetAttenuatorsValues(header, data, clientHandler);
                    break;
                case (byte) CommandCode.SetFrsRadioJamTargets:
                    SetFrsRadioJamTargets(header, data, clientHandler);
                    break;
                case (byte) CommandCode.SetFhssRadioJamTarget:
                    SetFhssRadioJamTargets(header, data, clientHandler);
                    break;
                case 8:
                    GetAttenuatorsValues(header, data, clientHandler);
                    break;
                case 9:
                    GetAmplifiersValues(header, data, clientHandler);
                    break;
                case 11:
                    GetRadioSources(header, data, clientHandler);
                    break;
                case 12:
                    GetSpectrum(header, data, clientHandler);
                    break;
                case 15:
                    SetCalibrationBands(header, data, clientHandler);
                    break;
                case 16:
                    Task.Run(() => PerformExecutiveDfRequest(header, data, clientHandler));
                    break;
                case 17:
                    Task.Run(() => PerformQuasiSimultaneousDfRequest(header, data, clientHandler));
                    break;
                case (byte) CommandCode.SetFilters:
                    SetFilters(header, data, clientHandler);
                    break;
                case 19:
                    Task.Run(() => GetTechAppSpectrum(header, data, clientHandler));
                    break;
                case 20:
                    SetReceiversChannel(header, data, clientHandler);
                    break;
                case 21:
                    Task.Run(() => GetHeterodyneRadioSources(header, data, clientHandler));
                    break;
                case 22:
                    GetCalibrationProgress(header, data, clientHandler);
                    break;
                case 23:
                    Task.Run(() => GetBandAmplitudeLevels(header, data, clientHandler));
                    break;
                case 24:
                    GetAmplitudeTimeSpectrum(header, data, clientHandler);
                    break;
                case 25:
                    StartRadioRecord(header, data, clientHandler);
                    break;
                case 26:
                    StopRadioRecord(header, data, clientHandler);
                    break;
                case 27:
                    SetDirectionCorrection(header, data, clientHandler);
                    break;
                case 28:
                    SetSynchronizationShift(header, data, clientHandler);
                    break;
                case 30:
                    CalculateCalibrationCorrection(header, data, clientHandler);
                    break;
                case 31:
                    GetAdaptiveThreshold(header, data, clientHandler);
                    break;
                case 32:
                    SetFrsRadioJamSettings(header, data, clientHandler);
                    break;
                case 34:
                    GetFhssNetworks(header, data, clientHandler);
                    break;
                case 35:
                    GetSpecialFrequencies(header, data, clientHandler);
                    break;
                case 36:
                    GetFilters(header, data, clientHandler);
                    break;
                case 37:
                    GetMode(header, data, clientHandler);
                    break;
                case 38:
                    GetSectorsAndRanges(header, data, clientHandler);
                    break;
                case 39:
                    GetFrsRadioJamTargets(header, data, clientHandler);
                    break;
                case 40:
                    GetFhssRadioJamTargets(header, data, clientHandler);
                    break;
                case 41:
                    GetScanSpeed(header, data, clientHandler);
                    break;
                case 44:
                    Task.Run(() => GetRadioControlSpectrum(header, data, clientHandler));
                    break;
                case 52:
                    PerformStorageAction(header, data, clientHandler);
                    break;
                case 53:
                    GetBearingPanoramaSignals(header, data, clientHandler);
                    break;
                case 54:
                    SetFrsAutoRadioJamSettings(header, data, clientHandler);
                    break;
                case 55:
                    SetAfrsRadioJamSettings(header, data, clientHandler);
                    break;
                case 57:
                    GetDirectionCorrection(header, data, clientHandler);
                    break;
                case 58:
                    GetSearchFhss(header,data,clientHandler);
                    break;
                case 59:
                    SetSearchFhss(header, data, clientHandler);
                    break;
                case 63:
                    SetAntennaDirections(header, data, clientHandler);
                    break;
                case 64:
                    //TODO: clear shit up (64 is occupied by "Shaper not responding event"?_?)
                    PhaseCorrelator.SaveTheoreticalTable("TheoreticalTable.bin");//TODO : add filename to config
                    break;
                case 65:
                    RemeasureFhssNetworksDuration(header, data, clientHandler);
                    break;
                case 66:
                    RestartGps(header, data, clientHandler);
                    break;
                case 67:
                    SetGpsSyncInterval(header, data, clientHandler);
                    break;
                case 68:
                    SetGpsPositionError(header, data, clientHandler);
                    break;
                case 70:
                    GetShaperStateRequest(header, data, clientHandler);
                    break;
                case 71:
                    GetShaperSettingsRequest(header, data, clientHandler);
                    break;
                case 72:
                    SetShaperAntennaRequest(header, data, clientHandler);
                    break;
                case 73:
                    ShaperStopJammingRequest(header, data, clientHandler);
                    break;
                case 74:
                    ShaperStartFrsJammingRequest(header, data, clientHandler);
                    break;
                case 75:
                    ShaperStartFhssJammingRequest(header, data, clientHandler);
                    break;
                case 76:
                    ShaperRestartRequest(header, data, clientHandler);
                    break;
                case 77:
                    ShaperStateUpdateIntervalRequest(header, data, clientHandler);
                    break;
                case 78:
                    ShaperSettingsUpdateIntervalRequest(header, data, clientHandler);
                    break;
                case 79:
                    GetBandsAdaptiveThreshold(header, data, clientHandler);
                    break;
                case 80:
                    SetVoiceJammingSettings(header, data, clientHandler);
                    break;
                case 81:
                    SetVoiceJamming(header, data, clientHandler);
                    break;
                case 82:
                    SetShaperTypeLoad(header, data, clientHandler);
                    break;
                case 83:
                    SetShaperPower(header, data, clientHandler);
                    break;
                case 90:
                    SetRcReceiver(header, data, clientHandler);
                    break;
                case 100:
                    SetCorrelationType(header, data, clientHandler);
                    break;
                default:
                    MessageLogger.Warning("Unknown request code");
                    var responseHeader = GetResponseMessageHeader(header, length: 0);
                    responseHeader.ErrorCode = (byte) RequestResult.InvalidRequest;
                    SendToClient(clientHandler, DefaultMessage.ToBinary(responseHeader), responseHeader, "Unknown request");
                    break;
            }
        }

        /// <summary>
        /// Sends message to all clients except senderClient
        /// </summary>
        private async Task SendBroadcastEvent(NetClientSlim senderClient, byte[] message)
        {
            var eventMessage = message.ToArray();
            // HACK!
            // Changing message code (event code is a relative message code + 100)
            eventMessage[2] += 100;

            try
            {
                foreach (var client in _server.ClientHandlers)
                {
                    if (!client.IsWorking)
                    {
                        _server.RemoveClient(client);
                    }
                }

                var tasks = _server.ClientHandlers
                    .Where(c => c != senderClient)
                    .Select(c => c.WriteAsync(eventMessage));
                await Task.WhenAll(tasks);
            }
            catch (Exception e)
            {
                MessageLogger.Warning($"Broadcast event sending exception: {e.Message}");
                // ignored
            }
        }

        private async Task SendBroadcastEvent(byte[] message)
        {
            try
            {
                foreach (var client in _server.ClientHandlers)
                {
                    if (!client.IsWorking)
                    {
                        _server.RemoveClient(client);
                    }
                }

                var tasks = _server.ClientHandlers.Select(c => c.WriteAsync(message));
                await Task.WhenAll(tasks);
            }
            catch (Exception e)
            {
                MessageLogger.Warning($"Broadcast event sending exception: {e.Message}");
                // ignored
            }
        }

        private void SendToClient(NetClientSlim client, byte[] message, MessageHeader header, string requestName)
        {
            lock (client)
            {
                try
                {
                    // update message informational length
                    const int informationalLengthOffset = 4;
                    SerializationExtensions.GetBytes(message.Length - MessageHeader.BinarySize, sizeof(int), message, informationalLengthOffset);

                    client.Write(message);
                    LogResponse(header.Code, requestName, (RequestResult) header.ErrorCode);
                }
                catch (Exception)
                {
                    client.Stop();
                }
            }
        }

        private void SetCorrelationType(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            try
            {
                LogRequest(header.Code);

                var request = SetCorrelationTypeRequest.Parse(data);
                TaskManager.DataProcessor.SetCorrelationType(request.CorrelationTypeNumber);
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set correlation type");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set correlation type");
            }
        }

        private void SetSynchronizationShift(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            try
            {
                LogRequest(header.Code);
                var request = SetSynchronizationShiftRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set synchronization shift");
                    return;
                }
                TaskManager.HardwareController.ReceiverManager.SetSynchronizationShift(request.Shift);
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set synchronization shift");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set synchronization shift");
            }
        }

        private async Task SetFhssRadioJamTargets(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            try
            {
                LogRequest(header.Code);
                var request = FhssJammingMessage.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set FHSS radio jam targets");
                    return;
                }
                var targets = request.Settings.Select(t => new RadioJamFhssTarget(
                        minFrequencyKhz: t.StartFrequency * 0.1f,
                        maxFrequencyKhz: t.EndFrequency * 0.1f,
                        threshold: -t.Threshold,
                        modulationCode: t.ModulationCode,
                        deviationCode: t.DeviationCode,
                        manipulationCode: t.ManipulationCode,
                        forbiddenRanges: GetForbiddenRanges(t),
                        id: t.Id))
                    .ToArray();
                RadioJamManager.FhssJamDuration = request.Duration;
                RadioJamManager.FhssFftResoultion = request.FftResolutionCode;

                await Controller.SetFhssJammingTargets(targets, request.Station, client.Id);

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set FHSS radio jam targets");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set FHSS radio jam targets");
            }

            IReadOnlyList<DspDataModel.FrequencyRange> GetForbiddenRanges(FhssJammingSetting setting)
            {
                var forbiddenRanges = setting.FixedRadioSources
                    .Select(rs => new DspDataModel.FrequencyRange(
                        rs.Frequency * 0.1f - rs.Bandwidth * 0.05f,
                        rs.Frequency * 0.1f + rs.Bandwidth * 0.05f)).ToList();
                var minFrequency = setting.StartFrequency * 0.1f;
                var difference = minFrequency - ((int) minFrequency / 1000) * 1000;
                if (difference > 0)
                {
                    forbiddenRanges.Add(
                        new DspDataModel.FrequencyRange(
                            setting.StartFrequency * 0.1f - difference, setting.StartFrequency * 0.1f
                            ));
                }
                return forbiddenRanges.ToArray();
            }
        }

        private async Task SetFrsRadioJamTargets(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            const string cmdName = "Set FRS radio jam targets";
            try
            {
                LogRequest(header.Code);
                var request = FrsJammingMessage.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
                    return;
                }

                var targets = request.Settings.Select(target => new RadioJamTarget(
                        frequencyKhz: target.Frequency * 0.1f,
                        priority: target.Priority,
                        threshold: -target.Threshold,
                        direction: target.Direction * 0.1f,
                        useAdaptiveThreshold: false,
                        modulationCode: target.ModulationCode,
                        deviationCode: target.DeviationCode,
                        manipulationCode: target.ManipulationCode,
                        durationCode: target.DurationCode,
                        id: target.Id,
                        liter: target.Liter,
                        targetConfig: RadioJamManager))
                    .ToArray();
                await Controller.SetFrsJammingTargets(targets, request.Station, client.Id);
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private static FRSJammingSetting[] GetFrsJammingTargets(IEnumerable<IRadioJamTarget> targets)
        {
            return targets.Select(target => new FRSJammingSetting(
                    Frequency: (int)(target.FrequencyKhz * 10),
                    ModulationCode: target.ModulationCode,
                    DeviationCode: target.DeviationCode,
                    ManipulationCode: target.ManipulationCode,
                    DurationCode: target.DurationCode,
                    Priority: (byte)target.Priority,
                    Threshold: (byte)-target.Threshold,
                    Direction: (short)(target.Direction * 10),
                    Id: target.Id,
                    Liter: (byte)target.Liter))
                .ToArray();
        }

        private static FhssJammingSetting[] GetFhssJammingTargets(IEnumerable<IRadioJamFhssTarget> targets)
        {
            return targets.Select(t => new FhssJammingSetting(
                StartFrequency: (int)t.MinFrequencyKhz,
                EndFrequency: (int)t.MaxFrequencyKhz,
                Threshold: (byte)t.Threshold,
                ModulationCode: t.ModulationCode,
                DeviationCode: t.DeviationCode,
                ManipulationCode: t.ManipulationCode,
                Id: t.Id,
                FixedRadioSourceCount: 0,
                FixedRadioSources: new FhssFixedRadioSource[0] 
                ))
                .ToArray();
        }

        private void GetFrsRadioJamTargets(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = GetFrsJammingRequest.Parse(data);
                var storage = request.Station == TargetStation.Current? 
                    RadioJamManager.Storage : RadioJamManager.LinkedStationStorage;
                var targets = GetFrsJammingTargets(storage.FrsTargets);
                responseHeader.InformationLength = targets.Length * FRSJammingSetting.BinarySize + 1;

                SendToClient(client, FrsJammingMessage.ToBinary(responseHeader, request.Station, targets), responseHeader, "Get FRS radio jam targets");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, FrsJammingMessage.ToBinary(responseHeader, TargetStation.Current, new FRSJammingSetting[0]), responseHeader, "Get FRS radio jam targets");
            }
        }

        private FhssJammingSetting[] GetFhssJammingSetting(IRadioJamTargetStorage storage)
        {
            return storage.FhssTargets
                .Select(target => new FhssJammingSetting(
                    StartFrequency: (int) (target.MinFrequencyKhz * 10),
                    EndFrequency: (int) (target.MaxFrequencyKhz * 10),
                    Threshold: (byte) -target.Threshold,
                    ModulationCode: target.ModulationCode,
                    DeviationCode: target.DeviationCode,
                    ManipulationCode: target.ManipulationCode,
                    Id: target.Id,
                    FixedRadioSourceCount: target.ForbiddenRanges.Count,
                    FixedRadioSources: target.ForbiddenRanges
                        .Select(t => new FhssFixedRadioSource(
                            (int) (t.CentralFrequencyKhz * 10),
                            (int) (t.RangeSizeKhz * 10)))
                        .ToArray()
                ))
                .ToArray();
        }

        private void GetFhssRadioJamTargets(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Get FHSS radio jam targets";
            try
            {
                LogRequest(header.Code);
                var request = GetFhssJammingRequest.Parse(data);
                var storage = request.Station == TargetStation.Current ?
                    RadioJamManager.Storage : RadioJamManager.LinkedStationStorage;

                var targets = GetFhssJammingSetting(storage);
                var targetsLength = targets.Sum(t => t.StructureBinarySize);
                responseHeader.InformationLength = targetsLength + 7;
                var response = FhssJammingMessage.ToBinary(responseHeader, RadioJamManager.EmitDuration, RadioJamManager.FhssFftResoultion, request.Station, (byte) targets.Length, targets);
                SendToClient(client, response, responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                responseHeader.InformationLength = 6;
                SendToClient(client, FhssJammingMessage.ToBinary(responseHeader, 0, 0, TargetStation.Current, 0, new FhssJammingSetting[0]), responseHeader, cmdName);
            }
        }

        private async Task SetAttenuatorsValues(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            try
            {
                LogRequest(header.Code);
                var request = AttenuatorsMessage.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set attenuators values");
                    return;
                }
                var tasks = new List<SetAttenuatorTask>();
                foreach (var setup in request.Settings)
                {
                    if (setup.BandNumber < Constants.UsrpFirstBandNumber)
                    {
                        var task = new SetAttenuatorTask(TaskManager, setup.BandNumber, setup.IsConstAttenuatorEnabled == 1, setup.AttenuatorValue * 0.5f);
                        tasks.Add(task);
                        TaskManager.AddTask(task);
                    }
                    else
                    {
                        TaskManager.HardwareController.UsrpReceiverManager.Amplification = (byte)(setup.AttenuatorValue);
                    }
                }

                await Task.WhenAll(tasks.Select(t => t.WaitForResult()));

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set attenuators values");

                var eventHeader = GetResponseMessageHeader(header, header.InformationLength);
                var message = AttenuatorsMessage.ToBinary(eventHeader, request.Settings);
                SendBroadcastEvent(client, message);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set attenuators values");
            }
        }

        private AttenuatorSetting[] GetAttenuatorSettings(byte[] bandNumbers)
        {
            var attenuatorSettings = new AttenuatorSetting[bandNumbers.Length];

            for (var i = 0; i < attenuatorSettings.Length; ++i)
            {
                if (bandNumbers[i] >= Constants.UsrpFirstBandNumber)
                {
                    attenuatorSettings[i] = new AttenuatorSetting(bandNumbers[i], 0, 0);
                    continue;
                }

                var bandInfo = TaskManager.HardwareController.FpgaDeviceBandsSettings[bandNumbers[i]];
                var attenuatorValue = (byte)(bandInfo.AttenuatorLevel * 2);
                var isConstAttenuatorEnabled = (byte)(bandInfo.IsConstantAttenuatorEnabled ? 1 : 0);
                attenuatorSettings[i] = new AttenuatorSetting(bandNumbers[i], attenuatorValue, isConstAttenuatorEnabled);
            }
            return attenuatorSettings;
        }

        private void GetAttenuatorsValues(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = GetAttenuatorsRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, GetAttenuatorsResponse.ToBinary(responseHeader, new AttenuatorSetting[0]), responseHeader, "Get attenuators values");
                    return;
                }
                responseHeader.InformationLength = request.BandNumbers.Length * AttenuatorSetting.BinarySize;

                var attenuatorSettings = GetAttenuatorSettings(request.BandNumbers);
                SendToClient(client, GetAttenuatorsResponse.ToBinary(responseHeader, attenuatorSettings), responseHeader, "Get attenuators values");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, GetAttenuatorsResponse.ToBinary(responseHeader, new AttenuatorSetting[0]), responseHeader, "Get attenuators values");
            }
        }

        private void GetAdaptiveThreshold(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: GetAdaptiveThresholdResponse.BinarySize - MessageHeader.BinarySize);
            try
            {
                LogRequest(header.Code);
                var request = GetAdaptiveThresholdRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, GetAdaptiveThresholdResponse.ToBinary(responseHeader, 0), responseHeader, "Get adaptive threshold");
                    return;
                }

                var threshold = TaskManager.FilterManager.GetAdaptiveThreshold(request.BandNumber);
                var byteThreshold = (byte) (-threshold);
                SendToClient(client, GetAdaptiveThresholdResponse.ToBinary(responseHeader, byteThreshold), responseHeader, "Get adaptive threshold");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, GetAdaptiveThresholdResponse.ToBinary(responseHeader, 0), responseHeader, "Get adaptive threshold");
            }
        }

        private void GetBandsAdaptiveThreshold(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header: header, length: header.InformationLength);
            try
            {
                LogRequest(header.Code);
                var request = GetBandsAdaptiveThresholdRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    responseHeader.InformationLength = 1;
                    SendToClient(client, GetBandsAdaptiveThresholdResponse.ToBinary(responseHeader, new byte[] { 0 }), responseHeader, "Get bands adaptive threshold");
                    return;
                }

                var thresholds = new byte[request.Bands.Length];
                for (int i = 0; i < thresholds.Length; i++)
                {
                    var threshold = TaskManager.FilterManager.GetAdaptiveThreshold(request.Bands[i]);
                    thresholds[i] = (byte)(-threshold);
                }

                SendToClient(client, GetBandsAdaptiveThresholdResponse.ToBinary(responseHeader, thresholds), responseHeader, "Get bands adaptive threshold");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                responseHeader.InformationLength = 1;
                SendToClient(client, GetBandsAdaptiveThresholdResponse.ToBinary(responseHeader, new byte[] { 0 }), responseHeader, "Get bands adaptive threshold");
            }
        }

        private void SetVoiceJammingSettings(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header: header, length: header.InformationLength);
            try
            {
                LogRequest(header.Code);
                var request = SetVoiceJammingSettingsRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set voice jamming settings");
                    return;
                }

                RadioJamManager.VoiceMode = request.VoiceJammingMode;
                RadioJamManager.VoiceJamFrequencyKhz = request.FrequencyKhz;
                RadioJamManager.VoiceJamDeviationCode = request.DeviationCode;
                RadioJamManager.VoiceJamFilePath = request.FilePath;
                
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, $"Set voice jamming settings : {request.FrequencyKhz} KHz," +
                                                                                              $" {request.VoiceJammingMode}, {request.FilePath}, dev {request.DeviationCode}");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set voice jamming settings");
            }
        }

        private void SetVoiceJamming(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header: header, length: header.InformationLength);
            try
            {
                LogRequest(header.Code);
                var request = SetVoiceJammingRequest.Parse(data);
                RadioJamManager.ChangeVoiceJamState(request.IsJammingEnabled);
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, $"Set voice jamming : {request.IsJammingEnabled}");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set voice jamming");
            }
        }

        private async Task SendMasterSlaveStateChangedEvent(StationRole role, ConnectionState state)
        {
            var header = new MessageHeader(0, 0, Code: 56, ErrorCode: 0, InformationLength: MasterSlaveStateChangedEvent.BinarySize - MessageHeader.BinarySize);
            var message = MasterSlaveStateChangedEvent.ToBinary(header, role, state);
            await SendBroadcastEvent(message);
        }

        public async Task SendFrsRadioJamStateEvent(IReadOnlyList<IRadioJamTarget> targets)
        {
        }

        public async Task SendFhssRadioJamStateEvent(IReadOnlyList<(int id, bool isJammed)> jammedFhssNetworks, IReadOnlyList<float> frequencies)
        {
            var header = new MessageHeader(0, 0, Code: 46,
                ErrorCode: 0, InformationLength: 5 + jammedFhssNetworks.Count * FhssNetworkJammingState.BinarySize + frequencies.Count * 4);
            var message = FhssRadioJamUpdateEvent.ToBinary(header,
                FhssNetworkCount: (byte) jammedFhssNetworks.Count,
                JammingStates: jammedFhssNetworks.Select(b => new FhssNetworkJammingState(b.isJammed, b.id)).ToArray(),
                JammedFrequenciesCount: frequencies.Count,
                Frequencies: frequencies.Select(f => (int) (f * 10)).ToArray()
            );

            await SendBroadcastEvent(message);
        }

        public async Task SendShaperStateEvent(bool isConnected)
        {
            var header = new MessageHeader(0, 0, Code: 45,
                ErrorCode: 0, InformationLength: ShaperStateUpdateEvent.BinarySize - MessageHeader.BinarySize);
            var message = ShaperStateUpdateEvent.ToBinary(header, isConnected);
            await SendBroadcastEvent(message);
        }

        private void SetFrsRadioJamSettings(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            const string cmdName = "Set FRS radio jam settings";
            try
            {
                LogRequest(header.Code);
                var request = SetFrsRadioJamSettingsRequest.Parse(data);
                if (!request.IsValid() || request.EmitionDuration < RadioJamManager.CheckEmitDelayMs)
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
                    return;
                }
                RadioJamManager.LongWorkingSignalDurationMs = request.LongWorkingSignalDuration;
                RadioJamManager.EmitDuration = request.EmitionDuration;
                RadioJamManager.ChannelsInLiter = request.ChannelsInLiter;

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private void SetAfrsRadioJamSettings(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            const string cmdName = "Set AFRS radio jam settings";
            try
            {
                LogRequest(header.Code);
                var request = SetAfrsRadioJamSettingsRequest.Parse(data);
                if (!request.IsValid() || request.EmitionDuration < RadioJamManager.CheckEmitDelayMs)
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
                    return;
                }
                RadioJamManager.LongWorkingSignalDurationMs = request.LongWorkingSignalDuration;
                RadioJamManager.EmitDuration = request.EmitionDuration;
                RadioJamManager.ChannelsInLiter = request.ChannelsInLiter;
                RadioJamManager.Threshold = -request.Threshold;
                RadioJamManager.DirectionSearchSector = request.DirectionSearchSector / 10;
                RadioJamManager.FrequencySearchBandwidthKhz = request.SearchBandwidth / 10;

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private void SetFrsAutoRadioJamSettings(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            const string cmdName = "Set FRS Auto radio jam settings";
            try
            {
                LogRequest(header.Code);
                var request = SetFrsAutoRadioJamSettingsRequest.Parse(data);
                if (!request.IsValid() || request.EmitionDuration < RadioJamManager.CheckEmitDelayMs)
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
                    return;
                }
                RadioJamManager.LongWorkingSignalDurationMs = request.LongWorkingSignalDuration;
                RadioJamManager.EmitDuration = request.EmitionDuration;
                RadioJamManager.ChannelsInLiter = request.ChannelsInLiter;
                RadioJamManager.Threshold = -request.Threshold;
                RadioJamManager.MinSignalBandwidthKhz = request.SignalBandwidthThreshold * 0.1f;

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }
        
        private void PerformStorageAction(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            const string cmdName = "Perform storage action";
            try
            {
                var request = StorageActionMessage.Parse(data);
                LogRequest(header.Code);
                var isStorageChanged = Controller
                    .PerformStorageAction(request.Storage, request.Action, request.SignalsId);

                if (isStorageChanged)
                {
                    var eventHeader = GetResponseMessageHeader(header, header.InformationLength);
                    var message = StorageActionMessage.ToBinary(eventHeader, request.Storage, request.Action, request.SignalsId);
                    SendBroadcastEvent(client, message);
                }
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private async Task GetBandAmplitudeLevels(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = GetBandAmplitudeLevelsRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, GetBandAmplitudeLevelsResponse.ToBinary(responseHeader, new BandAmplitudeLevel[0]), responseHeader, "Get band amplitude levels");
                    return;
                }
                responseHeader.InformationLength = request.BandNumbers.Length * BandAmplitudeLevel.BinarySize;
                var bandLevels = request.BandNumbers
                    .Select(b => new BandAmplitudeLevel(b, (short)TaskManager.FilterManager.NoiseLevels[b]))
                    .ToArray();

                SendToClient(client, GetBandAmplitudeLevelsResponse.ToBinary(responseHeader, bandLevels), responseHeader, "Get band amplitude levels");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, GetBandAmplitudeLevelsResponse.ToBinary(responseHeader, new BandAmplitudeLevel[0]), responseHeader, "Get band amplitude levels");
            }
        }

        private void GetAmplifiersValues(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = GetAmplifiersRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, GetAmplifiersResponse.ToBinary(responseHeader, new byte[0]), responseHeader, "Get amplifiers values");
                    return;
                }
                var amplifiers = new byte[request.BandNumbers.Length];
                responseHeader.InformationLength = request.BandNumbers.Length;

                for (var i = 0; i < amplifiers.Length; ++i)
                {
                    if (request.BandNumbers[i] >= Constants.UsrpFirstBandNumber)
                    {
                        amplifiers[i] = (byte)(TaskManager.HardwareController.UsrpReceiverManager.Amplification * 2);
                        continue;
                    }

                    var bandInfo = TaskManager.HardwareController.FpgaDeviceBandsSettings[request.BandNumbers[i]];
                    var level = (byte) (bandInfo.AmplifierLevel * 2);
                    amplifiers[i] = level;
                }
                SendToClient(client, GetAmplifiersResponse.ToBinary(responseHeader, amplifiers), responseHeader, "Get amplifiers values");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, GetAmplifiersResponse.ToBinary(responseHeader, new byte[0]), responseHeader, "Get amplifiers values");
            }
        }

        private void StartRadioRecord(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            try
            {
                LogRequest(header.Code);

                Recorder.StartRecording();

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Start radio recording");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Start radio recording");
            }
        }

        private void GetScanSpeed(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            const string cmdName = "Get scan speed";
            try
            {
                LogRequest(header.Code);
                SendToClient(client, GetScanSpeedResponse.ToBinary(
                    Header: responseHeader, 
                    FpgaScanSpeed: TaskManager.ScanSpeed, 
                    UsrpScanSpeed: TaskManager.HardwareController.UsrpReceiverManager.ScanSpeed), 
                    responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, GetScanSpeedResponse.ToBinary(responseHeader, 0, 0), responseHeader, cmdName);
            }
        }

        private async Task GetRadioControlSpectrum(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: GetRadioControlSpectrumResponse.BinarySize - MessageHeader.BinarySize);
            const string cmdName = "Get radio control spectrum";
            try
            {
                LogRequest(header.Code);
                var request = GetRadioControlSpectrumRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, GetRadioControlSpectrumResponse.ToBinary(responseHeader, new byte[Constants.BandSampleCount]), responseHeader, cmdName);
                    return;
                }
                var taskSetup = new SpectrumTaskSetup
                {
                    AveragingCount = 1,
                    IsEndless = false,
                    Objectives = new[] {new Band(request.BandNumber)},
                    TaskManager = this.TaskManager,
                    ReceiversIndexes = new[] {5}, // only radio control receiver works here
                    Priority = 1 // increased priority
                };
                var task = new SpectrumTask(taskSetup);
                TaskManager.AddTask(task);
                await task.WaitForResult();

                var amplitudes = task.TypedResult.Amplitudes
                    .Select(a => (byte) (a > 0 ? 0 : -a))
                    .ToArray();

                SendToClient(client, GetRadioControlSpectrumResponse.ToBinary(responseHeader, amplitudes), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, GetRadioControlSpectrumResponse.ToBinary(responseHeader, new byte[Constants.BandSampleCount]), responseHeader, cmdName);
            }
        }

        private void CalculateCalibrationCorrection(MessageHeader header, byte[] data, NetClientSlim client)
        {
            const string cmdName = "Calculate calibration correction";

            var responseHeader = GetResponseMessageHeader(header, length: 0);
            try
            {
                LogRequest(header.Code);

                var request = CalculateCalibrationCorrectionRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
                    return;
                }
                var signals = request.Signals
                    .Select(s => new Correlator.CalibrationCorrection.CalibrationCorrectionSignal(s.Frequency / 10, s.Direction,
                        s.Phases.Select(p => p * 0.1f).ToArray()))
                    .ToArray();

                var table = CalibrationCorrectionGenerator.GenerateTable(signals);
                var filename = _config.CalibrationSettings.CalibrationCorrectionFilename.IsNullOrEmpty()
                    ? "correlationCorrectionDefault.bin"
                    : _config.CalibrationSettings.CalibrationCorrectionFilename;
                table.Save(filename);

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private void StopRadioRecord(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: StopRecordingResponse.BinarySize - MessageHeader.BinarySize);
            try
            {
                LogRequest(header.Code);
                Recorder.StopRecording();
                SendToClient(client, StopRecordingResponse.ToBinary(responseHeader, (short) Recorder.CurrentRecordId), responseHeader, "Stop radio recording");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, StopRecordingResponse.ToBinary(responseHeader, (short) Recorder.CurrentRecordId), responseHeader, "Stop radio recording");
            }
        }

        private void GetAmplitudeTimeSpectrum(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);

                var request = GetAmplitudeTimeSpectrumRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, GetAmplitudeTimeSpectrumResponse.ToBinary(responseHeader, new byte[0]), responseHeader, "Get time spectrum");
                    return;
                }

                var startFrequency = request.StartFrequency / 10f;
                var endFrequency = request.EndFrequency / 10f;
                var timeSpan = TimeSpan.FromSeconds(request.TimeLength);

                var spectrumHistory = TaskManager.SpectrumHistoryStorage
                    .GetSpectrumHistory(startFrequency, endFrequency, request.PointCount, timeSpan);

                responseHeader.InformationLength = spectrumHistory.Bands.Count * spectrumHistory.Bands[0].Count;
                var spectrum = new byte[responseHeader.InformationLength];
                var i = 0;
                foreach (var band in spectrumHistory.Bands)
                {
                    for (var j = 0; j < band.Count; j++)
                    {
                        spectrum[i++] = (byte) (band.Amplitudes[j] - Constants.ReceiverMinAmplitude);
                    }
                }
                SendToClient(client, GetAmplitudeTimeSpectrumResponse.ToBinary(responseHeader, spectrum), responseHeader, "Get time spectrum");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, GetAmplitudeTimeSpectrumResponse.ToBinary(responseHeader, new byte[0]), responseHeader, "Get time spectrum");
            }
        }

        private void GetCalibrationProgress(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                if (TaskManager.CurrentMode is CalibrationMode currentMode)
                {
                    var progress = (byte) (currentMode.Progress * byte.MaxValue);
                    if (Math.Abs(currentMode.Progress - 1) < 1e-3f)
                    {
                        progress = byte.MaxValue;
                    }
                    else if (progress == byte.MaxValue)
                    {
                        progress = byte.MaxValue - 1;
                    }
                    var phaseDeviations = currentMode.PhaseDeviations
                        .Select(pd => (byte) pd)
                        .ToArray();
                    responseHeader.InformationLength = 1 + phaseDeviations.Length;
                    SendToClient(client, GetCalibrationProgressResponse.ToBinary(responseHeader, progress, phaseDeviations), responseHeader, "Get calibration progress");
                }
                else
                {
                    responseHeader.InformationLength = 1;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidServerMode;
                    SendToClient(client, GetCalibrationProgressResponse.ToBinary(responseHeader, 0, new byte[0]), responseHeader, "Get calibration progress");
                }
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 1;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, GetCalibrationProgressResponse.ToBinary(responseHeader, 0, new byte[0]), responseHeader, "Get calibration progress");
            }
        }

        private void SetReceiversChannel(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Set receivers channel ";
            try
            {
                var request = SetReceiversChannelRequest.Parse(data);
                var cmd = cmdName + request.Channel;

                LogRequest(header.Code);

                TaskManager.HardwareController.ReceiverManager.SetReceiversChannel(request.Channel);

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmd);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte) RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private void SetDirectionCorrection(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Set direction correction ";
            try
            {
                var request = DirectionCorrectionMessage.Parse(data);
                var correction = PhaseMath.NormalizePhase(request.DirectionCorrection * 0.1f);
                
                var cmd = cmdName + correction;
                
                LogRequest(header.Code);
                var isChanged = Math.Abs(correction - _config.DirectionFindingSettings.DirectionCorrection) > 1;
                if (isChanged)
                {
                    if (request.UseCorrection)
                    {
                        _config.DirectionFindingSettings.DirectionCorrection = correction;
                    }

                    var eventHeader = GetResponseMessageHeader(header, header.InformationLength);
                    var message = DirectionCorrectionMessage
                        .ToBinary(eventHeader, request.DirectionCorrection, request.UseCorrection);

                    SendBroadcastEvent(client, message);
                }
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmd);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private void GetDirectionCorrection(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, DirectionCorrectionMessage.BinarySize - MessageHeader.BinarySize);
            const string cmdName = "Get direction correction";
            try
            {
                LogRequest(header.Code);
                var direction = _config.DirectionFindingSettings.DirectionCorrection;

                // send -1 if direction correction is not set
                var sendDirection = (short) (direction == -1 ? -1 : direction * 10);
                var response = DirectionCorrectionMessage.ToBinary(responseHeader, sendDirection, true);
                SendToClient(client, response, responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte) RequestResult.ResponseParseError;
                SendToClient(client, DirectionCorrectionMessage.ToBinary(responseHeader, -1, true), responseHeader, cmdName);
            }
        }

        private void GetBearingPanoramaSignals(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Get bearing panorama signals";
            try
            {
                LogRequest(header.Code);
                var request = GetBearingPanoramaSignalsRequest.Parse(data);
                if (!request.IsValid())
                {
                    var message = GetErrorResponse(RequestResult.InvalidRequest);
                    SendToClient(client, message, responseHeader, cmdName);
                }

                var range = new DspDataModel.FrequencyRange(request.StartFrequency * 0.1f, request.EndFrequency * 0.1f);
                var now = DateTime.Now;
                var timeThreshold = Config.Instance.DirectionFindingSettings.BearingPanoramaTimeThreshold;

                var fixedSignals = TaskManager.RadioSourceStorage.GetRadioSources()
                    .Where(r => range.Contains(r.CentralFrequencyKhz) &&
                                now - r.LastActiveTime() < timeThreshold &&
                                r.IsDirectionReliable())
                    .Select(GetBearingPanoramaSignal)
                    .ToArray();

                var impulseSignals = TaskManager.SignalStorage.GetSignals()
                    .Where(s => range.Contains(s.Signal.CentralFrequencyKhz) &&
                                now - s.Time < timeThreshold &&
                                s.Signal.IsDirectionReliable())
                    .Select(s => GetBearingPanoramaSignal(s.Signal))
                    .ToArray();

                responseHeader.InformationLength = 8 + (fixedSignals.Length + impulseSignals.Length) * BearingPanoramaSignal.BinarySize;

                var response = GetBearingPanoramaSignalsResponse.ToBinary(responseHeader, 
                    impulseSignals.Length, impulseSignals, 
                    fixedSignals.Length, fixedSignals);

                SendToClient(client, response, responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                var message = GetErrorResponse(RequestResult.ResponseParseError);
                SendToClient(client, message, responseHeader, cmdName);
            }

            BearingPanoramaSignal GetBearingPanoramaSignal(ISignal signal)
            {
                return new BearingPanoramaSignal(
                    (int) (signal.CentralFrequencyKhz * 10),
                    (short) (signal.Direction * 10));
            }

            byte[] GetErrorResponse(RequestResult result)
            {
                responseHeader.InformationLength = 8;
                responseHeader.ErrorCode = (byte) result;

                return GetBearingPanoramaSignalsResponse.ToBinary(responseHeader, 
                    0, new BearingPanoramaSignal[0], 
                    0, new BearingPanoramaSignal[0]);
            }
        }

        private void GetSpectrum(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 1);
            try
            {
                LogRequest(header.Code);
                var request = GetSpectrumRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte) RequestResult.InvalidRequest;
                    SendToClient(client, GetSpectrumResponse.ToBinary(responseHeader, 0, new byte[] { }), responseHeader, "Get spectrum");
                    return;
                }
                var startFrequencyKhz = request.StartFrequency / 10;
                var endFrequencyKhz = request.EndFrequency / 10;

                var amplitudes = SpectrumStorage.GetSpectrum(startFrequencyKhz, endFrequencyKhz, request.PointCount).Amplitudes;
                var spectrum = FilterSpectrum(amplitudes, startFrequencyKhz, endFrequencyKhz, request.PointCount);

                responseHeader.InformationLength = spectrum.Length + 1;
                SendToClient(client, GetSpectrumResponse.ToBinary(responseHeader, 0, spectrum), responseHeader, "Get spectrum");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte) RequestResult.ResponseParseError;
                SendToClient(client, GetSpectrumResponse.ToBinary(responseHeader, 0, new byte[] { }), responseHeader, "Get spectrum");
            }

            byte[] FilterSpectrum(float[] amplitudes, int startFrequencyKhz, int endFrequencyKhz, int pointCount)
            {
                var spectrum = new byte[amplitudes.Length];

                int GetIndex(int frequency) => (int)(1f * (frequency - startFrequencyKhz) /
                                                     (endFrequencyKhz - startFrequencyKhz) * pointCount);

                foreach (var filter in TaskManager.FilterManager.Filters)
                {
                    var startIndex = GetIndex(filter.MinFrequencyKhz).ToBounds(0, spectrum.Length);
                    var endIndex = GetIndex(filter.MaxFrequencyKhz).ToBounds(0, spectrum.Length);
                    for (var i = startIndex; i < endIndex; ++i)
                    {
                        spectrum[i] = (byte) (amplitudes[i] - Constants.ReceiverMinAmplitude);
                    }
                }
                return spectrum;
            }
        }

        private static MessageHeader GetResponseMessageHeader(MessageHeader header, int length = 0)
        {
            return new MessageHeader(
                SenderAddress: header.ReceiverAddress,
                ReceiverAddress: header.SenderAddress,
                Code: header.Code,
                ErrorCode: (byte) RequestResult.Ok,
                InformationLength: length
            );
        }

        private static short GetProtocolDirection(float reliability, float direction)
        {
            return (short) (reliability > Constants.ReliabilityThreshold ? direction * 10 : -1);
        }

        private static short GetProtocolDirection(ISignal signal)
        {
            return GetProtocolDirection(signal.Reliability, signal.Direction);
        }

        private static RadioSource ToRadioSourceDO(IRadioSource radioSource)
        {
            // we are using frequency with max amplitude for narrow signal and central frequency for the broadband one
            // const value is suggested by Vadim Leopardovich
            const float bandwidthThresholdKhz = 12;

            var frequency = radioSource.BandwidthKhz < bandwidthThresholdKhz 
                ? radioSource.FrequencyKhz 
                : radioSource.CentralFrequencyKhz;
            var direction = GetProtocolDirection(radioSource);
            //var direction2 = GetProtocolDirection(radioSource.LinkedReliability, radioSource.LinkedDirection);

            return new RadioSource(
                Id: radioSource.Id,
                IsNew: radioSource.IsNew,
                IsActive: radioSource.IsActive,
                Frequency: (int) (frequency * 10),
                Direction: (short) direction,
                Latitude: radioSource.Latitude,
                Longitude: radioSource.Longitude,
                Altitude: radioSource.Altitude,
                StandardDeviation: (short) (radioSource.StandardDeviation * 10),
                Bandwidth: (int) (radioSource.BandwidthKhz * 10),
                Time: new DetectionTime(
                    Hour: (byte) radioSource.BroadcastStartTime.Hour,
                    Minute: (byte) radioSource.BroadcastStartTime.Minute,
                    Second: (byte) radioSource.BroadcastStartTime.Second,
                    Millisecond: (short) radioSource.BroadcastStartTime.Millisecond),
                Modulation: (byte) radioSource.Modulation,
                Amplitude: (byte) -radioSource.Amplitude,
                Duration: (int) radioSource.BroadcastTimeSpan.TotalMilliseconds,
                BroadcastCount: 0
            );
        }

        private void GetRadioSources(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var filterManager = TaskManager.FilterManager;
                var radioSources = RadioSourceStorage.GetRadioSources()
                    .Where(rs => 
                    {
                        var bandNumber = Utilities.GetBandNumber(rs.FrequencyKhz);
                        if (filterManager.GetRadioSourceType(rs) == RadioSourceType.Forbidden)
                        {
                            return false;
                        }
                        return rs.Amplitude > filterManager.GetThreshold(bandNumber);
                    })
                    .ToArray();

                var radioSourcesDo = radioSources.Select(ToRadioSourceDO).ToArray();

                Array.Sort(radioSourcesDo, (r1, r2) => r1.Frequency.CompareTo(r2.Frequency));
                responseHeader.InformationLength = radioSources.Length * RadioSource.BinarySize;

                SendToClient(client, GetRadioSourcesResponse.ToBinary(responseHeader, radioSourcesDo), responseHeader, "Get radio sources");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                SendToClient(client, GetRadioSourcesResponse.ToBinary(responseHeader, new RadioSource[] {}), responseHeader, "Get radio sources");
            }
        }

        private void GetFhssNetworks(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);

                var networks = TaskManager.FhssNetworkStorage.GetFhssNetworks().Select(network => new FhssRadioNetwork(
                        StartFrequency: (int) (network.MinFrequencyKhz * 10),
                        EndFrequency: (int) (network.MaxFrequencyKhz * 10),
                        Bandwidth: (int) (network.BandwidthKhz * 10),
                        Step: (int) (network.StepKhz * 10),
                        Amplitude: (byte) (-network.Amplitude),
                        Id: network.Id,
                        Time: new DetectionTime(
                            (byte) network.LastUpdateTime.Hour,
                            (byte) network.LastUpdateTime.Minute,
                            (byte) network.LastUpdateTime.Second,
                            (short) network.LastUpdateTime.Millisecond),
                        IsActive: network.IsActive ? 1 : 0,
                        LocationsCount: network.Directions.Count,
                        Locations: GetLocations(network),
                        FixedRadioSourceCount: network.FixedRadioSources.Count,
                        FixedRadioSources: GetFixedRadioSources(network),
                        ImpulseDuration: (int)network.ImpulseDurationMs))
                    .ToArray();

                responseHeader.InformationLength = 4 + networks.Sum(n => n.StructureBinarySize);
                SendToClient(client, GetFhssNetworksResponse.ToBinary(responseHeader, networks.Length, networks), responseHeader, "Get fhss networks");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 4;
                SendToClient(client, GetFhssNetworksResponse.ToBinary(responseHeader, 0, new FhssRadioNetwork[0]), responseHeader, "Get radio sources");
            }

            FhssFixedRadioSource[] GetFixedRadioSources(IFhssNetwork network)
            {
                return network.FixedRadioSources
                    .Select(frs => new FhssFixedRadioSource((int) (frs.FrequencyKhz * 10), (int) (frs.Bandwidth * 10)))
                    .ToArray();
            }

            RadioSourceLocation[] GetLocations(IFhssNetwork network)
            {
                return network.Directions
                    .Select(d => new RadioSourceLocation((short) (d * 10), -1, -1, -1))
                    .ToArray();
            }
        }

        private void GetSearchFhss(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, SearchFhssMessage.BinarySize - MessageHeader.BinarySize);
            const string cmdName = "Get Fhss search";
            try
            {
                LogRequest(header.Code);
                var searchConfig = _config.FhssSearchSettings;
                
                var response = SearchFhssMessage.ToBinary(responseHeader, searchConfig.SearchFhss);

                SendToClient(client, response, responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, SearchFhssMessage.ToBinary(responseHeader, byte.MaxValue), responseHeader, cmdName);
            }
        }

        private void SetSearchFhss(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Set Fhss search";
            try
            {
                LogRequest(header.Code);
                var request = SearchFhssMessage.Parse(data);
                var searchConfig = _config.FhssSearchSettings;
                
                var isSearchFhssChanged = searchConfig.SearchFhss != request.SearchFhss;

                if (isSearchFhssChanged)
                {
                    searchConfig.SearchFhss = request.SearchFhss;
                    var eventHeader = GetResponseMessageHeader(header, header.InformationLength);
                    var message = SearchFhssMessage.ToBinary(eventHeader, request.SearchFhss);
                    SendBroadcastEvent(client, message);
                }

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private void SetAntennaDirections(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Set Antenna Directions";
            try
            {
                LogRequest(header.Code);
                var request = AntennaDirectionsMessage.Parse(data);
                if (request.IsValid() == false)
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
                    return;
                }

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
                responseHeader.InformationLength = request.Header.InformationLength;
                SendBroadcastEvent(client, AntennaDirectionsMessage.ToBinary(responseHeader, request.antennaDirections));
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, AntennaDirectionsMessage.ToBinary(responseHeader, new AntennaDirections()), responseHeader, cmdName);
            }
        }

        private void RemeasureFhssNetworksDuration(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Remeasure fhss network duration";
            try
            {
                LogRequest(header.Code);
                var request = GetTemperatureResponse.Parse(data);
                var target = TaskManager.FhssNetworkStorage.GetFhssNetworks().FirstOrDefault(t => t.Id == request.Temperature);

                target?.RemeasureDuration();
                
                if(target != null)
                    Controller.SetMode(DspServerMode.FhssDurationMeasuring);
                else
                    MessageLogger.Log($"There are no target with id = {request.Temperature}");
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private async Task PerformExecutiveDfRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, ExecutiveDFResponse.BinarySize - MessageHeader.BinarySize);
            const string cmdName = "Executive df request";
            try
            {
                var request = ExecutiveDFRequest.Parse(data);
                LogRequest(header.Code);

                if (!request.IsValid())
                {
                    SendErrorResponse(RequestResult.InvalidRequest);
                    return;
                }
                if (RadioJamManager.IsJammingActive)
                {
                    SendErrorResponse(RequestResult.InvalidServerMode);
                    return;
                }
                var signal = await GetSignal(request).ConfigureAwait(false);
                var response = GetExecutiveDfResponse(signal, responseHeader);

                SendToClient(client, response.GetBytes(), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                SendErrorResponse(RequestResult.ResponseParseError);
                throw;
            }

            void SendErrorResponse(RequestResult result)
            {
                responseHeader.ErrorCode = (byte) result;
                SendToClient(client, ExecutiveDFResponse.ToBinary(responseHeader, 0, 0, 0, 0, new byte[360]), responseHeader, cmdName);
            }
        }

        private async Task PerformQuasiSimultaneousDfRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, QuasiSimultaneouslyDFResponse.NullValueBinarySize - MessageHeader.BinarySize);
            responseHeader.InformationLength = 0;
            const string cmdName = "Quasi simultaneous df request";
            try
            {
                var request = QuasiSimultaneouslyDFRequest.Parse(data);
                LogRequest(header.Code);

                if (!request.IsValid())
                {
                    SendErrorResponse(RequestResult.InvalidRequest);
                    return;
                }

                if (RadioJamManager.IsJammingActive)
                {
                    SendErrorResponse(RequestResult.InvalidServerMode);
                    return;
                }

                var signalTask = GetSignal(request).ConfigureAwait(false);
                
                var executiveDfResults = Controller.DataTransferController.PerformExecutiveDf(
                    Config.Instance.StationsSettings.LinkedPositions.Select(s => s.StationId).ToList(), 
                    request.StartFrequency * 0.1f, request.EndFrequency * 0.1f,
                    request.PhaseAveragingCount, request.DirectionAveragingCount);

                var signal = await signalTask;
                if (signal == null)
                {
                    SendErrorResponse(RequestResult.ResponseParseError);
                    return;
                }

                var radioSource = RadioSourceStorage.FindSameRadioSource(signal);
                if (radioSource == null)
                {
                    radioSource = new DataStorages.RadioSource(signal);
                    RadioSourceStorage.Put(new[] {radioSource});
                }

                foreach (var stationResult in executiveDfResults)
                {
                    if(stationResult.Value.HasValue == false || stationResult.Value == -1)
                        continue;

                    radioSource.UpdateLinkedStationDirection(
                        stationId: stationResult.Key, 
                        direction: stationResult.Value.Value, 
                        reliability: 1);
                }

                //update at database
                var sources = TaskManager.RadioSourceStorage.GetRadioSources();
                if (sources.Count() != 0 && executiveDfResults.Count != 0)
                {
                    TaskManager.DatabaseController.RadioSourceTableAddRange(sources);
                }

                responseHeader.InformationLength =
                    RadioSource.BinarySize + radioSource.LinkedInfo.Count * LinkedStationResult.BinarySize;

                SendToClient(
                    client: client, 
                    message: QuasiSimultaneouslyDFResponse.ToBinary(
                        Header: responseHeader, 
                        Source: ToRadioSourceDO(radioSource), 
                        LinkedStationResults: radioSource.LinkedInfo.Select(i => new LinkedStationResult(
                            StationId: (short)i.Key, 
                            Direction: (short)(i.Value.direction)))
                            .ToArray()), 
                    header: responseHeader, 
                    requestName: cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                SendErrorResponse(RequestResult.ResponseParseError);
                throw;
            }

            void SendErrorResponse(RequestResult result)
            {
                responseHeader.ErrorCode = (byte)result;
                var radioSource = new RadioSource();
                SendToClient(client, QuasiSimultaneouslyDFResponse.ToBinary(responseHeader, radioSource, new LinkedStationResult[0]), header, cmdName);
            }
        }

        private async Task GetHeterodyneRadioSources(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Get heterodyne radio sources";
            try
            {
                var request = HeterodyneRadioSourcesRequest.Parse(data);
                LogRequest(header.Code);

                if (!request.IsValid())
                {
                    SendErrorResponse(RequestResult.InvalidRequest);
                    return;
                }
                if (RadioJamManager.IsJammingActive)
                {
                    SendErrorResponse(RequestResult.InvalidServerMode);
                    return;
                }
                // to remove int step, request.StepMHz should be divided by 10. range 0 - 25 MHz (0 - 250 value)
                float stepMHz = (float)request.StepMhz / 10;
                var frequencyRanges = CreateFrequencyRanges(request.StartFrequency / 10, request.EndFrequency / 10, stepMHz);

                var signals = await Controller.GetSignals(frequencyRanges, request.PhaseAveragingCount, request.DirectionAveragingCount, true)
                                  .ConfigureAwait(false) ?? new ISignal[0];
                signals = FilterHeterodyneRadioSources(signals, request.StartFrequency / 10, request.EndFrequency / 10, stepMHz);
                var response = CreateHeterodyneRadioSourcesResponse(signals, responseHeader);

                SendToClient(client, response.GetBytes(), responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                SendErrorResponse(RequestResult.ResponseParseError);
                throw;
            }

            void SendErrorResponse(RequestResult result)
            {
                responseHeader.ErrorCode = (byte)result;
                responseHeader.InformationLength = 0;
                SendToClient(client, HeterodyneRadioSourcesResponse.ToBinary(responseHeader, new HeterodyneRadioSouce[0]),
                    responseHeader, "Get heterodyne radio sources");
            }

            List<DspDataModel.FrequencyRange> CreateFrequencyRanges(int startFrequencyKhz, int endFrequencyKhz, float stepMhz)
            {
                const int frequencyDeviationKhz = 50;

                var result = new List<DspDataModel.FrequencyRange>();
                var frequencyKhz = startFrequencyKhz;
                var bandCount = Config.Instance.BandSettings.BandCount;

                for (var i = 0; frequencyKhz <= endFrequencyKhz; ++i)
                {
                    frequencyKhz = startFrequencyKhz + (int)(stepMhz * 1000) * i;
                    if (frequencyKhz > endFrequencyKhz)
                    {
                        break;
                    }

                    var bandSelectionPolicy = i == 0
                        ? BandBorderSelectionPolicy.TakeRight
                        : BandBorderSelectionPolicy.TakeLeft;

                    var bandNumber = Utilities.GetBandNumber(frequencyKhz, bandSelectionPolicy, bandCount);
                    var bandMinFrequency = Utilities.GetBandMinFrequencyKhz(bandNumber);
                    var bandMaxFrequency = Utilities.GetBandMaxFrequencyKhz(bandNumber);

                    // checking if the frequency is lying on the band border
                    if (frequencyKhz - bandMinFrequency < frequencyDeviationKhz)
                    {
                        frequencyKhz = bandMinFrequency + frequencyDeviationKhz;
                    }
                    else if (bandMaxFrequency - frequencyKhz < frequencyDeviationKhz)
                    {
                        frequencyKhz = bandMaxFrequency - frequencyDeviationKhz;
                    }

                    var startFrequency = frequencyKhz - frequencyDeviationKhz;
                    var endFrequency = frequencyKhz + frequencyDeviationKhz;

                    if (startFrequency < Constants.FirstBandMinKhz)
                    {
                        startFrequency = Constants.FirstBandMinKhz;
                    }
                    if (endFrequency > _config.BandSettings.LastBandMaxKhz)
                    {
                        endFrequency = _config.BandSettings.LastBandMaxKhz;
                    }
                    result.Add(new DspDataModel.FrequencyRange(startFrequency, endFrequency));
                }

                return result;
            }
        }

        private static IReadOnlyList<ISignal> FilterHeterodyneRadioSources(IReadOnlyList<ISignal> signals, int startFrequencyKhz, int endFrequencyKhz, float stepMhz)
        {
            const int frequencyDeviationKhz = 50;

            Signal CreateEmptySignal(float frequency) => new Signal(
                frequencyKhz: frequency,
                centralFrequencyKhz: frequency,
                direction: 0,
                reliability: 0,
                bandwidthKhz: 1,
                amplitude: Constants.ReceiverMinAmplitude,
                standardDeviation: 180,
                discardedDirectionsPart: 1,
                phases: new float[10],
                relativeSubScanCount: 1,
                modulation: SignalModulation.Unknown,
                phaseDeviation: 0,
                broadcastTimeSpan: TimeSpan.Zero
            );

            var signalsList = signals
                .Where(s => s.BandwidthKhz <= 100)
                .ToList();
            var result = new List<ISignal>();
            for (var frequencyKhz = startFrequencyKhz; frequencyKhz <= endFrequencyKhz; frequencyKhz += (int)(stepMhz * 1000))
            {
                var fr = frequencyKhz;
                var range = new DspDataModel.FrequencyRange(fr - frequencyDeviationKhz, fr + frequencyDeviationKhz);
                var signalsAtFrequency = signalsList
                    .Where(s => range.Contains(s.FrequencyKhz));
                ISignal signal = null;
                if (signalsAtFrequency.Count() != 0)
                    signal = signalsAtFrequency.MaxArg(s => s.Amplitude);
                result.Add(signal ?? CreateEmptySignal(frequencyKhz));
                if (signal != null)
                {
                    signalsList.Remove(signal);
                }
            }

            return result;
        }

        private async Task<ISignal> GetSignal(ExecutiveDFRequest request)
        {
            return await Controller.GetSignal(
                request.StartFrequency * 0.1f,
                request.EndFrequency * 0.1f,
                request.PhaseAveragingCount,
                request.DirectionAveragingCount
            );
        }

        private async Task<ISignal> GetSignal(QuasiSimultaneouslyDFRequest request)
        {
            return await Controller.GetSignal(
                request.StartFrequency * 0.1f,
                request.EndFrequency * 0.1f,
                request.PhaseAveragingCount,
                request.DirectionAveragingCount
            );
        }

        private ExecutiveDFResponse GetExecutiveDfResponse(ISignal signal, MessageHeader responseHeader)
        {
            if (signal == null)
            {
                return new ExecutiveDFResponse(responseHeader, 0, 0, 0, 0, new byte[360]);
            }
            var signalFrequency = signal.BandwidthKhz < 50 ? signal.FrequencyKhz : signal.CentralFrequencyKhz;

            var frequency = (int) (signalFrequency * 10);
            var direction = GetProtocolDirection(signal);
            var standardDeviation = (short)(signal.StandardDeviation * 10);
            var discardedDirectionPercent = (short)(signal.DiscardedDirectionsPart * 10);
            var correlationCurve = PhaseCorrelator.GetCorrelationCurve(signal).Select(v => (byte)(v * 256)).ToArray();

            return new ExecutiveDFResponse(responseHeader, frequency, direction, standardDeviation, discardedDirectionPercent, correlationCurve);
        }

        private HeterodyneRadioSourcesResponse CreateHeterodyneRadioSourcesResponse(IReadOnlyList<ISignal> signals, MessageHeader responseHeader)
        {
            if (signals.Count == 0)
            {
                return new HeterodyneRadioSourcesResponse(responseHeader, new HeterodyneRadioSouce[0]);
            }

            responseHeader.InformationLength = signals.Count * HeterodyneRadioSouce.BinarySize;
            var resultSignals = new HeterodyneRadioSouce[signals.Count];
            for (var i = 0; i < signals.Count; i++)
            {
                var signal = signals[i];
                var bandNumber = Utilities.GetBandNumber(signal.FrequencyKhz);
                var direction = (short) (signal.Direction * 10);
                var standardDeviation = (short) (signal.StandardDeviation * 10);
                var frequency = (int) (signal.CentralFrequencyKhz * 10);
                var level = (byte) -signal.Amplitude;
                var reliabilty = (byte) (signal.Reliability * 255);
                var phases = signal.Phases.Select(p => (short) (p * 10)).ToArray();
                var phaseDeviation = (byte) signal.PhaseDeviation;

                var stn = signal.Amplitude - TaskManager.FilterManager.GetAdaptiveThreshold(bandNumber);
                var signalToNoiseRatio = (byte) Math.Max(stn, 0);
 
                resultSignals[i] = new HeterodyneRadioSouce(frequency, level, direction, standardDeviation, 
                    reliabilty, signalToNoiseRatio, phaseDeviation, phases);
            }

            return new HeterodyneRadioSourcesResponse(responseHeader, resultSignals);
        }

        private void OnSectorsAndRangesChanged(object sender, SectorAndRangesChangedEventArgs commandResult)
        {
            try
            {
                if (commandResult.Result != RequestResult.Ok || !commandResult.SendBroadcastEvent)
                {
                    return;
                }
                var rangeSectors = commandResult.NewFilters.Select(f => new RangeSector(
                        f.MinFrequencyKhz * 10,
                        f.MaxFrequencyKhz * 10,
                        (short) (f.DirectionFrom * 10),
                        (short) (f.DirectionTo * 10)))
                    .ToArray();

                var header = new MessageHeader(0, 0, (byte) CommandCode.SetSectorsAndRanges, 0, rangeSectors.Length * RangeSector.BinarySize + 2);
                var message = SectorsAndRangesMessage.ToBinary(header, commandResult.RangesType, commandResult.Station, rangeSectors);
                var client = _server.ClientHandlers.FirstOrDefault(c => c.Id == commandResult.ClientId);

                SendBroadcastEvent(client, message);
            }
            catch (Exception e)
            {
                MessageLogger.Warning("Sectors and ranges broadcast event error! " + e.StackTrace);
            }
        }

        private RangeSector[] GetRangeSectors(RangeType rangeType, TargetStation station)
        {
            return TaskManager
                .GetFilterManager(station)
                .GetFilters(rangeType)
                .Select(f => new RangeSector(f.MinFrequencyKhz * 10, f.MaxFrequencyKhz * 10, (short) (f.DirectionFrom * 10), (short) (f.DirectionTo * 10)))
                .ToArray();
        }

        private void GetSectorsAndRanges(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Get sectors and ranges";
            try
            {
                LogRequest(header.Code);

                var request = GetSectorsAndRangesRequest.Parse(data);
                var rangeSectors = GetRangeSectors(request.RangesType, request.Station);
                responseHeader.InformationLength = RangeSector.BinarySize * rangeSectors.Length + 2;

                var message = SectorsAndRangesMessage.ToBinary(responseHeader, request.RangesType, request.Station, rangeSectors);
                SendToClient(client, message, responseHeader, cmdName);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                var message = SectorsAndRangesMessage.ToBinary(responseHeader, RangeType.Intelligence, TargetStation.Current, new RangeSector[0]);
                SendToClient(client, message, responseHeader, cmdName);
            }
        }

        private void SetFilters(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = FiltersMessage.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte) RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set filters");
                    return;
                }
                var filterManager = TaskManager.FilterManager;
                var thresold = -request.Threshold;
                var hasChanged = filterManager.Thresholds.Any(t => Math.Abs(t - thresold) >= 1);
                filterManager.SetGlobalThreshold(thresold);

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set filters");

                if (hasChanged)
                {
                    var eventHeader = GetResponseMessageHeader(header, length: header.InformationLength);
                    var message = FiltersMessage.ToBinary(eventHeader, request.Threshold, request.StandardDeviation,
                        request.Bandwidth, request.Duration);
                    SendBroadcastEvent(client, message);
                }
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte) RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set filters");
                throw;
            }
        }

        private void GetFilters(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, FiltersMessage.BinarySize - MessageHeader.BinarySize);
            try
            {
                LogRequest(header.Code);

                var filterManager = TaskManager.FilterManager;
                var message = FiltersMessage.ToBinary(responseHeader, (byte) -filterManager.GetThreshold(0), 0, 0, 0);
                SendToClient(client, message, responseHeader, "Get filters");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, FiltersMessage.ToBinary(responseHeader, 0, 0, 0, 0), responseHeader, "Get filters");
                throw;
            }
        }

        private async Task SetMode(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            const string cmdName = "Set mode ";

            try
            {
                var request = ModeMessage.Parse(data);
                var cmd = cmdName + request.Mode;
                LogRequest(header.Code);

                //interuppting fhss duration measuring is not allowed
                if(Controller.CurrentMode == DspServerMode.FhssDurationMeasuring)
                    SendInvalidResponse(RequestResult.InvalidRequest);

                var requestResult = await Controller.SetMode(request.Mode, client.Id);
                if (requestResult == RequestResult.Ok)
                {
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmd);
                }
                else
                {
                    SendInvalidResponse(requestResult);
                }
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                SendInvalidResponse(RequestResult.ResponseParseError);
            }

            void SendInvalidResponse(RequestResult result)
            {
                responseHeader.ErrorCode = (byte)result;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, cmdName);
            }
        }

        private void OnModeChanged(object sender, IServerCommandResult commandResult)
        {
            try
            {
                if (commandResult.Result != RequestResult.Ok)
                {
                    return;
                }

                var header = new MessageHeader(0, 0, (byte) CommandCode.SetMode, 0, ModeMessage.BinarySize - MessageHeader.BinarySize);

                var message = ModeMessage.ToBinary(header, Controller.CurrentMode);
                var client = _server.ClientHandlers.FirstOrDefault(c => c.Id == commandResult.ClientId);
                SendBroadcastEvent(client, message);
            }
            catch (Exception e)
            {
                MessageLogger.Warning("Mode changed broadcast event error! " + e.StackTrace);
            }
        }

        public async Task StartCalibration()
        {
            try
            {
                await Controller.SetMode(DspServerMode.Calibration);
            }
            catch (Exception e)
            {
                MessageLogger.Error(e);
            }
        }

        public async Task StopFhssMode()
        {
            TaskManager.ClearTasks();
            await RadioJamManager.Stop();
        }

        public async Task StartFhssMode()
        {
            TaskManager.ClearTasks();
            RadioJamManager.Start(RadioJamMode.Fhss);
        }

        private void GetMode(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, ModeMessage.BinarySize - MessageHeader.BinarySize);
            try
            {
                LogRequest(header.Code);
                SendToClient(client, ModeMessage.ToBinary(responseHeader, Controller.CurrentMode), responseHeader, "Get mode");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte) RequestResult.ResponseParseError;
                SendToClient(client, ModeMessage.ToBinary(responseHeader, Controller.CurrentMode), responseHeader, "Get mode");
            }
        }

        private void LogRequest(int commandCode)
        {
            var now = DateTime.UtcNow;

            if (_lastRequestTimeDictionary.ContainsKey(commandCode))
            {
                _lastRequestTimeDictionary[commandCode] = now;
            }
            else
            {
                _lastRequestTimeDictionary.TryAdd(commandCode, now);
            }
        }

        private void LogResponse(string message, RequestResult result)
        {
            LogResponse(-1, message, result);
        }

        private void LogResponse(int commandCode, string message, RequestResult result)
        {
            var messageEnd = "";

            var requestTimeSpan = _lastRequestTimeDictionary.ContainsKey(commandCode)
                ? DateTime.UtcNow - _lastRequestTimeDictionary[commandCode]
                : TimeSpan.Zero;

            switch (result)
            {
                case RequestResult.Ok:
                    messageEnd = "ok";
                    break;
                case RequestResult.NoConnection:
                    messageEnd = "no connection";
                    break;
                case RequestResult.ResponseParseError:
                    messageEnd = "exception";
                    break;
                case RequestResult.InvalidServerMode:
                    messageEnd = "invalid mode";
                    break;
                case RequestResult.InvalidRequest:
                    messageEnd = "invalid request";
                    break;
                case RequestResult.LinkedStationConnectionError:
                    messageEnd = "linked station connection error";
                    break;
                default:
                    MessageLogger.Error("Uknown request result in logging");
                    throw new ArgumentOutOfRangeException(nameof(result), result, null);
            }

            if (commandCode != -1)
            {
                var durationString = $"{requestTimeSpan.TotalSeconds:F0}.{requestTimeSpan.Milliseconds:D3}";
                MessageLogger.ServerLog($"{message} ({durationString}): {messageEnd}");
            }
            else
            {
                MessageLogger.ServerLog($"{message}: {messageEnd}");
            }
        }

        private void GetSpecialFrequencies(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = GetSpecialFrequenciesRequest.Parse(data);

                var frequencies = GetFrequencyRanges(request.FrequencyType, request.Station);
                responseHeader.InformationLength = frequencies.Length * Protocols.FrequencyRange.BinarySize + 1;
                var message = SpecialFrequenciesMessage.ToBinary(responseHeader, request.FrequencyType, request.Station, frequencies);
                SendToClient(client, message, responseHeader, "Get special frequencies");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                var message = SpecialFrequenciesMessage.ToBinary(responseHeader, FrequencyType.Forbidden, TargetStation.Current, new Protocols.FrequencyRange[0]);
                SendToClient(client, message, responseHeader, "Get special frequencies");
            }
        }

        private void OnSpecialRangesChanged(object sender, SpecialRangesChangedEventArgs commandResult)
        {
            try
            {
                if (commandResult.Result != RequestResult.Ok || !commandResult.SendBroadcastEvent)
                {
                    return;
                }
                var ranges = commandResult.Ranges.Select(f => new Protocols.FrequencyRange(
                        (int)(f.StartFrequencyKhz * 10),
                        (int)(f.EndFrequencyKhz * 10)
                        )).ToArray();

                var header = new MessageHeader(0, 0, (byte)CommandCode.SetSpecialFrequencies + 100, 0, ranges.Length * Protocols.FrequencyRange.BinarySize + 2);
                var message = SpecialFrequenciesMessage.ToBinary(header, commandResult.RangesType, commandResult.Station, ranges);

                SendBroadcastEvent(message);
                LogResponse(header.Code, $"{commandResult.Station} station special ranges changed", (RequestResult)header.ErrorCode);
            }
            catch (Exception e)
            {
                MessageLogger.Error("Special ranges broadcast event error! " + e.StackTrace);
            }
        }

        private Protocols.FrequencyRange[] GetFrequencyRanges(FrequencyType type, TargetStation station)
        {
            return GetFrequencies()
                .Select(f => new Protocols.FrequencyRange((int)(f.StartFrequencyKhz * 10), (int)(f.EndFrequencyKhz * 10)))
                .ToArray();

            IReadOnlyList<DspDataModel.FrequencyRange> GetFrequencies()
            {
                var filterManager = station == TargetStation.Current ? 
                    TaskManager.FilterManager : TaskManager.LinkedStationFilterManager;
                switch (type)
                {
                    case FrequencyType.Forbidden:
                        return filterManager.ForbiddenFrequencies;
                    case FrequencyType.Known:
                        return filterManager.KnownFrequencies;
                    case FrequencyType.Important:
                        return filterManager.ImportantFrequencies;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void SetCalibrationBands(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = GetAttenuatorsRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, GetAttenuatorsResponse.ToBinary(responseHeader, new AttenuatorSetting[0]), responseHeader, "Get attenuators values");
                    return;
                }
                var bands = request.BandNumbers
                    .Select(number => (int)number)
                    .ToList();
                TaskManager.FilterManager.SetCalibrationBands(bands);
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set calibration bands");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, GetAttenuatorsResponse.ToBinary(responseHeader, new AttenuatorSetting[0]), responseHeader, "Get attenuators values");
            }
        }

        private void RestartGps(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Restart Gps");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Restart Gps");
            }
        }

        private void SetGpsSyncInterval(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = SetGpsSyncIntervalRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set Gps Synchronization interval");
                    return;
                }
                //todo : add logic

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set Gps Synchronization interval");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set Gps Synchronization interval");
            }
        }

        private void SetGpsPositionError(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = SetGpsPositionErrorRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set Gps Synchronization interval");
                    return;
                }

                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set Gps Synchronization interval");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set Gps Synchronization interval");
            }
        }

        private void OnNumberOfSatellitesUpdateEvent(object sender, int numberOfSatellites)
        {
            var header = new MessageHeader(0, 0, 166, 0, NumberOfSatellitesUpdateEvent.BinarySize - MessageHeader.BinarySize);
            var message = NumberOfSatellitesUpdateEvent.ToBinary(header, (byte)numberOfSatellites);
            SendBroadcastEvent(message);
        }

        private void OnShaperStateUpdateEvent(object sender, byte[] errors, byte[] stateData)
        {
            var header = new MessageHeader(0, 0, 170, 0, stateData.Length);
            var message = ShaperConditionUpdateEvent.ToBinary(header, errors, stateData);
            SendBroadcastEvent(message);
        }

        private void OnShaperVoltageUpdateEvent(object sender, byte[] errors, float[] voltage)
        {
            var header = new MessageHeader(0, 0, 171, 0, voltage.Length * 4 + 2);
            var message = ShaperVoltageUpdateEvent.ToBinary(header, errors, (short)voltage.Length, voltage);
            UpdateShaperConditionMessage(message);
            SendBroadcastEvent(message);
        }

        private void OnShaperPowerUpdateEvent(object sender, byte[] errors, short[] power)
        {
            var header = new MessageHeader(0, 0, 172, 0, power.Length * 2 + 2);
            var message = ShaperPowerUpdateEvent.ToBinary(header, errors, (short)power.Length, power);
            UpdateShaperConditionMessage(message);
            SendBroadcastEvent(message);
        }

        private void OnShaperTemperatureUpdateEvent(object sender, byte[] errors, byte[] temperature)
        {
            var header = new MessageHeader(0, 0, 173, 0, temperature.Length);
            var message = ShaperConditionUpdateEvent.ToBinary(header, errors, temperature);
            SendBroadcastEvent(message);
        }

        private void OnShaperCurrentUpdateEvent(object sender, byte[] errors, byte[] current)
        {
            var header = new MessageHeader(0, 0, 174, 0, current.Length);
            var message = ShaperConditionUpdateEvent.ToBinary(header, errors, current);
            SendBroadcastEvent(message);
        }

        private void UpdateShaperConditionMessage(byte[] message)
        {
            message[4] += 3; //hack for shaper error bytes, because otherwise it doesn't work :/
        }

        private void SetShaperPower(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = SetShaperLiterStateRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper power");
                    return;
                }

                var result = Controller.RadioJamManager.Shaper.SetLiterPower(request.Liter, request.State);
                if (!result)
                    responseHeader.ErrorCode = 2;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper power");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper power");
            }
        }

        private void SetShaperTypeLoad(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                var request = SetShaperLiterStateRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.InformationLength = 0;
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper type load");
                    return;
                }

                var result = Controller.RadioJamManager.Shaper.SetLiterTypeLoad(request.Liter, request.State);
                if (!result)
                    responseHeader.ErrorCode = 2;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper type load");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper type load");
            }
        }
        
        /// <summary>
        /// This method only sends requests to shaper. Responses are sended back automatically
        /// </summary>
        private async Task GetShaperStateRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);

                Controller.RadioJamManager.Shaper.GetLiterPowers();
                await Micropause();
                Controller.RadioJamManager.Shaper.GetLiterCurrents();
                await Micropause();
                Controller.RadioJamManager.Shaper.GetLiterTemperatures();
                await Micropause();
                Controller.RadioJamManager.Shaper.GetLiterVoltages();

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Get shaper state");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Get shaper state");
            }

            //micropause is needed for shaper to not being drowned
            async Task Micropause()
            {
                await Task.Delay(_config.HardwareSettings.JammingConfig.SendConditionCommandDelayMs);
            }
        }

        private void GetShaperSettingsRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Get shaper settings");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Get shaper settings");
            }
        }

        private void SetShaperAntennaRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper antenna");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper antenna");
            }
        }

        private void ShaperStopJammingRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Stop shaper jamming");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Stop shaper jamming");
            }
        }

        private void ShaperStartFrsJammingRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Shaper start frs jamming");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Shaper start frs jamming");
            }
        }

        private void ShaperStartFhssJammingRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Shaper start fhss jamming");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Shaper start fhss jamming");
            }
        }

        private void ShaperRestartRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Shaper restart request");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Shaper restart request");
            }
        }

        private void ShaperStateUpdateIntervalRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper state update interval");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper state update interval");
            }
        }

        private void ShaperSettingsUpdateIntervalRequest(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header);
            try
            {
                LogRequest(header.Code);
                //todo : add logic
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper settings update interval");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.InformationLength = 0;
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set shaper settings update interval");
            }
        }

        private void UpdateLocalTime(object sender, DateTime localTime)
        {
            var header = new MessageHeader(0, 0, 151, 0, SetTimeRequest.BinarySize - MessageHeader.BinarySize);
            var message = SetTimeRequest.ToBinary(header, (byte)localTime.Hour, (byte)localTime.Minute,
                (byte)localTime.Second, (short)localTime.Millisecond);
            SendBroadcastEvent(message);
        }

        private void OnResponseGetFrsTargets(object sender, IReadOnlyCollection<IRadioSource> targets)
        {
            try
            {
                var responseHeader = new MessageHeader(0, 0, 162, 0, 0);

                var radioSources = targets.Select(t => new Protocols.RadioSource(
                    Id: t.Id,
                    IsNew: t.IsNew,
                    IsActive: t.IsActive,
                    Frequency: (int)t.FrequencyKhz,
                    Direction : (short)t.Direction,
                    Latitude : t.Latitude,
                    Longitude : t.Longitude,
                    Altitude:t.Altitude,
                    StandardDeviation : (short)t.StandardDeviation,
                    Bandwidth : (int)t.BandwidthKhz,
                    Time : new DetectionTime(
                        (byte)t.BroadcastStartTime.Hour, 
                        (byte)t.BroadcastStartTime.Minute, 
                        (byte)t.BroadcastStartTime.Second, 
                        (short)t.BroadcastStartTime.Millisecond), 
                    Modulation : (byte)t.Modulation,
                    Amplitude: (byte)t.Amplitude,
                    Duration: (int)t.BroadcastTimeSpan.TotalMilliseconds,
                    BroadcastCount: (byte)t.RelativeSubScanCount
                    )).ToArray();

                Array.Sort(radioSources, (r1, r2) => r1.Frequency.CompareTo(r2.Frequency));
                responseHeader.InformationLength = targets.Count * RadioSource.BinarySize;
                
                var message = GetRadioSourcesResponse.ToBinary(responseHeader, radioSources);
                SendBroadcastEvent(message);
                LogResponse(responseHeader.Code, "Get frs targets response", (RequestResult)responseHeader.ErrorCode);
            }
            catch (Exception ex)
            {
                MessageLogger.Warning("Get frs targets response error : " + ex.StackTrace);
            }
        }

        private async void SetRcReceiver(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: 0);
            try
            {
                LogRequest(header.Code);
                var request = AttenuatorsMessage.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set attenuators values");
                    return;
                }
                var tasks = new List<SetAttenuatorTask>();
                foreach (var setup in request.Settings)
                {
                    var task = new SetAttenuatorTask(TaskManager, setup.BandNumber, setup.IsConstAttenuatorEnabled == 1, setup.AttenuatorValue * 0.5f);
                    tasks.Add(task);
                    TaskManager.AddTask(task);
                }

                await Task.WhenAll(tasks.Select(t => t.WaitForResult()));

                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set attenuators values");

                var eventHeader = GetResponseMessageHeader(header, header.InformationLength);
                var message = AttenuatorsMessage.ToBinary(eventHeader, request.Settings);
                SendBroadcastEvent(client, message);
            }
            catch (Exception e)
            {
                MessageLogger.Error(e);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;
                SendToClient(client, DefaultMessage.ToBinary(responseHeader), responseHeader, "Set rc receiver value");
            }
        }

        private async Task GetTechAppSpectrum(MessageHeader header, byte[] data, NetClientSlim client)
        {
            var responseHeader = GetResponseMessageHeader(header, length: TechAppSpectrumResponse.BinarySize - MessageHeader.BinarySize);
            try
            {
                LogRequest(header.Code);
                var request = TechAppSpectrumRequest.Parse(data);
                if (!request.IsValid())
                {
                    responseHeader.ErrorCode = (byte)RequestResult.InvalidRequest;
                    var zeroScans = new Scan[Constants.ReceiversCount];
                    for (var i = 0; i < zeroScans.Length; i++)
                    {
                        zeroScans[i] = new Scan();
                    }
                    SendToClient(client, TechAppSpectrumResponse.ToBinary(responseHeader, zeroScans), responseHeader, "Get tech app spectrum");
                    return;
                }
                var task = new TechAppSpectrumTask(TaskManager, new Band(request.BandNumber),
                    new[] { 0, 1, 2, 3, 4, 5 }, request.AveragingCount, priority: 1);
                TaskManager.AddTask(task);
                await task.WaitForResult().ConfigureAwait(false);
                var scans = GetTechAppSpectrumData(task.TypedResult?.Scans);

                SendToClient(client, TechAppSpectrumResponse.ToBinary(responseHeader, scans), responseHeader, "Get tech app spectrum");
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                responseHeader.ErrorCode = (byte)RequestResult.ResponseParseError;

                var scans = GetEmptyScans();
                SendToClient(client, TechAppSpectrumResponse.ToBinary(responseHeader, scans), responseHeader, "Get tech app spectrum");
            }

            Scan[] GetEmptyScans()
            {
                var scans = new Scan[Constants.ReceiversCount];
                for (var i = 0; i < scans.Length; i++)
                {
                    scans[i] = new Scan();
                }

                return scans;
            }

            Scan[] GetTechAppSpectrumData(IReadOnlyList<IReceiverScan> inputScans)
            {
                var scans = GetEmptyScans();
                if (inputScans == null || inputScans.Count < Constants.ReceiversCount)
                {
                    return scans;
                }

                for (var i = 0; i < inputScans.Count; ++i)
                {
                    for (var j = 0; j < inputScans[i].Count; ++j)
                    {
                        scans[i].Amplitudes[j] = (byte)(inputScans[i].Amplitudes[j] - Constants.ReceiverMinAmplitude);
                        scans[i].Phases[j] = (short)(inputScans[i].Phases[j] * 10);
                    }
                }
                return scans;
            }
        }

        enum CommandCode
        {
            SetSpecialFrequencies = 1,
            SetMode = 2,
            SetSectorsAndRanges = 3,
            SetAttenuatorsValues = 4,
            SetFrsRadioJamTargets = 6,
            SetFhssRadioJamTarget = 7,
            SetFilters = 18,
        }

        private static readonly CommandCode[] _eventsCommands =
        {
            CommandCode.SetSpecialFrequencies,
            CommandCode.SetMode,
            CommandCode.SetSectorsAndRanges,
            CommandCode.SetAttenuatorsValues,
            CommandCode.SetFrsRadioJamTargets,
            CommandCode.SetFhssRadioJamTarget,
            CommandCode.SetFilters,
        };
    }
}