﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using Settings;
using TasksLibrary.Tasks;

namespace TasksLibrary.Modes
{
    public class SequenceReceiversMode : IModeController
    {
        public ITaskManager TaskManager { get; }
        public float ScanSpeed { get; private set; }

        private ReceiversSequenceTask _task;

        private readonly ISpectrumStorage _spectrumStorage;
        private readonly IRadioSourceStorage _radioSourceStorage;
        private readonly DirectionFindingConfig _config;

        private DateTime _lastCycleEndTime;

        public SequenceReceiversMode(ITaskManager taskManager)
        {
            TaskManager = taskManager;
            _radioSourceStorage = taskManager.RadioSourceStorage;
            _spectrumStorage = taskManager.SpectrumStorage;

            _config = Config.Instance.DirectionFindingSettings;
        }

        public Task Initialize() => Task.CompletedTask;

        public void OnActivated()
        {
            _lastCycleEndTime = DateTime.UtcNow;
        }

        public void OnStop()
        {
            _task.Cancel();
        }

        public IEnumerable<IReceiverTask> CreateTasks()
        {
            _task = new ReceiversSequenceTask(TaskManager, 
                averagingCount: Config.Instance.DirectionFindingSettings.SpectrumScanCount,
                findSignals: true,
                isEndless: true);
            _task.DataProcessedEvent += OnNewDataProcessed;
            yield return _task;
        }

        private void OnNewDataProcessed(object sender, ReceiversSequenceTask.TaskResult data)
        {
            var lastBandNumber = _task.Objectives.LastOrDefault().Number;

            foreach (var scan in data.Scans)
            {
                //hack for 99th band division between fpga and usrp
                if (scan.BandNumber == Constants.UsrpFirstBandNumber - 1)
                {
                    var usrpScan = TaskManager.SpectrumStorage.GetSpectrum(scan.BandNumber);
                    for (int i = 1638; i < scan.Amplitudes.Length; i++)
                    {
                        scan.Amplitudes[i] = usrpScan.Amplitudes[i];
                    }
                }

                _spectrumStorage.Put(scan);
                TaskManager.UpdateUsrpSpectrum();
            }

            var subScanRelativeThreshold = _config.RadioSourceRelativeSubScanCount;
            _radioSourceStorage.Put(data.Signals.Where(s => s.RelativeSubScanCount > subScanRelativeThreshold));
        }

        public void UpdateScanSpeed(float getScanTimeMs)
        {
            var currentScanSpeed = Constants.DfReceiversCount * Constants.BandwidthKhz /
                                   (1000 * (getScanTimeMs + Constants.ReceiverBandChangeTime));

            const float maxScanSpeedGhzSec = 1000;
            if (currentScanSpeed < 0 || currentScanSpeed > maxScanSpeedGhzSec)
            {
                return;
            }
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (ScanSpeed == 0)
            {
                ScanSpeed = currentScanSpeed;
            }

            const float currentScanSpeedFactor = 0.5f;

            ScanSpeed = ScanSpeed * (1 - currentScanSpeedFactor)
                        + currentScanSpeed * currentScanSpeedFactor;
        }
    }
}
