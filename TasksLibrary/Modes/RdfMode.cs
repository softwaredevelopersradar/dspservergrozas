﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataProcessor;
using DataStorages;
using DspDataModel;
using DspDataModel.DataProcessor;
using DspDataModel.DataTransfer;
using DspDataModel.LinkedStation;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using Settings;
using TasksLibrary.Tasks;

namespace TasksLibrary.Modes
{
    public class RdfMode : IModeController
    {
        public ITaskManager TaskManager { get; }
        public float ScanSpeed { get; private set; }
        
        private DirectionFindingTask _dfTask;
        private readonly DirectionFindingConfig _config;

        private readonly List<IRadioSource> _signalsToBeSent = new List<IRadioSource>();
        private int _signalsToBeSentCounter = 0;
        private int _scansToSkip = 0;
        private int _lastBandNumber = 0;
        private readonly ILinkedRdfProcessor _linkedRdfProcessor;
        private readonly IDataTransferController _dataTransferController;

        private DateTime _lastCycleEndTime;
        
        public RdfMode(ITaskManager taskManager, IDataTransferController dataTransferController)
        {
            TaskManager = taskManager;
            _config = Config.Instance.DirectionFindingSettings;
            _dataTransferController = dataTransferController;
            
            if (Config.Instance.StationsSettings.Role == StationRole.Master)
            {
                _linkedRdfProcessor = new LinkedRdfProcessor();
                SubscribeToLinkedRdfProcessorEvents();
            }
        }

        private void SubscribeToLinkedRdfProcessorEvents()
        {
            _linkedRdfProcessor.OnStateChanged += (sender, state) =>
            {
                if (state)
                {
                    SubscribeToDataTransferEvents();
                }
                else
                {
                    UnsubscribeFromDataTransferEvents();
                    MessageLogger.Error($"All linked station are unavailable. Check connections at data transfer server");
                }
            };
            _linkedRdfProcessor.SignalsProcessed += (sender, list) =>
            {
                //_lastBandNumber++; // this is the last band number, and we need the next one to sync
                //_dataTransferController.SetBandNumber(_lastBandNumber);
                //_dfTask.TrySetCurrentObjective(_lastBandNumber);
                //update our signals
                TaskManager.RadioSourceStorage.PutLinkedStationSignals(list);

                //send to proper linked stations, so they can update signals' coordinates
                var sources = TaskManager.RadioSourceStorage.GetRadioSources();
                foreach (var linkedStation in Config.Instance.StationsSettings.LinkedPositions)
                {
                    var linkedSources = sources.Where(s =>
                        s.LinkedInfo.ContainsKey(linkedStation.StationId) &&
                        s.Longitude != -1 &&
                        s.Latitude != -1).ToList();

                    _dataTransferController.SendRdfResults(
                        stationId: linkedStation.StationId,
                        results: linkedSources);
                }
            };
        }

        private void SubscribeToDataTransferEvents()
        {
            _dataTransferController.SignalsReceivedEvent += OnSignalsReceived;
            _dataTransferController.SetBandNumberRequest += OnSetBandNumberRequest;
        }

        private void OnSetBandNumberRequest(object sender, int bandNumber)
        {
            _dfTask.TrySetCurrentObjective(bandNumber);
        }

        private void OnSignalsReceived(object sender, (int stationId, IReadOnlyList<IRadioSource> signals) scanResults)
        {
            Task.Run(() => _linkedRdfProcessor.Put(scanResults.signals, scanResults.stationId));
        }

        private void UnsubscribeFromDataTransferEvents()
        {
        }

        public Task Initialize()
        {
            _linkedRdfProcessor?.Initialize();
            return Task.CompletedTask;
        }

        public void OnActivated()
        {
            _lastCycleEndTime = DateTime.UtcNow;
        }

        public void OnStop()
        {
            _dfTask?.Cancel();
            _linkedRdfProcessor?.Stop();
        }

        public IEnumerable<IReceiverTask> CreateTasks()
        {
            var dfSetup = new DirectionFindingTaskSetup
            {
                FrequencyRanges = TaskManager.FilterManager.Filters.Select(f => f.ToFrequencyRange()).ToArray(),
                TaskManager = TaskManager,
                Priority = 0,
                ReceiversIndexes = new[] {0, 1, 2, 3, 4},
                IsEndless = true,
                AutoUpdateObjectives = true
            };

            var dfTask = new DirectionFindingTask(dfSetup);
            dfTask.SignalsProcessedEvent += ProcessSignalsInStandaloneMode;
            _dfTask = dfTask;

            return new [] {_dfTask};
        }

        private IEnumerable<ISignal> RadioSourceSignals(IReadOnlyList<ISignal> signals)
        {
            var subScanRelativeThreshold = _config.RadioSourceRelativeSubScanCount;
            return signals.Where(s => s.RelativeSubScanCount > subScanRelativeThreshold);
        }

        private IEnumerable<ISignal> ImpulseSignals(IReadOnlyList<ISignal> signals)
        {
            var subScanRelativeThreshold = _config.RadioSourceRelativeSubScanCount;
            return signals.Where(s => s.RelativeSubScanCount <= subScanRelativeThreshold);
        }

        private void ProcessSignalsInStandaloneMode(object sender, SignalsProcessedEventArg e)
        {
            //hack for 99th band division between fpga and usrp
            if (e.Scan.BandNumber == Constants.UsrpFirstBandNumber - 1)
            {
                var scan = TaskManager.SpectrumStorage.GetSpectrum(e.Scan.BandNumber);
                for (int i = 1638; i < e.Scan.Amplitudes.Length; i++)
                {
                    e.Scan.Amplitudes[i] = scan.Amplitudes[i];
                }
            }
            TaskManager.SpectrumStorage.Put(e.Scan);
            TaskManager.UpdateUsrpSpectrum();
            TaskManager.RadioSourceStorage.Put(RadioSourceSignals(e.Signals));
            TaskManager.SignalStorage.Put(ImpulseSignals(e.RawSignals));

            if(!_dataTransferController.IsConnected)
                return;

            if (_scansToSkip != 0)
            {
                _scansToSkip--;
                return;
            }

            var signals = RadioSourceSignals(e.Signals)
                .Where(s => s.IsDirectionReliable())
                .Select(s => new RadioSource(s))
                .ToList();
            _signalsToBeSent.AddRange(signals);
            //_signalsToBeSentCounter++;

            if (e.Scan.BandNumber == _dfTask.Objectives.Last().Number)
            {
                var stationId = Config.Instance.StationsSettings.OwnPosition.StationId;
                switch (Config.Instance.StationsSettings.Role)
                {
                    case StationRole.Master:
                        _linkedRdfProcessor.Put(_signalsToBeSent, stationId);
                        _signalsToBeSent.Clear();
                        break;
                    case StationRole.Slave:
                        _dataTransferController.SendSignals(stationId, _signalsToBeSent, e.Scan.BandNumber);
                        _signalsToBeSent.Clear();
                        break;
                }
            }

            //if (_signalsToBeSentCounter == Math.Min(_dfTask.Objectives.Count, Constants.LinkedRdfShardSize))
            //{
            //    var stationId = Config.Instance.StationsSettings.OwnPosition.StationId;
            //    switch (Config.Instance.StationsSettings.Role)
            //    {
            //        case StationRole.Master:
            //            _lastBandNumber = e.Scan.BandNumber;
            //            _linkedRdfProcessor.Put(_signalsToBeSent, stationId);
            //            LinkedRdfReset();
            //            break;
            //        case StationRole.Slave: 
            //            _dataTransferController.SendSignals(stationId, _signalsToBeSent, e.Scan.BandNumber);
            //            LinkedRdfReset();
            //            break;
            //    }

            //    void LinkedRdfReset() 
            //    {
            //        _signalsToBeSentCounter = 0;
            //        _signalsToBeSent.Clear();

            //        //skip next few scans, so we will have constant speed for linked rdf
            //        if (_dfTask.Objectives.Count < Constants.LinkedRdfShardSize)
            //            _scansToSkip = Constants.LinkedRdfShardSize - _dfTask.Objectives.Count;
            //    }
            //}
        }

        public void UpdateScanSpeed(float getScanTimeMs)
        {
            var currentScanSpeed = Constants.BandwidthKhz / (1000 * (getScanTimeMs + Constants.ReceiverBandChangeTime));

                const float maxScanSpeedGhzSec = 1000;
                if (currentScanSpeed < 0 || currentScanSpeed > maxScanSpeedGhzSec)
                {
                    return;
                }
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (ScanSpeed == 0)
                {
                    ScanSpeed = currentScanSpeed;
                }

                const float currentScanSpeedFactor = 0.5f;

                ScanSpeed = ScanSpeed * (1 - currentScanSpeedFactor)
                            + currentScanSpeed * currentScanSpeedFactor;
        }
    }
}
