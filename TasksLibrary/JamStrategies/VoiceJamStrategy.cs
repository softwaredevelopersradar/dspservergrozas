﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.RadioJam;

namespace TasksLibrary.JamStrategies
{
    public class VoiceJamStrategy : IRadioJamStrategy
    {
        private readonly IRadioJamManager RadioJamManager;
        private readonly ManualResetEvent _changeVoiceJamStateEvent;
        private bool _jamState;

        public VoiceJamStrategy(IRadioJamManager radioJamManager, ManualResetEvent changeVoiceJamStateEvent)
        {
            RadioJamManager = radioJamManager;
            _changeVoiceJamStateEvent = changeVoiceJamStateEvent;
        }

        public async Task JammingTask(CancellationToken token)
        {
            try
            {
                if (!StartJamming())
                    throw new ArgumentException("Current settings are not valid for voice jamming");

                while (!token.IsCancellationRequested)
                {
                    var changeVoiceJamState = _changeVoiceJamStateEvent.WaitOne(1000);
                    if (changeVoiceJamState)
                    {
                        _changeVoiceJamStateEvent.Reset();
                        _jamState = !_jamState;
                        RadioJamManager.Shaper.StartVoiceJamming(_jamState, RadioJamManager.VoiceJamFrequencyKhz, RadioJamManager.VoiceJamDeviationCode);
                    }
                    await Task.Delay(1);
                }
            }
            catch(Exception e)
            {
                MessageLogger.Error(e);
            }
            finally
            {
                RadioJamManager.Shaper.StopJamming();
            }
        }

        private bool StartJamming()
        {
            if (!RadioJamManager.Shaper.CheckIsVoiceJamAvailable(
                RadioJamManager.VoiceMode, RadioJamManager.VoiceJamFilePath))
            {
                return false;
            }

            RadioJamManager.Shaper.StartVoiceJamming(_jamState, RadioJamManager.VoiceJamFrequencyKhz, RadioJamManager.VoiceJamDeviationCode);
            return true;
        }
    }
}
