﻿using DspDataModel.RadioJam;
using DspDataModel.Tasks;

namespace TasksLibrary.JamStrategies
{
    // Подавление ФРЧ
    public class FrsJamStrategy : JamStrategy
    {
        public FrsJamStrategy(IRadioJamManager radioJamManager, ITaskManager taskManager, UpdateFrsJamStateDelegate updateStateEventFunction)
            : base(radioJamManager, taskManager, updateStateEventFunction)
        { }
    }
}
