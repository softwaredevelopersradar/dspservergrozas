using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.RadioJam;
using DspDataModel.Server;
using DspDataModel.Storages;
using Settings;
using SharpExtensions;
using TasksLibrary.Tasks;

namespace TasksLibrary.JamStrategies
{
    //todo : remove later, currently not used at all
    // ���������� ��� ����
    public class FrsAutoJamStrategy : JamStrategy
    {
        private readonly IRadioSourceStorage _radioSourceStorage;
        private readonly ISpectrumStorage _spectrumStorage;
        private readonly IServerController _serverController;

        public FrsAutoJamStrategy(IServerController serverController, UpdateFrsJamStateDelegate updateStateEventFunction)
            : base(serverController.RadioJamManager, serverController.TaskManager, updateStateEventFunction)
        {
            _serverController = serverController;
            _radioSourceStorage = serverController.TaskManager.RadioSourceStorage;
            _spectrumStorage = serverController.TaskManager.SpectrumStorage;
        }

        protected override async Task<IReadOnlyList<TargetSignalPair>> FindRadioSources()
        {
            var task = new ReceiversSequenceTask(TaskManager,
                averagingCount: Config.Instance.DirectionFindingSettings.SpectrumScanCount,
                findSignals: true,
                isEndless: false);

            TaskManager.AddTask(task);

            UpdateRadioJamTargets();
            await task.WaitForResult();

            var newFoundSignals = task.TypedResult.Signals
                .Where(s => !_radioSourceStorage.HasSameRadioSource(s))
                .ToArray();

            _radioSourceStorage.Put(task.TypedResult.Signals);
            if (newFoundSignals.Length != 0)
            {
                var storage = RadioJamManager.Storage;
                var newTargets = newFoundSignals.Select(RadioJamManager.CreateRadioJamTarget);
                var targets = storage.FrsTargets.Concat(newTargets);

                _serverController.UpdateFrsRadioJamTargets(targets.ToArray());
            }
            foreach (var scan in task.TypedResult.Scans)
            {
                //hack for 99th band division between fpga and usrp
                if (scan.BandNumber == Constants.UsrpFirstBandNumber - 1)
                {
                    var usrpScan = TaskManager.SpectrumStorage.GetSpectrum(scan.BandNumber);
                    for (int i = 1638; i < scan.Amplitudes.Length; i++)
                    {
                        scan.Amplitudes[i] = usrpScan.Amplitudes[i];
                    }
                }

                _spectrumStorage.Put(scan);
            }

            TaskManager.UpdateUsrpSpectrum();

            return FindTargetSignalPairs(RadioJamManager.Storage.FrsTargets, task.TypedResult.Signals);
        }

        private void UpdateRadioJamTargets()
        {
            var targets = RadioJamManager.Storage.FrsTargets;

            var newTargets = targets
                .Where(t => t.ControlState != TargetState.NotActiveLongTime)
                .ToArray();

            if (newTargets.Length == targets.Count)
            {
                return;
            }
            _serverController.UpdateFrsRadioJamTargets(newTargets);
        }

        /// <summary>
        /// Finds for each target appropriate signal (or null otherwise) and returns list of such pairs
        /// </summary>
        private List<TargetSignalPair> FindTargetSignalPairs(IReadOnlyList<IRadioJamTarget> targets, IReadOnlyList<ISignal> signals)
        {
            var maxDeviation = Config.Instance.HardwareSettings
                .JammingConfig.ControlFrequencyDeviationKhz;

            var result = new List<TargetSignalPair>();

            foreach (var target in targets)
            {
                var signal = signals.Where(s => Distance(target, s) < maxDeviation).MaxArg(s => s.Amplitude);
                if (signal != null)
                {
                    result.Add(new TargetSignalPair(target, signal));
                }
            }

            return result;

            float Distance(IRadioJamTarget target, ISignal signal)
            {
                return SignalExtensions.Distance(
                    new FrequencyRange(target.FrequencyKhz - 1, target.FrequencyKhz + 1),
                    signal.GetFrequencyRange());
            }
        }
    }
}