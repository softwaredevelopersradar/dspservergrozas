﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using Settings;

namespace TasksLibrary.Tasks.Calibration
{
    public class NoiseCalibrationTask : CalibrationTask
    {
        public override float StepKhz { get; protected set; }

        private readonly CalibrationTaskResult _calibrationResult;

        private readonly List<float> _phaseDeviations;

        public override float Progress { get; protected set; }

        internal int BandCountToRecalibrate { get; private set; }

        public override IReadOnlyList<float> PhaseDeviations => _phaseDeviations;

        private readonly List<Band> _recalibrateBands;

        private bool _isRecalibrating;

        private readonly IReceiverManager _receiverManager;

        private int _bandIndex = 0;

        private int _totalBandCount;

        private readonly int[] _averagingCounts;

        BandCalibrationResult _lastCalibratedData;

        private readonly CalibrationConfig _calibrationConfig;

        private readonly bool _useRecalibration;

        public NoiseCalibrationTask(ITaskManager taskManager, IReadOnlyList<Band> objectives, float stepKhz, int bandIterationCount, bool useRecalibration = true, int priority = 0)
            : base(taskManager.DataProcessor, objectives, new[] {0, 1, 2, 3, 4}, bandIterationCount, true, priority)
        {
            _calibrationConfig = Config.Instance.CalibrationSettings;
            StepKhz = stepKhz;
            IsEndless = false;
            _phaseDeviations = new List<float>();
            _calibrationResult = new CalibrationTaskResult();
            _recalibrateBands = new List<Band>();
            _isRecalibrating = false;
            _receiverManager = taskManager.HardwareController.ReceiverManager;
            _totalBandCount = Objectives.Count;
            _averagingCounts = Enumerable.Repeat(int.MaxValue, Config.Instance.BandSettings.BandCount).ToArray();
            _useRecalibration = useRecalibration;

            taskManager.HardwareController.ReceiverManager.SetReceiversChannel(ReceiverChannel.NoiseGenerator);
        }

        public override Task HandleScan(IFpgaDataScan scan)
        {
            if (DataAggregator.DataCount == _calibrationConfig.CalibrationMinScanCount)
            {
                _lastCalibratedData = DataAggregator.GetCalibrationData(StepKhz);
                var deviation = AveragePhaseDeviation(_lastCalibratedData.PhaseDeviations);

                var quantileLevel = 0.25;
                var pointsInStep = StepKhz * quantileLevel * Constants.BandSampleCount / Constants.BandwidthKhz;
                var averagingCount = (int) (System.Math.Pow(6 * deviation, 2) / pointsInStep);
                if (averagingCount > _calibrationConfig.CalibrationMaxScanCount)
                {
                    averagingCount = _calibrationConfig.CalibrationMaxScanCount;
                }
                if (averagingCount - _calibrationConfig.CalibrationMinScanCount <= 5)
                {
                    averagingCount = _calibrationConfig.CalibrationMinScanCount;
                }
                _averagingCounts[CurrentObjective.Number] = averagingCount;
            }
            return base.HandleScan(scan);
        }

        internal override bool IsDataReadyForProcessing()
        {
            return DataAggregator.DataCount >= _averagingCounts[CurrentObjective.Number];
        }

        private float AveragePhaseDeviation(float[] deviations)
        {
            const float alpha = 0.7f;
            return alpha * deviations.Average() + (1 - alpha) * deviations.Max();
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            IReadOnlyList<float[]> phaseDifferencies;
            IReadOnlyList<float[]> amplitudes;
            float[] phaseDeviations;

            if (aggregator.DataCount != _calibrationConfig.CalibrationMinScanCount)
            {
                var calibration = aggregator.GetCalibrationData(StepKhz);

                phaseDeviations = calibration.PhaseDeviations;
                phaseDifferencies = calibration.Phases;
                amplitudes = calibration.Amplitudes;
            }
            else
            {
                var calibration = _lastCalibratedData;

                phaseDeviations = calibration.PhaseDeviations;
                phaseDifferencies = calibration.Phases;
                amplitudes = calibration.Amplitudes;
            }
            var phaseDeviation = AveragePhaseDeviation(phaseDeviations);
            var bandNumber = objectives[0].BandNumber;
            _bandIndex++;
            if (_isRecalibrating)
            {
                RecalibrateBand();
            }
            else
            {
                CalibrateBand();
            }
            Progress = 1f * _bandIndex / _totalBandCount;

            if (bandNumber == Objectives.Last().Number)
            {
                if (_isRecalibrating || _recalibrateBands.Count == 0)
                {
                    Progress = 1;
                    TypedResult = _calibrationResult;
                }
                else
                {
                    SetAttenuators(true);
                    Objectives = _recalibrateBands;
                    _isRecalibrating = true;
                    BandIndex = 0;
                    UpdateIterationsTasks();
                }
            }

            void RecalibrateBand()
            {
                if (phaseDeviation > _phaseDeviations[bandNumber])
                {
                    // previous band calibration was better than the current one, so we shouldn't change it
                    return;
                }
                // we'll change the phases, but keep the original amplitudes without attenuation
                var originalCalibration = _calibrationResult.RadioPathBandCalibrations[bandNumber];
                var bandCalibration = new BandRadioPathCalibration(bandNumber, phaseDifferencies, originalCalibration.Amplitudes, phaseDeviation);

                _calibrationResult.RadioPathBandCalibrations[bandNumber] = bandCalibration;
                _phaseDeviations[bandNumber] = phaseDeviation;
                LogBandCalibration(bandNumber, phaseDeviation);
            }

            void CalibrateBand()
            {
                var noiseLevel = amplitudes.Select(a => a.Average()).Average();
                if (phaseDeviation > MaxPhaseDeviation && noiseLevel > _calibrationConfig.CalibrationAttenuatorThreshold)
                {
                    if (_useRecalibration)
                    {
                        _recalibrateBands.Add(new Band(bandNumber));
                        _totalBandCount++;
                    }
                    BandCountToRecalibrate++;
                }
                var bandCalibration = new BandRadioPathCalibration(bandNumber, phaseDifferencies, amplitudes, phaseDeviation);
                _calibrationResult.AddBandRadioPathCalibration(bandCalibration);
                //updating progress
                _phaseDeviations.Add(phaseDeviation);
                LogBandCalibration(bandNumber, phaseDeviation);
            }

            void SetAttenuators(bool isConstAttenuatorEnabled)
            {
                foreach (var band in _recalibrateBands)
                {
                    _receiverManager.SetConstAttenuatorValue(band.Number, isConstAttenuatorEnabled);
                }
            }
        }
    }
}
