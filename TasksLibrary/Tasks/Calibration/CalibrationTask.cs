﻿using System;
using System.Collections.Generic;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.Tasks;

namespace TasksLibrary.Tasks.Calibration
{
    public abstract class CalibrationTask : ReceiverTask<CalibrationTaskResult>
    {
        public virtual float StepKhz { get; protected set; }

        public virtual float Progress { get; protected set; }

        public virtual IReadOnlyList<float> PhaseDeviations { get; }

        protected const float MaxPhaseDeviation = 15;

        internal void LogBandCalibration(int bandNumber, float phaseDeviation)
        {
            const float goodPhaseDeviationThreshold = 10;
            const float normalPhaseDeviationThreshold = 20;

            var color = ConsoleColor.Green;
            if (phaseDeviation > goodPhaseDeviationThreshold)
            {
                color = phaseDeviation < normalPhaseDeviationThreshold
                    ? ConsoleColor.Yellow
                    : ConsoleColor.DarkRed;
            }

            MessageLogger.Trace($"Band {bandNumber} is calibrated, phase deviation is {phaseDeviation:F1}", color);
        }

        protected CalibrationTask(IDataProcessor dataProcessor, IReadOnlyList<Band> objectives, int[] receiversIndexes, int averagingCount, bool updateObjectives = false, int priority = 0)
            : base(dataProcessor, objectives, receiversIndexes, averagingCount, updateObjectives, priority)
        { }
    }
}
