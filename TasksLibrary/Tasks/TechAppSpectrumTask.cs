﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Tasks;
using System;
using System.Linq;
using Settings;

namespace TasksLibrary.Tasks
{
    public class TechAppSpectrumTask : ReceiverTask<IFpgaDataScan>
    {
        private readonly ITaskManager _taskManager;

        private readonly List<IFpgaDataScan> _scans;

        public TechAppSpectrumTask(ITaskManager taskManager, Band target, int[] receiversIndexes, int averagingCount, int priority = 0)
            : base(taskManager.DataProcessor, new [] {target}, receiversIndexes, averagingCount, false, priority)
        {
            IsEndless = false;
            _taskManager = taskManager;
            _scans = new List<IFpgaDataScan>();
        }


        public override Task HandleScan(IFpgaDataScan scan)
        {
            if (scan.Scans.All(s => s.BandNumber == scan.RcScan.BandNumber))
            {
                _scans.Add(scan);
                return base.HandleScan(scan);
            }
            return Task.Run(() => { });
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            try
            {
                // if user tries to change attenuator at some band other than the current one
                // receiver will change it's frequency so we might get broken scan - with different bands numbers
                // if we get such scan, get spectrum method will fail and we'll just try to get one more time
                var spectrum = aggregator.GetSpectrum();
                var noiseLevel = spectrum.Amplitudes.GetNoiseLevel();
                _taskManager.FilterManager.UpdateNoiseLevel(spectrum.BandNumber, noiseLevel);
                TypedResult = MergeScans();
            }
            catch (Exception e)
            {
                MessageLogger.Warning(e.Message + "\n" + e.StackTrace);
                // ignore this, just try to get scan one more time
            }
        }

        private IFpgaDataScan MergeScans()
        {
            if (AveragingCount == 1)
            {
                return _scans[0];
            }

            var resultScans = new IReceiverScan[Constants.ReceiversCount];
            for (var i = 0; i < Constants.ReceiversCount; ++i)
            {
                var scans = _scans.Select(s => s.Scans[i]).ToArray();
                resultScans[i] = scans.MergeScans(useAverageAmplitude: true);
            }
            return new FpgaDataScan(resultScans);
        }
    }
}
