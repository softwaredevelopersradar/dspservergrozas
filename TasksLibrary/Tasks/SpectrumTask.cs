﻿using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using Settings;

namespace TasksLibrary.Tasks
{
    public class SpectrumTask : ReceiverTask<IAmplitudeBand>
    {
        private readonly ISpectrumStorage _spectrumStorage;

        private readonly List<float> _amplitudes;
        private readonly ITaskManager _taskManager;

        public SpectrumTask(SpectrumTaskSetup setup)
            : base(setup.TaskManager.DataProcessor, setup.Objectives, setup.ReceiversIndexes, setup.AveragingCount, true, setup.Priority)
        {
            _spectrumStorage = setup.TaskManager.SpectrumStorage;
            IsEndless = setup.IsEndless;
            _amplitudes = new List<float>();
            _taskManager = setup.TaskManager;
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            var bandNumber = objectives[0].BandNumber;
            var data = aggregator.GetSpectrum();
            if (IsEndless)
            {
                //hack for 99th band division between fpga and usrp
                if (data.BandNumber == Constants.UsrpFirstBandNumber - 1)
                {
                    var usrpScan = _taskManager.SpectrumStorage.GetSpectrum(data.BandNumber);
                    for (int i = 1638; i < data.Amplitudes.Length; i++)
                    {
                        data.Amplitudes[i] = usrpScan.Amplitudes[i];
                    }
                }

                _spectrumStorage.Put(data);
                _taskManager.UpdateUsrpSpectrum();
            }
            else
            {
                _amplitudes.AddRange(data.Amplitudes);
                if (bandNumber != Objectives.Last().Number)
                {
                    return;
                }
                TypedResult = new AmplitudeBand(_amplitudes.ToArray());
            }
        }
    }

    public class SpectrumTaskSetup
    {
        public ITaskManager TaskManager;
        public IReadOnlyList<Band> Objectives;
        public int[] ReceiversIndexes;
        public int Priority = 0;
        public int AveragingCount = Config.Instance.DirectionFindingSettings.SpectrumScanCount;
        public bool IsEndless = true;
    }
}
