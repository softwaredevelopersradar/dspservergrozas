﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Tasks;

namespace TasksLibrary.Tasks
{
    public class FhssTask : ReceiverTask<FhssTaskResult>
    {
        private readonly DirectionFindingTask _dfTask;
        
        public int ScanCount { get; }

        private int _currentScanIndex;

        private readonly FhssTaskResult _taskResult;

        private readonly IReadOnlyList<Band> _objectives;

        private int _currentObjectiveIndex;

        public FhssTask(DirectionFindingTaskSetup setup, int scanCount = 100)
            : base(setup.TaskManager.DataProcessor, setup.GetObjectives(), setup.ReceiversIndexes, setup.ScanAveragingCount * setup.BearingAveragingCount, setup.AutoUpdateObjectives, setup.Priority)
        {
            _objectives = setup.GetObjectives();
            setup.Objectives = new[] {_objectives[0]};
            _dfTask = new DirectionFindingTask(setup);
            _dfTask.SignalsProcessedEvent += OnSignalsProcessed;
            ScanCount = scanCount;
            _currentScanIndex = 0;
            _taskResult = new FhssTaskResult();
            _taskResult.AddScan();
        }

        private void OnSignalsProcessed(object sender, SignalsProcessedEventArg e)
        {
            var now = DateTime.Now;
            _taskResult.Scans.Last().Signals.AddRange(e.Signals.Select(s => (s, now)));
            _currentScanIndex++;
            if (_currentScanIndex != ScanCount)
            {
                _taskResult.AddScan();
            }
            else
            {
                ++_currentObjectiveIndex;
                if (_currentObjectiveIndex != _objectives.Count)
                {
                    UpdateBandNumber();
                    _currentScanIndex = 0;
                    _dfTask.Objectives = new[] {_objectives[_currentObjectiveIndex]};
                    return;
                }
                TypedResult = _taskResult;
            }
        }

        public override async Task HandleScan(IFpgaDataScan scan)
        {
            await _dfTask.HandleScan(scan).ConfigureAwait(false);
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            
        }
    }
}
