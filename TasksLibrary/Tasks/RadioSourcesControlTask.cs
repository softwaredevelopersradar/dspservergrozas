using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using Settings;
using SharpExtensions;
using TasksLibrary.JamStrategies;

namespace TasksLibrary.Tasks
{
    public class RadioSourcesControlTask : ReceiverTask<RadioSourcesControlTask.TaskResult>
    {
        private readonly ITaskManager _taskManager;

        private readonly IReadOnlyList<IRadioJamTarget> _targets;

        private readonly TaskResult _result;

        private readonly HashSet<int> _processedBands;

        private readonly float _controlFrequencyDeviationKhz;

        private readonly ISpectrumStorage _spectrumStorage;

        private readonly IReadOnlyList<Band> _usrpObjectives;
        private bool _isUsrpOnly = false;
        private bool _isUsrpProcessed = false;


        private RadioSourcesControlTask(ITaskManager taskManager, IReadOnlyList<IRadioJamTarget> targets, IReadOnlyList<Band> objectives)
            : base(taskManager.DataProcessor, objectives, new[] { 0, 1, 2, 3, 4 }, 1, false, 1, moveReceiversSynchronously: true)
        {
            _taskManager = taskManager;
            _spectrumStorage = taskManager.SpectrumStorage;
            _targets = targets;
            _result = new TaskResult();
            _processedBands = new HashSet<int>();
            IsEndless = false;

            var config = Config.Instance.HardwareSettings.JammingConfig;
            _controlFrequencyDeviationKhz = config.ControlFrequencyDeviationKhz;

            if (targets.IsNullOrEmpty())
            {
                TypedResult = new TaskResult();
                return;
            }

            // we do not split objectives to fpga and usrp here, since all USRP USAGE IS JUST A HACK
            // this causes double radio source control at usrp bands
            // to avoid it there is a filter at TaskResult.Add method
            Objectives = objectives; 
            if (Objectives.Count == 0)
                _isUsrpOnly = true;

            _usrpObjectives = objectives.Where(o => o.Number >= Constants.UsrpFirstBandNumber).ToList();

            UpdateIterationsTasks();
        }

        public static RadioSourcesControlTask Create(ITaskManager taskManager, IReadOnlyList<IRadioJamTarget> targets)
        {
            var objectives = targets
                .Select(s => s.FrequencyKhz)
                .Select(Utilities.GetBandNumber)
                .OrderBy(b => b)
                .Distinct()
                .Select(b => new Band(b))
                .ToArray();

            return new RadioSourcesControlTask(taskManager, targets, objectives);
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            ProcessUsrpData();
            if (_isUsrpOnly)
            {
                TypedResult = _result;
                return;
            }

            var scans = aggregator.GetSpectrums();

            var isLastScan = false;
            for (var i = 0; i < scans.Count; i++)
            {
                var scan = scans[i];
                if (objectives[i].BandNumber != -1)
                {
                    ProcessScan(scan);
                }
            }
            if (isLastScan)
            {
                TypedResult = _result;
            }

            void ProcessScan(IDataScan scan)
            {
                if (_processedBands.Contains(scan.BandNumber))
                {
                    return;
                }

                //hack for 99th band division between fpga and usrp
                if (scan.BandNumber == Constants.UsrpFirstBandNumber - 1)
                {
                    var usrpScan = _taskManager.SpectrumStorage.GetSpectrum(scan.BandNumber);
                    for (int i = 1638; i < scan.Amplitudes.Length; i++)
                    {
                        scan.Amplitudes[i] = usrpScan.Amplitudes[i];
                    }
                }

                _spectrumStorage.Put(scan);
                //_taskManager.UpdateUsrpSpectrum(); //todo : check me?

                if (scan.BandNumber == Objectives.Last().Number)
                {
                    isLastScan = true;
                }
                var scanTargets = _targets.Where(t => Utilities.GetBandNumber(t.FrequencyKhz) == scan.BandNumber).ToArray();
                _processedBands.Add(scan.BandNumber);

                _taskManager.FilterManager.UpdateNoiseLevel(scan);
                var adaptiveThreshold = _taskManager.FilterManager.GetAdaptiveThreshold(scan.BandNumber);

                foreach (var target in scanTargets)
                {
                    ProcessSignal(scan, target, adaptiveThreshold);
                }
            }

            // Trying to find signal corresponding to current target
            void ProcessSignal(IDataScan scan, IRadioJamTarget target, float adaptiveThreshold)
            {
                var threshold = target.UseAdaptiveThreshold ? adaptiveThreshold : target.Threshold;

                var frequencyRange = new FrequencyRange(target.FrequencyKhz - _controlFrequencyDeviationKhz, target.FrequencyKhz + _controlFrequencyDeviationKhz);
                var processConfig = ScanProcessConfig.CreateConfigWithoutDf(threshold, new [] {frequencyRange});

                var processResult = DataProcessor.GetSignals(scan, processConfig);
                var scannedSignals = processResult.Signals;

                if (scannedSignals.IsNullOrEmpty())
                {
                    return;
                }
                var result = new TargetSignalPair(target, scannedSignals.MaxArg(s => s.Amplitude));
                _result.Add(result);
            }

            void ProcessUsrpData()
            {
                if (_isUsrpProcessed)
                    return;

                foreach (var _usrpObjective in _usrpObjectives)
                {
                    _taskManager.HardwareController.UsrpReceiverManager.BandNumber = _usrpObjective.Number;
                    var usrpScan = _taskManager.HardwareController.UsrpReceiverManager.GetScan();
                    _taskManager.SpectrumStorage.Put(usrpScan);
                    _taskManager.FilterManager.UpdateNoiseLevel(usrpScan.BandNumber,
                        usrpScan.Amplitudes.GetNoiseLevel() + Config.Instance.HardwareSettings.UsrpSettings.AdaptiveThresholdAddedLevel);

                    var scanTargets = _targets.Where(t => Utilities.GetBandNumber(t.FrequencyKhz) == usrpScan.BandNumber).ToArray();
                    var adaptiveThreshold = _taskManager.FilterManager.GetAdaptiveThreshold(usrpScan.BandNumber);

                    foreach (var target in scanTargets)
                    {
                        ProcessUsrpSignal(usrpScan, target, adaptiveThreshold);
                    }
                }

                _isUsrpProcessed = true;
            }

            void ProcessUsrpSignal(IAmplitudeScan scan, IRadioJamTarget target, float adaptiveThreshold)
            {
                var threshold = target.UseAdaptiveThreshold ? adaptiveThreshold : target.Threshold;

                var frequencyRange = new FrequencyRange(target.FrequencyKhz - _controlFrequencyDeviationKhz, target.FrequencyKhz + _controlFrequencyDeviationKhz);
                var processConfig = ScanProcessConfig.CreateConfigWithoutDf(threshold, new[] { frequencyRange });

                var processResult = DataProcessor.GetSignals(scan, processConfig);
                var scannedSignals = processResult.Signals;

                if (scannedSignals.IsNullOrEmpty())
                {
                    return;
                }
                var result = new TargetSignalPair(target, scannedSignals.MaxArg(s => s.Amplitude));
                _result.Add(result);
            }
        }

        public class TaskResult
        {
            public IReadOnlyList<TargetSignalPair> Signals => _signals;

            private readonly List<TargetSignalPair> _signals;

            public TaskResult()
            {
                _signals = new List<TargetSignalPair>();
            }

            internal void Add(TargetSignalPair signal)
            {
                // when we try to assign multiple signals to one target
                // also works as a hack for usrp radio source control
                if (_signals.Any(s => s.Target.FrequencyKhz == signal.Target.FrequencyKhz))
                    return; 
                _signals.Add(signal);
            }
        }
    }
}